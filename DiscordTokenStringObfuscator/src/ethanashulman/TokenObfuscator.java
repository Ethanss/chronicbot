package ethanashulman;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Random;

public class TokenObfuscator {

    //simple ascii string obfuscation, undefined when s.Length > 255
    public static byte[] ObfuscateString(String s)
    {
        int len = s.length();
        byte[] k = new byte[len*2],
                sb = s.getBytes(StandardCharsets.US_ASCII);


        //gen key
        Random rand = new Random();
        for (int i = 0; i < len; i++)
        {
            byte sk = (byte)rand.nextInt(len - 1);
            k[i] = sk;
        }


        //apply key
        for (int i = 0; i < len; i++)
        {
            byte spot = k[i],
                 old = sb[0];
            sb[0] = sb[(int)spot];
            sb[(int)spot] = old;
        }

        //copy result into return buffer
        for (int i = 0; i < len; i++) {
        	k[i+len] = sb[i];
        }

        return k;
    }
    
    
    public static void main(String[] args) {
		BufferedReader cin;
		try {
			cin = new BufferedReader(new InputStreamReader(System.in));
	
	    	while (true) {
	    		System.out.println("Enter bot token to be obfuscated:");
	    		System.out.println(Arrays.toString(ObfuscateString(cin.readLine().replaceAll("\n|\r", ""))).replaceAll("\\]|\\[| ", ""));
	    	}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
    }
	
}
