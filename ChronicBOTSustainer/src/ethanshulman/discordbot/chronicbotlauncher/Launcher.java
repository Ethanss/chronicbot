//Ethan Alexander Shulman 2016

package ethanshulman.discordbot.chronicbotlauncher;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;


public class Launcher implements Runnable {

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private static final long serialVersionUID = -7052237344185645097L;
	
	
		
	private boolean botThreadRunning = false,
					restarting = false;
	
	
	public Launcher() {
		while (botThreadRunning) {//wait for old bot thread to shut down
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {}
		}
		
		botThreadRunning = true;
		new Thread(this).start();
	
		//wait for shutdown input
		Scanner console = new Scanner(System.in);
		while (console.hasNextLine()) {
			try {
				String input = console.nextLine();
				if (input.equals("shutdownchronicbot")) {
					try {
						processConsoleWrite.write("shutdownchronicbot");
						processConsoleWrite.write("\n");
						processConsoleWrite.flush();
						processConsoleWrite.close();
					} catch (IOException e) {}
					System.exit(0);
				} else {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {}
				}
			} catch (Exception e) {e.printStackTrace();}
		}
		console.close();
	}
	
	private BufferedWriter processConsoleWrite;

	@Override
	public void run() {
		
		Process process = null;
		try {
			process = (new ProcessBuilder("java","-jar","ChronicBOT_discordbot.jar").redirectErrorStream(true)).start();
		} catch (IOException e) {}
		
		
		processConsoleWrite = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
		Scanner ib = new Scanner(process.getInputStream());
		
		//main process thread reading console output
		while (true) {
			
			//read input
			String istr = ib.nextLine();
			if (istr.contains("Websocket closed with reason") || istr.contains("Connection timed out: connect") || istr.contains("Read timed out")) {
				//disconnected, restart bot
				restarting = true;
				try {
					processConsoleWrite.write("shutdownchronicbot");
					processConsoleWrite.write("\n");
					processConsoleWrite.flush();
					processConsoleWrite.close();
				} catch (IOException e) {}
			} else if (istr.startsWith("ChronicBOT initialized.")) {
			} else {
				if (istr.startsWith("Shutting down ChronicBOT")) {
					//bot is shut down
					break;
				}
			}
			
		}
		
		ib.close();
		
		if (restarting) {
			new Thread(this).start();
			restarting = false;
		} else {
			botThreadRunning = false;
		}
	}
	
	
}
