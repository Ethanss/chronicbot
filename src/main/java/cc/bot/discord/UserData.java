//Ethan Alexander Shulman 2016


//User data class

package cc.bot.discord;

import java.util.HashMap;

public class UserData {

	public String id;//discord user id
	public int aid;//array list id
	public HashMap<String, DataObject> data = new HashMap<String, DataObject>();//data variables
	
	
	public UserData(String i, int a) {
		id = i;
		aid = a;
	}
	
}
