//Ethan Alexander Shulman 2016

//Bot utility functions

package cc.bot.discord;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Random;


public class BotUtils {
	
	//returns image url from first image result on google images
	public static String FirstGoogleImagesResult(String searchString) {
		//https://www.google.ca/search?tbm=isch&q=test+search
		String htmlRes = BotUtils.HttpGet("https://www.google.ca/search", "tbm=isch&q="+searchString.replace(' ', '+'));
		if (htmlRes == null) return null;//something went wrong, return to prevent exceptions
		
		//find first image and parse URL from HTML
		final String findFirstImage = "class="+'"'+"rg_meta"+'"';//baseDivSearchString = "id="+'"'+"rg_s"+'"'+" class="+'"'+"y yf";
		int findex = htmlRes.indexOf(findFirstImage);
		if (findex == -1) return null;//something messed up, return to prevent exceptions
		
		//find url
		final String findURL = '"'+"ou"+'"'+":"+'"';
		findex = htmlRes.indexOf(findURL, findex);
		if (findex == -1) return null;//something messed up, return to prevent exceptions
		
		//found image :D
		findex += findURL.length();
		//find end of url
		int eindex = htmlRes.indexOf('"',findex);
		try {
			return BotUtils.Unescape(java.net.URLDecoder.decode(htmlRes.substring(findex, eindex),"UTF-8"));
		} catch (UnsupportedEncodingException e) {}
		
		return null;
		
	}
	
	//unescape unicode chars
	public static String Unescape(String s) {
		java.util.Properties p = new java.util.Properties();
		try {
			p.load(new StringReader("a="+s));
			return p.getProperty("a");
		} catch (IOException e) {}
		return s;
	}
	
	//found here http://stackoverflow.com/questions/1359689/how-to-send-http-request-in-java , reformatted and a few things changed by me
	public static String HttpPost(String targetURL, String urlParameters) {
		URL url;
		try {
			url = new URL(targetURL);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	
				
		HttpURLConnection con;
		try {
			con = (HttpURLConnection) url.openConnection();
	
			con.setRequestMethod("POST");
	
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			con.setDoOutput(true);
			
			OutputStream os = con.getOutputStream();
			os.write(urlParameters.getBytes());
			os.flush();
			os.close();
			
			int resCode = con.getResponseCode();
			
			if (resCode == HttpURLConnection.HTTP_OK) {
		         BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		         String inputLine;
		         StringBuffer response = new StringBuffer();
		 
		         while ((inputLine = in.readLine()) != null) {
		        	 response.append(inputLine);
		         }
		         in.close();
		 
		         return response.toString();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	public static String HttpGet(String targetURL, String urlParameters) {
		URL url;
		try {
			url = new URL(targetURL+"?"+urlParameters);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	
				
		HttpURLConnection con;
		try {
			con = (HttpURLConnection) url.openConnection();
	
			con.setRequestMethod("GET");
	
			con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36");
			con.setDoOutput(true);
			
			/*OutputStream os = con.getOutputStream();
			os.write(urlParameters.getBytes());
			os.flush();
			os.close();
			*/
			int resCode = con.getResponseCode();
			
			if (resCode == HttpURLConnection.HTTP_OK) {
		         BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		         String inputLine;
		         StringBuffer response = new StringBuffer();
		 
		         while ((inputLine = in.readLine()) != null) {
		        	 response.append(inputLine);
		         }
		         in.close();
		 
		         return response.toString();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	
    //simple ascii string obfuscation, undefined when s.Length > 255
    public static byte[] ObfuscateString(String s)
    {
        int len = s.length();
        byte[] k = new byte[len*2],
                sb = s.getBytes(StandardCharsets.US_ASCII);


        //gen key
        Random rand = new Random();
        for (int i = 0; i < len; i++)
        {
            byte sk = (byte)rand.nextInt(len - 1);
            k[i] = sk;
        }


        //apply key
        for (int i = 0; i < len; i++)
        {
            byte spot = k[i],
                 old = sb[0];
            sb[0] = sb[(int)spot];
            sb[(int)spot] = old;
        }

        //copy result into return buffer
        for (int i = 0; i < len; i++) {
        	k[i+len] = sb[i];
        }

        return k;
    }
    public static String DeobfuscateString(byte[] s)
    {

        int len = s.length / 2;
        byte[] ss = new byte[len];
        for (int i = 0; i < len; i++) {
        	ss[i] = s[i+len];
        }

        //apply key
        for (int i = len - 1; i > -1; i--)
        {
            byte spot = s[i],
            		old = ss[0];
            ss[0] = ss[spot];
            ss[spot] = old;
        }

        return StandardCharsets.US_ASCII.decode(ByteBuffer.wrap(ss)).toString();
    }

}
