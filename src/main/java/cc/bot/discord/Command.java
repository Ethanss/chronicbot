//Ethan Alexander Shulman 2016

//Abstract class for a chat command

package cc.bot.discord;

import de.btobastian.javacord.entities.message.Message;

public abstract class Command {
	
	public abstract void run(Message m);
	public abstract String name();
	public abstract String description();
	public void init() {}
	public void deinit() {}
	
}
