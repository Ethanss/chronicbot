//Ethan Alexander Shulman 2016

//Handles logging of messages 


package cc.bot.discord;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import de.btobastian.javacord.entities.message.Message;

public class MessageLogging {

	
	private ZipOutputStream out;
	
	public MessageLogging() {
		//open new logging file
		String date = new SimpleDateFormat("MM(dd(yyyy HH(mm(ss").format(Calendar.getInstance().getTime());

		//create logs directory if it doesnt exist
		File logDir = new File("logs");
		if (!logDir.exists()) {
			logDir.mkdir();
		}
		
		//open logging file to begin writing
		File ofile = new File("logs/"+date+".zip");
		try {
			out = new ZipOutputStream(new FileOutputStream(ofile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		
		ZipEntry entry = new ZipEntry("log.txt");
		try {
			out.putNextEntry(entry);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	
	public void finish() {
		//finish logging, closing gzip stream
		try {
			out.closeEntry();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void process(Message m) {
		//log message
		String logString = m.getAuthor().getId()+":"+m.getContent()+"\n\r\n\r\n\r\n\r";
		try {
			out.write(logString.getBytes(StandardCharsets.UTF_16));
		} catch (IOException e) {e.printStackTrace();}
	}
}
