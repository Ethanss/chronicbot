//Ethan Alexander Shulman 2016

//Base class for bot

package cc.bot.discord;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import cc.bot.discord.commands.*;

import com.google.common.util.concurrent.FutureCallback;

import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.Javacord;
import de.btobastian.javacord.entities.Channel;
import de.btobastian.javacord.entities.Server;
import de.btobastian.javacord.entities.User;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.listener.message.MessageCreateListener;
import de.btobastian.javacord.utils.LoggerUtil;


public class ChronicBot {
	
	
	public final Command[] commands = new Command[] {
		new ChronicBotHelpCommand(),
		new ShutdownChronicBotCommand(),
		new ToAllCommand(),
		new ChronicCashCommand(),
		new FlipCommand(),
		new RichestCommand(),
		new GiveCashCommand(),
		new PickUpCashCommand(),
		new StakeCommand(),
		new AboutChronicBotCommand(),
		new LockChannelsCommand(),
		new SetPermissionCommand(),
		new GoogleImageCommand(),
		new DefineCommand(),
		new MonCommand(),
		new RSPCCommand()
	};
	
	public DiscordAPI discordAPI;
	
	
	private boolean shuttingDown = false;
	
	private MessageLogging messageLogging = new MessageLogging();
	
	private Object messageQueueLock = new Object();
	private ArrayList<PreparedMessage> sendMessageQueue = new ArrayList<PreparedMessage>();
	
	private Object userDataLock = new Object();
	public ArrayList<UserData> userData = new ArrayList<UserData>();
	private HashMap<String, UserData> mappedUserData = new HashMap<String, UserData>();
	
	public HashMap<String, DataObject> mappedChannelData = new HashMap<String, DataObject>();
	
	
	public ChronicBot() {
		Main.chronicBot = this;
		
		System.out.println("Initializing ChronicBOT.");
		
		//load chronicbot options from ini
		byte[] TOKEN_BYTES = null;
		
		BufferedReader iin;
		try {
			iin = new BufferedReader(new FileReader(new File("bot.ini")));
		    String ln = null;
		    while ((ln = iin.readLine()) != null) {
		    	//process options line
		    	String[] svars = ln.split("=");
		    	if (svars.length < 2) continue;
		    	
		    	if (svars[0].equals("token")) {
		    		String[] sbytes = svars[1].split(",");
		    		TOKEN_BYTES = new byte[sbytes.length];
		    		for (int i = 0; i < sbytes.length; i++) {
		    			TOKEN_BYTES[i] = Byte.parseByte(sbytes[i]);
		    		}
		    	} else if (svars[0].equals("serverid")) {
		    		HOME_SERVER_ID = svars[1];
		    	} else if (svars[0].equals("channelid")) {
		    		HOME_CHANNEL_ID = svars[1];
		    	}
		    }
		} catch (IOException ex) {
			return;
		}
	    		
		//load user data from file
		FileInputStream fin = null;
		try {
			fin = new FileInputStream(new File("userdata"));
		} catch (FileNotFoundException e) {}
		
		if (fin != null) {
			ByteBuffer bb = ByteBuffer.allocate(1024*1024);//data shouldnt exceed 1mb
			try {
				while (fin.available() > 0) {
					fin.read(bb.array(), 0, 4);
					
					int len = bb.getInt(0);
					if (len > bb.capacity()) {
						bb = ByteBuffer.allocate(bb.capacity()*2);
					}
					fin.read(bb.array(), 0, len);
					
					int ilen = bb.getInt(0),
						pos = 4;
					String id = StandardCharsets.UTF_16.decode(ByteBuffer.wrap(bb.array(),pos,ilen)).toString();
					pos += ilen;
					int aid = bb.getInt(pos);
					pos += 4;
					UserData udat = new UserData(id,aid);
					
					int nDataEntries = bb.getInt(pos);
					pos += 4;

					for (int i = 0; i < nDataEntries; i++) {
						int keyLen = bb.getInt(pos);
						pos += 4;
						String key = StandardCharsets.UTF_16.decode(ByteBuffer.wrap(bb.array(), pos, keyLen)).toString();
						pos += keyLen;

					
						int valueLen = bb.getInt(pos);
						pos += 4;
						String value = StandardCharsets.UTF_16.decode(ByteBuffer.wrap(bb.array(), pos, valueLen)).toString();
						pos += valueLen;
						
						udat.data.put(key, new DataObject(value));
					}
										
					userData.add(udat);
					mappedUserData.put(id, udat);
				}
			} catch (IOException e) {e.printStackTrace();}
		}
		
		//load mapped channel data from file
		fin = null;
		try {
			fin = new FileInputStream(new File("channeldata"));
		} catch (FileNotFoundException e) {}
		
		if (fin != null) {
			ByteBuffer bb = ByteBuffer.allocate(1024*1024);//data shouldnt exceed 1mb
			try {
				fin.read(bb.array(), 0, 4);
			} catch (IOException e) {e.printStackTrace();}
			int nDataEntries = bb.getInt(0);
			for (int i = 0; i < nDataEntries; i++) {
				try {
					fin.read(bb.array(), 0, 4);
				} catch (IOException e) {}
				int keyLen = bb.getInt(0);

				try {
					fin.read(bb.array(), 0, keyLen);
				} catch (IOException e) {}
				String key = StandardCharsets.UTF_16.decode(ByteBuffer.wrap(bb.array(), 0, keyLen)).toString();

				try {
					fin.read(bb.array(), 0, 4);
				} catch (IOException e) {}
				int valueLen = bb.getInt(0);
				try {
					fin.read(bb.array(), 0, valueLen);
				} catch (IOException e) {}
				String value = StandardCharsets.UTF_16.decode(ByteBuffer.wrap(bb.array(), 0, valueLen)).toString();
					
				mappedChannelData.put(key, new DataObject(value));
			}
		}
		
		//load commands
		for (int i = 0; i < commands.length; i++) {
			commands[i].init();
		}
		
		System.out.println("Connecting to discord API server.");
		
		//disable debug logging
		LoggerUtil.setDebug(false);
	
		//try and login to discord with bot token
		discordAPI = Javacord.getApi(BotUtils.DeobfuscateString(TOKEN_BYTES), true);
		discordAPI.setAutoReconnect(false);
		discordAPI.connect(new FutureCallback<DiscordAPI>() {
			public void onSuccess(DiscordAPI api) {
				initializeChronicBot();
            }
			public void onFailure(Throwable t) {
				//error logging in, print error
				t.printStackTrace();
			}
        });
		
		
		//begin listening for shutdown command
		new Thread(new Runnable() {

			public void run() {
				Scanner console = new Scanner(System.in);
				while (console.hasNextLine()) {
					try {
						String input = console.nextLine();
						if (input.equals("shutdownchronicbot")) {
							Main.chronicBot.shutdown();
						} else {
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {}
						}
					} catch (Exception e) {e.printStackTrace();}
				}
				console.close();
			}
			
		}).start();
	}
	
	
	private void initializeChronicBot() {
		//add message listener
		discordAPI.registerListener(new MessageCreateListener() {
            public void onMessageCreate(DiscordAPI api, Message message) {
            	onMessageReceived(message);
            }
		});
		
		System.out.println("ChronicBOT initialized.");
	}
	

	
	private void onMessageReceived(Message m) {
		if (shuttingDown) return;//dont process any more messages if started shutting down
		

		//log
		messageLogging.process(m);
		
		
		//process command
		String msgs = m.getContent();
		if (msgs == null || msgs.length() == 0 || msgs.charAt(0) != '!') return;
		
		for (int i = 0; i < commands.length; i++) {
			if (msgs.startsWith("!"+commands[i].name())) {
				commands[i].run(m);
				return;
			}
		}
	}
	
	
	//process sendMessageQueue and send out messages at a rate of 1 per 1.5 seconds
	public void processMessageSender() {
		while (true) {
			synchronized (messageQueueLock) {
				if (sendMessageQueue.size() > 0) {
					PreparedMessage pm = sendMessageQueue.get(0);
					FutureCallback<Message> cb = new MessageSendCallback(pm);
					if (pm.toUser != null) {
						pm.toUser.sendMessage(pm.content,cb);
					} else {
						pm.toChannel.sendMessage(pm.content,cb);
					}
					
					sendMessageQueue.remove(0);
				}
			}
			
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {}
		}
	}
	
	
	
	//send message m to user t or channel p
	public void sendMessage(String m, User t, Channel p) {
		if (t != null) {
			if (t.isYourself()) return;//dont send message to yourself
		}
		PreparedMessage pm = new PreparedMessage(m,t,p);
		synchronized (messageQueueLock) {
			sendMessageQueue.add(pm);
		}
	}
	public void sendMessage(PreparedMessage p) {
		synchronized (messageQueueLock) {
			sendMessageQueue.add(p);
		}
	}
	
	
	//get user data from user id, optionally create new if doesnt exist
	public UserData getUserData(String id, boolean create) {
		synchronized (userDataLock) {
			UserData udat = mappedUserData.get(id);
			if (udat == null && create) {
				//no user data exists yet, create new
					udat = new UserData(id,userData.size());
					userData.add(udat);
					mappedUserData.put(id, udat);
			}
			return udat;
		}
	}
	
	//get discord user object from user id from chroniccast server
	public User getUser(String id) {
		return Main.chronicBot.discordAPI.getServerById(HOME_SERVER_ID).getMemberById(id);
	}

	//get discord server home
	public Server getServer() {
		return Main.chronicBot.discordAPI.getServerById(HOME_SERVER_ID);
	}
	
	//get discord server home channel
	public Channel getHomeChannel() {
		return Main.chronicBot.discordAPI.getServerById(HOME_SERVER_ID).getChannelById(HOME_CHANNEL_ID);
	}
	
	//save user data and finish logging text
	public void shutdown() {
		shuttingDown = true;
		
		System.out.println("Shutting down ChronicBOT.");
		
		messageLogging.finish();
		
		//deinit commands
		for (int i = 0; i < commands.length; i++) {
			commands[i].deinit();
		}
		
		//save user data to file
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File("userdata"));
		} catch (FileNotFoundException e) {}
		
		ByteBuffer bb = ByteBuffer.allocate(1024*1024);//1mb(1024*1024 bytes) length byte buffer, user data shouldnt exceed this
		for (int i = 0; i < userData.size(); i++) {
			UserData udat = userData.get(i);
			
			bb.position(4);
			
			int len = 0;
			
			byte[] idBytes = udat.id.getBytes(StandardCharsets.UTF_16);
			bb.putInt(idBytes.length);
			len += 4;
			bb.put(idBytes);
			len += idBytes.length;
			
			bb.putInt(udat.aid);
			len += 4;
		
			Set<Entry<String,DataObject>> sdataEntries = udat.data.entrySet();
			@SuppressWarnings("unchecked")
			Entry<String,DataObject>[] dataEntries = Arrays.copyOf(sdataEntries.toArray(), sdataEntries.size(), Entry[].class);
			
			bb.putInt(dataEntries.length);
			len += 4;
		
			for (int k = 0; k < dataEntries.length; k++) {
				byte[] keyBytes = dataEntries[k].getKey().getBytes(StandardCharsets.UTF_16),
					   valueBytes = dataEntries[k].getValue().data.getBytes(StandardCharsets.UTF_16);

				if (len+4*2+keyBytes.length+valueBytes.length > bb.capacity()) {
					ByteBuffer obb = bb;
					bb = ByteBuffer.allocate(bb.capacity()*2);
					bb.put(ByteBuffer.wrap(obb.array(),0,len));
					bb.position(len);
				}
					
				bb.putInt(keyBytes.length);
				bb.put(keyBytes);
				
				bb.putInt(valueBytes.length);
				bb.put(valueBytes);
				
				len += 4*2+keyBytes.length+valueBytes.length;
			}
			
			bb.position(0);
			bb.putInt(len);
			
			try {
				fout.write(bb.array(), 0, len+4);
			} catch (IOException e) {}
		}
		
		try {
			fout.close();
		} catch (IOException e) {}
		
		
		
		//save channel data to file
		fout = null;
		try {
			fout = new FileOutputStream(new File("channeldata"));
		} catch (FileNotFoundException e) {}
		

		int len = 0;
	
		Set<Entry<String,DataObject>> sdataEntries = mappedChannelData.entrySet();
		@SuppressWarnings("unchecked")
		Entry<String,DataObject>[] dataEntries = Arrays.copyOf(sdataEntries.toArray(), sdataEntries.size(), Entry[].class);
			
		bb.position(0);
		bb.putInt(dataEntries.length);
		len += 4;
		
		for (int k = 0; k < dataEntries.length; k++) {
			byte[] keyBytes = dataEntries[k].getKey().getBytes(StandardCharsets.UTF_16),
					  valueBytes = dataEntries[k].getValue().data.getBytes(StandardCharsets.UTF_16);

			if (len+4*2+keyBytes.length+valueBytes.length > bb.capacity()) {
				ByteBuffer obb = bb;
				bb = ByteBuffer.allocate(bb.capacity()*2);
				bb.put(ByteBuffer.wrap(obb.array(),0,len));
				bb.position(len);
			}
					
			bb.putInt(keyBytes.length);
			bb.put(keyBytes);
				
			bb.putInt(valueBytes.length);
			bb.put(valueBytes);
				
			len += 4*2+keyBytes.length+valueBytes.length;
		}
			
		try {
			fout.write(bb.array(), 0, len);
		} catch (IOException e) {e.printStackTrace();}
		
		
		try {
			fout.close();
		} catch (IOException e) {}
		
		
		System.exit(0);//destroy process
	}
	
	
	public static String HOME_SERVER_ID,
							   HOME_CHANNEL_ID;
}


class MessageSendCallback implements FutureCallback<Message> {

	private PreparedMessage pm;
	
	MessageSendCallback(PreparedMessage m) {
		pm = m;
	}
	
	public void onFailure(Throwable arg0) {
		String msg = arg0.getMessage();
		if (msg.startsWith("Missing permissions! Cannot send messages to this user")) return;//cant send message, dont retry
		Main.chronicBot.sendMessage(pm);
	}

	public void onSuccess(Message arg0) {}
	
}
