//Ethan Alexander Shulman 2016

//User data object for storing data fields


package cc.bot.discord;

public class DataObject {
	public String data;
	public Object lock = new Object();
	public DataObject() {
		data = "";
	}
	public DataObject(String s) {
		data = s;
	}
}
