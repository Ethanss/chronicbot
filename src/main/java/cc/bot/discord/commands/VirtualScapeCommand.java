//Ethan Alexander Shulman 2016


//!vs command, VirtualScape simple text based MMORPG

package cc.bot.discord.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import cc.bot.discord.BotUtils;
import cc.bot.discord.Command;
import cc.bot.discord.DataObject;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import de.btobastian.javacord.entities.Channel;
import de.btobastian.javacord.entities.message.Message;



public class VirtualScapeCommand extends Command {

	public static VirtualScapeCommand virtualScapeCommand;
	static Channel homeChannel = null;
	private VSWorld world = null;
	private Object registerLock = new Object();
	
	public VirtualScapeCommand() {
		virtualScapeCommand = this;
	}
	
	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null) return;
		
		if (homeChannel == null) {
			homeChannel = Main.chronicBot.discordAPI.getChannelById("217881375754289155");//#bot-zone home channel
		}
		
		if (world == null) {
			//world not loaded/created, load world or create new
			System.out.println("Loading and starting VirtualScape world...");
			world = new VSWorld();
		}
		
		
		//parse command
		String msg = m.getContent();
		if (msg.length() < 4) return;//no command
		
		String cmd,cmdArg;
		
		int spaceIndex = msg.indexOf(' ');
		if (spaceIndex == -1) {
			cmdArg = "";//no command args
			cmd = msg.substring(3);
		} else {
			cmdArg = msg.substring(spaceIndex+1);
			cmd = msg.substring(3,spaceIndex);
		}
		
		//get discord user id
		String duid = m.getAuthor().getId();
		
		//run command
		if (cmd.equals("register")) {
			
			//register for virtualscape with your description
			synchronized (registerLock) {
				UserData udat = Main.chronicBot.getUserData(duid, true);
				if (udat.data.get("virtualscapeid") != null) return;//already registered
				udat.data.put("virtualscapeid", new DataObject(""+world.register(m.getAuthor().getName(), cmdArg)));//register
				
				Main.chronicBot.sendMessage("Registered for VirtualScape.", null, m.getChannelReceiver());
			}
			
		} else if (cmd.equals("newlocation")) {
			
			//admin command for creating new location with name:description seperated by ':'
			String[] vars = cmdArg.split(":");
			if (vars.length < 2) return;//not enough args
			
			int luid = world.newLocation(vars[0], vars[1]);
			Main.chronicBot.sendMessage("Created new location with uid "+luid+".", null, m.getChannelReceiver());
			
		}
		
		

		//the rest of these commands require being registered for virtualscape
		UserData udat = Main.chronicBot.getUserData(duid, true);
		DataObject vdat = udat.data.get("virtualscapeid");
		if (vdat == null) return;
		int vscapeid = Integer.parseInt(vdat.data);
		
		
		if (cmd.equals("tp")) {
			
			//admin command for teleporting to location by uid
			int luid = Integer.parseInt(cmdArg);
			Main.chronicBot.sendMessage("Attempting to teleport.", null, m.getChannelReceiver());
			world.tp(vscapeid, luid);
			
		}
	}

	@Override
	public String name() {
		return "vs";
	}

	@Override
	public String description() {
		return "VirtualScape, simple text based MMORPG.";
	}

	
	public void save() {
		world.save();
	}
}


class VSWorld {
		
	//randomly generated unique id, chance of being unique 1/Integer.MAX_VALUE
	//private int worlduuid = (int) (Math.random()*Integer.MAX_VALUE);
	
	/*
	private int uuid_quests = 0;
	private ArrayList<QuestDef> quests = new ArrayList<QuestDef>();
	private int uuid_npcs = 0;
	private ArrayList<NPCDef> npcs = new ArrayList<NPCDef>();
	private int uuid_items = 0;
	private ArrayList<ItemDef> items = new ArrayList<ItemDef>();
	private int uuid_locations = 0;
	private ArrayList<LocationDef> locations = new ArrayList<LocationDef>();
	*/
	
	//hashmap of locations
	private final Object uuid_locationsLock = new Object();
	private int uuid_locations = 0;
	private final HashMap<Integer, LocationDef> locations = new HashMap<Integer, LocationDef>();
	
	//hashmap of player id and player data
	private final Object uuid_playersLock = new Object();
	private int uuid_players = 0;
	private final HashMap<Integer, Player> players = new HashMap<Integer, Player>();
	
	
	private int startingLocationId = 0;//starting location
	
	
	VSWorld() {
		//try and load world data from file
		FileInputStream fin = null;
		try {
			fin = new FileInputStream(new File("vsdata"));
		} catch (FileNotFoundException e) {}
		
		if (fin != null) {
			//world saved, load
			try {
				fin.close();
			} catch (IOException e) {}
		}
	}
	
	
	//saves world and player data to file
	void save() {
		//save world to file
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(new File("vsdata"));
			
			out.close();
		} catch (IOException e) {return;}
	}
	
	
	
	
	//teleports player(puid) to location(luid)
	void tp(int puid, int luid) {
		LocationDef l = locations.get(luid);
		if (l == null) return;//no location exists with that id
		
		Player p = players.get(puid);
		if (p == null) return;//no player exists with that id
		p.travel(l);
	}
	
	
	//registers new player, returns unique id of new player
	int register(String name, String desc) {		
		//register new player and initialize
		Player np = new Player();
		np.name = name;
		np.description = desc;
		
		np.findImageURL();
		np.generateTag();
		
		//put player in starting location
		LocationDef startingLoc = locations.get(startingLocationId);
		if (startingLoc != null) np.travel(startingLoc);
		
		synchronized (uuid_playersLock) {
			np.uuid = uuid_players;
			uuid_players++;
		}
		
		players.put(np.uuid, np);
		return np.uuid;
	}
	
	
	//registers new location with name and description, returns unique id
	int newLocation(String name, String desc) {
		LocationDef loc = new LocationDef();
		loc.name = name;
		loc.description = desc;
		
		loc.findImageURL();
		loc.generateTag();
		
		synchronized (uuid_locationsLock) {
			loc.uuid = uuid_locations;
			uuid_locations++;
		}
		
		locations.put(loc.uuid, loc);		
		return loc.uuid;
	}
}




abstract class VSObject {
	int uuid = 0;//unique id
	String imgURL = null,
		   name = null,
		   description = null,
		   tag = null;
	
	
	//find img url from google search if not already loaded
	void findImageURL() {
		if (imgURL != null) return;//already found
		
		//https://www.google.ca/search?tbm=isch&q=test+search
		String htmlRes = BotUtils.HttpGet("https://www.google.ca/search", "tbm=isch&q="+name.replace(' ', '+')+"+"+description.replace(' ', '+'));
		if (htmlRes == null) return;//something went wrong, return to prevent exceptions
		
		//find first image and parse URL from HTML
		final String findFirstImage = "class="+'"'+"rg_meta"+'"';//baseDivSearchString = "id="+'"'+"rg_s"+'"'+" class="+'"'+"y yf";
		int findex = htmlRes.indexOf(findFirstImage);
		if (findex == -1) return;//something messed up, return to prevent exceptions
		
		//find url
		final String findURL = '"'+"ou"+'"'+":"+'"';
		findex = htmlRes.indexOf(findURL, findex);
		if (findex == -1) return;//something messed up, return to prevent exceptions
		
		//found image :D
		findex += findURL.length();
		//find end of url
		int eindex = htmlRes.indexOf('"',findex);
		try {
			imgURL = BotUtils.Unescape(java.net.URLDecoder.decode(htmlRes.substring(findex, eindex),"UTF-8"));
		} catch (UnsupportedEncodingException e) {}
	}
	
	//generates tag string
	void generateTag() {
		tag = "```\n"+name+" "+description+"\n```\n"+(imgURL==null?"":imgURL)+"\n";
	}
}


class LocationDef extends VSObject {
	ArrayList<Integer> npcs = new ArrayList<Integer>(),
					   items = new ArrayList<Integer>();
}
class WeaponDef {
	int minDamage = 0,
		maxDamage = 1,
		cooldown = 0;
}
class ItemDef extends VSObject {
	int maxStacks = 1;
	boolean tradeable = true;
	WeaponDef weapon = null;
}
class QuestDef extends VSObject {
	ArrayList<int[]> stepsCode = new ArrayList<int[]>();
}
class NPCDef extends VSObject {
	//int level = 1,
	//	minDamage = 0,
	//	maxDamage = 
	
}


class Player extends VSObject {
	
	
	ItemDef[] inventory = new ItemDef[14];
	int[] inventoryCount = new int[14];
	
	int level = 1,
		exp = 0,
		health = 25;
	
	LocationDef location = null;
	
	
	void travel(LocationDef loc) {
		location = loc;
		
		//announce player has entered location
		Main.chronicBot.sendMessage(tag+"\n has entered "+loc.tag+".", null, VirtualScapeCommand.homeChannel);
	}
}