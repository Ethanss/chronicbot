//Ethan Alexander Shulman 2016

//!setpermission command gives a user permissions

package cc.bot.discord.commands;

import cc.bot.discord.Command;
import cc.bot.discord.DataObject;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import de.btobastian.javacord.entities.message.Message;

public class SetPermissionCommand extends Command {

	@Override
	public void run(Message m) {
		String msg = m.getContent();
		String[] vars = msg.split(" ");
		if (vars.length != 4) return;//invalid args
		
		String pid,pv;
		try {
			pid = vars[2].substring(2,vars[2].length()-1);
			pv = vars[3].startsWith("allow")?"1":"0";
		} catch (Exception e) {return;}
		
		String uid = m.getAuthor().getId();
		UserData udat = Main.chronicBot.getUserData(uid,true);
		
		//check if admin(keith/ethan) or has permission to set permission
		DataObject pdat = udat.data.get("perms");
		if (pdat == null || pdat.data.equals("0")) {
			if (!uid.equals("155661712715022336") &&
				!uid.equals("201328843246665728")) return;
		}
		
		//set permission
		udat = Main.chronicBot.getUserData(pid, false);
		if (udat == null) return;//user doesnt exist
		DataObject npdat = udat.data.get(vars[1]);
		if (npdat == null) {
			npdat = new DataObject(pv);
			udat.data.put(vars[1],npdat);
		} else {
			npdat.data = pv;
		}
	}

	@Override
	public String name() {
		return "setpermission";
	}

	@Override
	public String description() {
		return null;
	}

}
