//Ethan Alexander Shulman 2016


//!chroniccash command lists how much cash you have

package cc.bot.discord.commands;

import cc.bot.discord.ChronicBot;
import cc.bot.discord.Command;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import cc.bot.discord.DataObject;
import de.btobastian.javacord.entities.message.Message;

public class ChronicCashCommand extends Command {

	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null || !m.getChannelReceiver().getId().equals(ChronicBot.HOME_CHANNEL_ID)) return;//only allow bot in botzone
		
		//get user data from id
		String uid = m.getAuthor().getId();
		UserData udat = Main.chronicBot.getUserData(uid,true);
		
		//get cash amount, give 2000 cash if 0 cash every 4 hours
		DataObject cashObj = udat.data.get("cash");
		if (cashObj == null) {
			cashObj = new DataObject("2000");
			udat.data.put("cash", cashObj);
			udat.data.put("cashcd", new DataObject(System.currentTimeMillis()/1000+""));
		}
		if (udat.data.get("cashearned") == null) {
			udat.data.put("cashearned", new DataObject("0"));
		}
		
		DataObject cdObj = udat.data.get("cashcd");
		int cash = Integer.parseInt(cashObj.data),
			earned = Integer.parseInt(udat.data.get("cashearned").data);
		
		DataObject pdat = udat.data.get("infcash");
		boolean infCash = (pdat != null && pdat.data.equals("1"));

		synchronized (cdObj.lock) {
			long nowTime = System.currentTimeMillis()/1000;
			if (infCash || (cash == 0 && nowTime-Long.parseLong(cdObj.data) > 14400)) {//if cooldown of 4 hours is over and no cash left
				synchronized (cashObj.lock) {
					cashObj.data = infCash?((cash+69696969)+""):"2000";
				}
				cash = (infCash?cash+69696969:2000);
				cdObj.data = nowTime+"";
			}
		}
		
		Main.chronicBot.sendMessage("You have $"+cash+". You have earned $"+earned+" in all your time here.", null, m.getChannelReceiver());
	}

	@Override
	public String name() {
		return "chroniccash";
	}

	@Override
	public String description() {
		return "Displays how much chroniccash you have and must be used at least once before playing any gambling games like !flip, !stake or !pickupcash. If you have 0 cash this command will give you $2000 starting cash every 4 hours.";
	}

}
