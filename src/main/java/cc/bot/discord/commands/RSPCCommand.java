//Ethan Alexander Shulman 2016
//Command idea by M E R L Y N

//runescape price checker command

package cc.bot.discord.commands;

import cc.bot.discord.BotUtils;
import cc.bot.discord.ChronicBot;
import cc.bot.discord.Command;
import cc.bot.discord.Main;
import de.btobastian.javacord.entities.message.Message;

public class RSPCCommand extends Command {

	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null || !m.getChannelReceiver().getId().equals(ChronicBot.HOME_CHANNEL_ID)) return;//only allow bot in botzone

		String iname = m.getContent();
		if (iname.length() < 6) return;//not enough text
		
		iname = iname.substring(6);
		if (iname.length() < 1) return;//too short
		iname = iname.substring(0,1).toUpperCase()+iname.substring(1).replaceAll(" ","_");
		
	    String pageHTML = BotUtils.HttpGet("http://runescape.wikia.com/wiki/"+iname, "");//get item wiki page
	    if (pageHTML == null) {
	    	Main.chronicBot.sendMessage("Couldn't find item with name "+iname+".", null, m.getChannelReceiver());
	    	return;
	    }
	    
	    //html to use to find price
	    final String searchString = "<span class="+'"'+"infobox-quantity-replace"+'"'+">";

	    //find where that html occurs
	    int searchIndex = pageHTML.indexOf(searchString);
	    
	    int itemPrice = 0;
	    try {
		    //cut out price part from HTML and parse into integer
		    itemPrice = Integer.parseInt(pageHTML.substring(searchIndex+searchString.length(), pageHTML.indexOf('<',searchIndex+4)).replaceAll(",",""));
	    } catch (Exception ex) {
	    	Main.chronicBot.sendMessage("Couldn't find item with name "+iname+".", null, m.getChannelReceiver());
	    	return;
	    }


	    Main.chronicBot.sendMessage("__**"+iname+"**__ has an exchange price of "+itemPrice+".", null, m.getChannelReceiver());
	}

	@Override
	public String name() {
		return "rspc";
	}

	@Override
	public String description() {
		return "Checks the price of an item in Runescape by name, item name must have proper spelling and grammar(spacing). Format: !rspc 'itemname', example: '!rspc steel dagger'.";
	}

}
