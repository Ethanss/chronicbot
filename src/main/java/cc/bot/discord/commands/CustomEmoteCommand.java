//Ethan Alexander Shulman 2016


//!emote command

package cc.bot.discord.commands;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import cc.bot.discord.Command;
import cc.bot.discord.DataObject;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import de.btobastian.javacord.entities.message.Message;


public class CustomEmoteCommand extends Command {

	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null) return;
		
		//custom emote command
		String msgs = m.getContent();
		int cmdIndex = msgs.indexOf(' ');
		if (cmdIndex == -1) return;
		
		String cmd = msgs.substring(cmdIndex+1);
		int adminCmd = -1;
		String adminParam = null;
		if (cmd.charAt(0) == '$') {
			//admin command, only allow keith and ethan to do admin commands
			if (!m.getAuthor().getId().equals("155661712715022336") &&
					!m.getAuthor().getId().equals("201328843246665728")) return;
			
			adminCmd = cmd.startsWith("$delete")?1:0;//delete or register commands
			cmdIndex = cmd.indexOf(' ');
			if (cmdIndex == -1) return;//admin command needs parameter
			
			//grab parameter and cut to just command
			adminParam = cmd.substring(cmdIndex+1);
			cmd = cmd.substring(0, cmdIndex);
		}
		
		//get list of custom emotes from keiths user data
		UserData udat = Main.chronicBot.getUserData("155661712715022336",true);
		DataObject edat = udat.data.get("emote");
		if (edat == null) {
			ByteBuffer bbb = ByteBuffer.allocate(4);
			bbb.putInt(0);
			bbb.position(0);
			udat.data.put("emote", new DataObject(StandardCharsets.ISO_8859_1.decode(bbb).toString()));//build initial emote data
			return;
		}
		
		byte[] byteData = edat.data.getBytes(StandardCharsets.ISO_8859_1);
		ByteBuffer bb = ByteBuffer.wrap(byteData);
		int nEmotes = 0;
		
		nEmotes = bb.getInt();
		
		//if listing emotes
		if (cmd.equals("!list")) {
			String emoteListMsg = "To call an emote type !emote 'emotename'.\n\nList of emotes:\n";
			for (int i = 0; i < nEmotes; i++) {
				int nameLen = bb.getInt();
				emoteListMsg += StandardCharsets.UTF_16.decode(ByteBuffer.wrap(bb.array(),bb.position(),nameLen))+"\n";
				bb.position(bb.position()+nameLen);

				//skip emote data
				nameLen = bb.getInt();
				bb.position(bb.position()+nameLen);
			}
			Main.chronicBot.sendMessage(emoteListMsg, null, m.getChannelReceiver());
			return;
		}
		
		//if calling emote
		if (adminCmd == -1) {
			//find emote to call
			byte[] cmpBytes = cmd.getBytes(StandardCharsets.UTF_16);
			ByteBuffer cbb = ByteBuffer.wrap(cmpBytes);
			for (int i = 0; i < nEmotes; i++) {
				int nameLen = bb.getInt();
				if (nameLen != cmpBytes.length) {
					bb.position(bb.position()+nameLen);//skip name, doesnt match
				} else {
					//compare names
					int nlen = nameLen;
											
					while (nlen != 0) {
						//compare as biggest size possible to make use of optimizations
						if (nlen > 7) {
							//compare as long
							if (bb.getLong() != cbb.getLong()) break;
							nlen -= 8;
						} else if (nlen > 3) {
							//compare as int
							if (bb.getInt() != cbb.getInt()) break;
							nlen -= 4;
						} else {
							//compare as byte
							if (bb.get() != cbb.get()) break;
							nlen--;
						}
					}
					
					if (nlen == 0) {
						//equal, call this emote
						int datLen = bb.getInt();
						Main.chronicBot.sendMessage(StandardCharsets.UTF_16.decode(ByteBuffer.wrap(bb.array(), bb.position(), datLen)).toString(),
								null, m.getChannelReceiver());
						return;
					}
					
					cbb.position(0);
				}
				//skip emote data
				nameLen = bb.getInt();
				bb.position(bb.position()+nameLen);
			}
		} else {
			//if admin cmd(registering/deleting emote)
			if (adminCmd == 0) {
				//register new emote
				cmdIndex = adminParam.indexOf(' ');
				if (cmdIndex == -1) return;
				
				String newEmoteName = adminParam.substring(0,cmdIndex);
				adminParam = adminParam.substring(cmdIndex+1);
				if (adminParam.length() == 0) return;
				
				nEmotes++;
				byte[] emoteNameBytes = newEmoteName.getBytes(StandardCharsets.UTF_16),
					   emoteBytes = adminParam.getBytes(StandardCharsets.UTF_16);
				ByteBuffer ob = ByteBuffer.allocate(byteData.length+8+emoteNameBytes.length+emoteBytes.length);
				ob.putInt(nEmotes);
				if (byteData.length > 4) ob.put(byteData, 4, byteData.length-4);
				ob.putInt(emoteNameBytes.length);
				ob.put(emoteNameBytes);
				ob.putInt(emoteBytes.length);
				ob.put(emoteBytes);
				ob.position(0);
				
				//save new emote
				edat.data = StandardCharsets.ISO_8859_1.decode(ob).toString();
				
				Main.chronicBot.sendMessage("Registered emote.", null, m.getChannelReceiver());
			} else {
				//find emote to delete
				byte[] cmpBytes = adminParam.getBytes(StandardCharsets.UTF_16);
				ByteBuffer cbb = ByteBuffer.wrap(cmpBytes);
				for (int i = 0; i < nEmotes; i++) {
					int nameLen = bb.getInt();
					if (nameLen != cmpBytes.length) {
						bb.position(bb.position()+nameLen);//skip name, doesnt match
					} else {
						//compare names
						int sindex = bb.position(),
							nlen = nameLen;
												
						while (nlen != 0) {
							//compare as biggest size possible to make use of optimizations
							if (nlen > 7) {
								//compare as long
								if (bb.getLong() != cbb.getLong()) break;
								nlen -= 8;
							} else if (nlen > 3) {
								//compare as int
								if (bb.getInt() != cbb.getInt()) break;
								nlen -= 4;
							} else {
								//compare as byte
								if (bb.get() != cbb.get()) break;
								nlen--;
							}
						}
						
						if (nlen == 0) {
							//equal, delete this emote
							nEmotes--;
							int datLen = bb.getInt();
							bb.position(0);
							bb.putInt(nEmotes);
							bb.position(sindex-4);
							sindex = sindex+nameLen+4+datLen;
							bb.put(ByteBuffer.wrap(bb.array(),sindex,byteData.length-sindex));
							edat.data = StandardCharsets.ISO_8859_1.decode(ByteBuffer.wrap(bb.array(),0,byteData.length-(8+nameLen+datLen))).toString();
							Main.chronicBot.sendMessage("Deleted emote.", null, m.getChannelReceiver());
							return;
						}
						
						cbb.position(0);
					}
					//skip emote data
					nameLen = bb.getInt();
					bb.position(bb.position()+nameLen);
				}
			}
		}
	}

	@Override
	public String name() {
		return "emote";
	}

	@Override
	public String description() {
		return "Custom emotes, try !emote !list for a list of possible emotes.";
	}

}
