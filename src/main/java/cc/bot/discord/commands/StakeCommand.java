//Ethan Alexander Shulman 2016

//!stake bet on a virtual battle command


package cc.bot.discord.commands;

import cc.bot.discord.ChronicBot;
import cc.bot.discord.Command;
import cc.bot.discord.DataObject;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import de.btobastian.javacord.entities.message.Message;

public class StakeCommand extends Command {

	@Override
	public void run(Message m) {	
		if (m.getChannelReceiver() == null || !m.getChannelReceiver().getId().equals(ChronicBot.HOME_CHANNEL_ID)) return;//only allow bot in botzone
		
		String msgs = m.getContent();
		String[] vars = msgs.split(" ");
		
		String uid = m.getAuthor().getId();
		UserData udat = Main.chronicBot.getUserData(uid,true);
		DataObject stakeData = udat.data.get("stake");
		
		DataObject cashData = udat.data.get("cash");
		if (cashData == null) return;//no cash
			
		if (stakeData == null) {//check if no stake started yet
			
			if (vars.length < 2) return;
			boolean opStranger = vars[1].startsWith("stranger");

			//stake human or ai
			if (!opStranger) {
			
				//stake human
				
				//try and set up stake agreement
				synchronized (cashData.lock) {
					int cash = Integer.parseInt(cashData.data);
					
					if (vars.length < 3) return;
					int amount;
					try {
						amount = Integer.parseInt(vars[2]);//amount to gamble
					} catch (Exception ex1) {return;}//not valid gamble amount
					if (amount < 1) return;//no reason to stake 0 or negative
					if (amount > cash) {
						//not enough cash to gamble that
						Main.chronicBot.sendMessage("You don't have enough cash to stake $"+amount+".", null, m.getChannelReceiver());
						return;
					}
					
					String opId = null;
					try {
						opId = vars[1].substring(2,vars[1].length()-1);
					} catch (Exception ex2) {return;}//not valid opponent
					
					UserData odat = Main.chronicBot.getUserData(opId,false);
					if (odat == null) return;//not valid opponent
					
					DataObject ocashData = odat.data.get("cash"),
							   ostakeData = odat.data.get("stake");
					if (ocashData == null) return;//opponent doesnt have cash
					if (ostakeData != null) {
						Main.chronicBot.sendMessage("Opponent is already staking someone else.", null, m.getChannelReceiver());
						return;
					}
					
					synchronized (ocashData.lock) {
						int ocash = Integer.parseInt(ocashData.data);
						if (amount > ocash) {
							//opponent doesnt have enough cash
							Main.chronicBot.sendMessage("Opponent doesn't have enough cash to stake $"+amount+".", null, m.getChannelReceiver());
							return;
						}
						
						//everything good setup stake agreement and data
						stakeData = new DataObject(opId+","+amount+",50,0");
						ostakeData = new DataObject(uid+","+amount+",50,9");
						udat.data.put("stake", stakeData);
						odat.data.put("stake", ostakeData);
						
						Main.chronicBot.sendMessage("Waiting for a response from <@"+opId+">. !stake agree to accept and !stake cancel to decline the stake.", null, m.getChannelReceiver());
						return;
					}
				}
			} else {
				//stake ai(stranger)
				udat.data.put("stake", new DataObject("stranger,"+Math.round(Math.pow(Math.random(),8.0)*1000)+",50,50"));
				Main.chronicBot.sendMessage("Beginning battle. You each start with 50 healthpoints, choose whether to attack or defend with !stake attack/defend.", null, m.getChannelReceiver());			
				return;
			}
		}
		
		synchronized (stakeData.lock) {
			String[] stakeVars = stakeData.data.split(",");
			if (stakeVars[0].charAt(0) != 's') {
				String opId = stakeVars[0];
			
				UserData odat = Main.chronicBot.getUserData(opId, false);
				
				DataObject ostakeData = odat.data.get("stake");
				synchronized (ostakeData.lock) {
					String[] ostakeVars = ostakeData.data.split(",");
					
					//stake commands
					if (vars[1].startsWith("<@")) {
						//can't start new stake because ones already started
						Main.chronicBot.sendMessage("Cannot start another stake, still waiting for a response from <@"+stakeVars[0]+">.", null, m.getChannelReceiver());
						return;
					} else if (vars[1].equals("forfeit")) {
						//forfeit stake and handover money
						DataObject ocashData = odat.data.get("cash");
						
							synchronized(ocashData.lock) {
								int cash = Integer.parseInt(ocashData.data),
									amount = Integer.parseInt(stakeVars[1]);
								
								ocashData.data = ""+(cash+amount*2);
							}
						
						
						udat.data.remove("stake");
						odat.data.remove("stake");
						Main.chronicBot.sendMessage("Forfeited stake.", null, m.getChannelReceiver());
					} else if (vars[1].equals("decline") || vars[1].equals("cancel")) {
						//cant cancel if already started 
						if (!stakeVars[3].equals("9") && !ostakeVars[3].equals("9")) {
							return;
						}
						
						//cancel stake
						udat.data.remove("stake");
						odat.data.remove("stake");
						Main.chronicBot.sendMessage("Stake cancelled.", null, m.getChannelReceiver());
						return;
					} else if (vars[1].equals("agree") || vars[1].equals("accept")) {
						//cant agree if already started
						if (!stakeVars[3].equals("9")) return;
						
						//check if either opponent no longer has the cash
						DataObject ocashData = odat.data.get("cash");
						
						synchronized (cashData.lock) {
							synchronized(ocashData.lock) {
								int cash = Integer.parseInt(cashData.data),
									ocash = Integer.parseInt(ocashData.data),
									amount = Integer.parseInt(stakeVars[1]);
								
								if (cash < amount || ocash < amount) {
									udat.data.remove("stake");
									odat.data.remove("stake");
									Main.chronicBot.sendMessage("Not enough cash to stake anymore, stake cancelled.", null, m.getChannelReceiver());
									return;
								}
								
								//start stake
								cashData.data = ""+(cash-amount);
								ocashData.data = ""+(ocash-amount);
								
								stakeData.data = stakeVars[0]+","+stakeVars[1]+","+stakeVars[2]+",0";
								Main.chronicBot.sendMessage("Beginning battle. You each start with 50 healthpoints, choose whether to attack or defend with !stake attack/defend.", null, m.getChannelReceiver());			
								return;
							}
						}
					}
					
					if (stakeVars[3].equals("9") || ostakeVars[3].equals("9")) return;//if not started return
					
					
					boolean attack = vars[1].startsWith("a");
					
					//check if both players are now ready
					if (!ostakeVars[3].equals("0")) {
						//both now ready, do round
						String resultMsg = "";
						int roll = (int) Math.floor(Math.random()*21),
							hp = Integer.parseInt(stakeVars[2]),
							oroll = (int) Math.floor(Math.random()*21),
							ohp = Integer.parseInt(ostakeVars[2]);
						
						boolean oattack = ostakeVars[3].equals("1");
						if (attack) {
							if (!oattack) {
								//opponent defending against attack
								if (oroll >= roll) {
									//succesfull defense, possible retaliation
									if (oroll > roll*2) {
										//retaliation from opponents defense
										hp -= (oroll-roll*2);
										resultMsg += "<@"+stakeVars[0]+"> blocked and retaliated against <@"+ostakeVars[0]+">'s attack dealing "+(oroll-roll*2)+" damage. ";
									} else {
										resultMsg += "<@"+stakeVars[0]+"> blocked <@"+ostakeVars[0]+">'s attack. ";
									}
								} else {
									//succesfull attack against defense
									ohp -= roll;
									resultMsg += "<@"+ostakeVars[0]+"> attacked <@"+stakeVars[0]+"> dealing "+roll+" damage. ";
								}
							} else {
								//both attack
								ohp -= roll;
								resultMsg += "<@"+ostakeVars[0]+"> attacked <@"+stakeVars[0]+"> dealing "+roll+" damage. ";
								hp -= oroll;
								resultMsg += "<@"+stakeVars[0]+"> attacked <@"+ostakeVars[0]+"> dealing "+oroll+" damage. ";
							}
						} else {
							if (oattack) {
								//opponent attacks against defense
								if (roll >= oroll) {
									//succesfull defense, possible retaliation
									if (roll > oroll*2) {
										//retaliation
										ohp -= roll-oroll*2;
										resultMsg += "<@"+ostakeVars[0]+"> blocked and retaliated against <@"+stakeVars[0]+">'s attack dealing "+(roll-oroll*2)+" damage. ";
									} else {
										resultMsg += "<@"+ostakeVars[0]+"> blocked <@"+stakeVars[0]+">'s attack. ";
									}
								} else {
									//succesfull attack against defense
									hp -= oroll;
									resultMsg += "<@"+stakeVars[0]+"> attacked <@"+ostakeVars[0]+"> dealing "+oroll+" damage. ";
								}
							} else {
								//both defend, nothing happens
								resultMsg += "Both <@"+ostakeVars[0]+"> and <@"+stakeVars[0]+"> defended, no one dealt any damage. ";
							}
						}
											
						//save result and check if death
						if (hp <= 0 || ohp <= 0) {
							//reward winning cash
							DataObject ocashData = odat.data.get("cash");
							
							synchronized (cashData.lock) {
								synchronized(ocashData.lock) {
									int amount = Integer.parseInt(stakeVars[1]);
									
									if (hp <= 0 && ohp <= 0) {
										ocashData.data = ""+(Integer.parseInt(ocashData.data)+amount);
										cashData.data = ""+(Integer.parseInt(cashData.data)+amount);
										resultMsg += "\n\nBoth <@"+ostakeVars[0]+"> and <@"+stakeVars[0]+"> died. It's a tie!";
									} else {
										if (hp <= 0) {
											ocashData.data = ""+(Integer.parseInt(ocashData.data)+amount*2);
											if (odat.data.get("cashearned") == null) {
												odat.data.put("cashearned", new DataObject("0"));
											}
											DataObject cashEarned = odat.data.get("cashearned");
											synchronized (cashEarned.lock) {
												cashEarned.data = ""+(Integer.parseInt(cashEarned.data)+amount*2);
											}
											resultMsg += "\n\n<@"+ostakeVars[0]+"> died, <@"+stakeVars[0]+"> won $"+amount+". ";
										} else {
											cashData.data = ""+(Integer.parseInt(cashData.data)+amount*2);
											if (udat.data.get("cashearned") == null) {
												udat.data.put("cashearned", new DataObject("0"));
											}
											DataObject cashEarned = udat.data.get("cashearned");
											synchronized (cashEarned.lock) {
												cashEarned.data = ""+(Integer.parseInt(cashEarned.data)+amount*2);
											}
											resultMsg += "\n\n<@"+stakeVars[0]+"> died, <@"+ostakeVars[0]+"> won $"+amount+". ";
										}
									}
								}
							}
							
							//done stake
							udat.data.remove("stake");
							odat.data.remove("stake");
						} else {
							resultMsg += "\n\n<@"+ostakeVars[0]+"> has "+hp+"HP and <@"+stakeVars[0]+"> has "+ohp+"HP. ";
							
							//save result of round
							stakeData.data = stakeVars[0]+","+stakeVars[1]+","+hp+",0";
							ostakeData.data = ostakeVars[0]+","+ostakeVars[1]+","+ohp+",0";
						}
						
						//send out result message
						Main.chronicBot.sendMessage(resultMsg, null, m.getChannelReceiver());			
					} else {
						stakeData.data = stakeVars[0]+","+stakeVars[1]+","+stakeVars[2]+","+(attack?1:2);//set next move
						Main.chronicBot.sendMessage("Next move confirmed. ", null, m.getChannelReceiver());
					}
				}
			} else {
			
				if (vars.length < 2) return;
				
				//stake stranger
				boolean attack = vars[1].startsWith("a");

				//both now ready, do round
				String resultMsg = "";
				int roll = (int) Math.floor(Math.random()*21),
					hp = Integer.parseInt(stakeVars[2]),
					oroll = (int) Math.floor(Math.random()*21),
					ohp = Integer.parseInt(stakeVars[3]);
				
				boolean oattack = Math.random()<0.8;
				if (attack) {
					if (!oattack) {
						//opponent defending against attack
						if (oroll >= roll) {
							//succesfull defense, possible retaliation
							if (oroll > roll*2) {
								//retaliation from opponents defense
								hp -= (oroll-roll*2);
								resultMsg += "stranger blocked and retaliated against <@"+uid+">'s attack dealing "+(oroll-roll*2)+" damage. ";
							} else {
								resultMsg += "stranger blocked <@"+uid+">'s attack. ";
							}
						} else {
							//succesfull attack against defense
							ohp -= roll;
							resultMsg += "<@"+uid+"> attacked stranger dealing "+roll+" damage. ";
						}
					} else {
						//both attack
						ohp -= roll;
						resultMsg += "<@"+uid+"> attacked stranger dealing "+roll+" damage. ";
						hp -= oroll;
						resultMsg += "stranger attacked <@"+uid+"> dealing "+oroll+" damage. ";
					}
				} else {
					if (oattack) {
						//opponent attacks against defense
						if (roll >= oroll) {
							//succesfull defense, possible retaliation
							if (roll > oroll*2) {
								//retaliation
								ohp -= roll-oroll*2;
								resultMsg += "<@"+uid+"> blocked and retaliated against stranger's attack dealing "+(roll-oroll*2)+" damage. ";
							} else {
								resultMsg += "<@"+uid+"> blocked stranger's attack. ";
							}
						} else {
							//succesfull attack against defense
							hp -= oroll;
							resultMsg += "stranger attacked <@"+uid+"> dealing "+oroll+" damage. ";
						}
					} else {
						//both defend, nothing happens
						resultMsg += "Both <@"+uid+"> and stranger defended, no one dealt any damage. ";
					}
				}
									
				//save result and check if death
				if (hp <= 0 || ohp <= 0) {
					//reward winning cash					
					synchronized (cashData.lock) {
						int amount = Integer.parseInt(stakeVars[1]);
							
						if (hp <= 0 && ohp <= 0) {
							resultMsg += "\n\nBoth <@"+uid+"> and stranger died. It's a tie!";
						} else {
							if (hp <= 0) {
								resultMsg += "\n\n<@"+uid+"> died, stranger stole <@"+uid+">'s clothes and left him naked on the ground. ";
							} else {
								cashData.data = ""+(Integer.parseInt(cashData.data)+amount);
								resultMsg += "\n\nstranger died, <@"+uid+"> won $"+amount+". ";
							}
						}
					}
					
					
					//done stake
					udat.data.remove("stake");
				} else {
					resultMsg += "\n\n<@"+uid+"> has "+hp+"HP and stranger has "+ohp+"HP. ";
					
					//save result of round
					stakeData.data = stakeVars[0]+","+stakeVars[1]+","+hp+","+ohp;
				}
				
				//send out result message
				Main.chronicBot.sendMessage(resultMsg, null, m.getChannelReceiver());	
			}
		}
	}

	@Override
	public String name() {
		return "stake";
	}

	@Override
	public String description() {
		return "Bet chroniccash on a virtual battle. Format !stake @opponentname amount, example !stake @ChronicBOT 50. You can also stake a stranger(!stake stranger) to mug someone for cash. ";
	}

}
