//Ethan Alexander Shulman 2016


//!chronicbothelp displays helpful information about the bot and its commands

package cc.bot.discord.commands;

import cc.bot.discord.Command;
import cc.bot.discord.Main;
import de.btobastian.javacord.entities.message.Message;

public class ChronicBotHelpCommand extends Command {
	
	private String helpStr = null;
	
	@Override
	public void run(Message m) {
		if (helpStr == null) {
			//build help string
			helpStr = "__**Commands**__:\n\n";
			Command[] cmds = Main.chronicBot.commands;
			for (int i = 0; i < cmds.length; i++) {
				String desc = cmds[i].description();
				if (desc != null) helpStr += "**!"+cmds[i].name()+"** - "+desc+"\n\n";
			}
		}
		
		//send out help string
		Main.chronicBot.sendMessage(helpStr, m.getAuthor(), null);
	}

	@Override
	public String name() {
		return "chronicbothelp";
	}

	@Override
	public String description() {
		return "Displays helpful information about ChronicBOT's commands.";
	}

}
