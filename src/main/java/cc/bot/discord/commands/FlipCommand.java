//Ethan Alexander Shulman 2016


//!flip heads/tails amount to gamble cash on a coin flip

package cc.bot.discord.commands;

import cc.bot.discord.ChronicBot;
import cc.bot.discord.Command;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import cc.bot.discord.DataObject;
import de.btobastian.javacord.entities.message.Message;

public class FlipCommand extends Command {

	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null || !m.getChannelReceiver().getId().equals(ChronicBot.HOME_CHANNEL_ID)) return;//only allow bot in botzone
		
		//gambling vars, heads/tails and amount to gamble
		String smsg = m.getContent();
		if (smsg.length() < "!flip ".length()) return;
		boolean heads = smsg.substring("!flip ".length()).startsWith("h");
		int amount = 0;
		try {
			amount = Integer.parseInt(smsg.substring(smsg.lastIndexOf(' ')+1), 10);
		} catch (Exception e) {return;}
		if (amount < 0) return;
				
		//get user data
		String uid = m.getAuthor().getId();
		UserData udat = Main.chronicBot.getUserData(uid,true);
		
		//get cash amount
		DataObject cashObj = udat.data.get("cash");
		if (cashObj == null) {
			Main.chronicBot.sendMessage("Not enough cash to bet $"+amount+".", null, m.getChannelReceiver());
			return;
		}
		if (udat.data.get("cashearned") == null) {
			udat.data.put("cashearned", new DataObject("0"));
		}
		synchronized (cashObj.lock) {
			int cash = Integer.parseInt(cashObj.data);
	
			if (cash < amount) {
				//not enough cash to bet that
				Main.chronicBot.sendMessage("Not enough cash to bet $"+amount+".", null, m.getChannelReceiver());
			} else {
				//50/50 heads or tails gambling
				boolean win = Math.random()<0.5;
				
				if (win) cash += amount;
				else cash -= amount;
				
				//save new cash
				cashObj.data = ""+cash;
				//if win add to cash earned
				if (win) {
					DataObject cashEarned = udat.data.get("cashearned");
					synchronized (cashEarned.lock) {
						cashEarned.data = ""+(Integer.parseInt(cashEarned.data)+amount);
					}
				}
				//send result
				Main.chronicBot.sendMessage("Bet $"+amount+" on "+(heads?"heads":"tails")+" and "+(win?"won":"lost")+", you now have $"+cash+".", null, m.getChannelReceiver());
			}
		}
	}

	@Override
	public String name() {
		return "flip";
	}

	@Override
	public String description() {
		return "Gambles chroniccash on a coin flip. Format !flip heads/tales cash, example !flip tales 25.";
	}

}
