//Ethan Alexander Shulman 2016


//!shutdownchronicbot command to safely shut down the bot

package cc.bot.discord.commands;

import cc.bot.discord.Command;
import cc.bot.discord.Main;
import de.btobastian.javacord.entities.message.Message;

public class ShutdownChronicBotCommand extends Command {

	@Override
	public void run(Message m) {
		//only allow keith or ethan do command
		if (!m.getAuthor().getId().equals("155661712715022336") &&
			!m.getAuthor().getId().equals("201328843246665728")) return;
	
		Main.chronicBot.shutdown();
	}

	@Override
	public String name() {
		return "shutdownchronicbot";
	}

	@Override
	public String description() {
		return null;
	}
	
}
