//Ethan Alexander Shulman 2016

//!gimg txt shows the first google images result of 'txt'

package cc.bot.discord.commands;

import cc.bot.discord.BotUtils;
import cc.bot.discord.Command;
import cc.bot.discord.Main;
import de.btobastian.javacord.entities.message.Message;

public class GoogleImageCommand extends Command {

	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null) return;//only allow bot in botzone

		String search = m.getContent();
		try {
			search = search.substring(6);
		} catch (IllegalArgumentException ex) {return;}
		if (search.length() < 2) return;//too short
		
		//search google
		String googleImageURL = BotUtils.FirstGoogleImagesResult(search);
		if (googleImageURL == null) return;//no results
		
		Main.chronicBot.sendMessage(googleImageURL, null, m.getChannelReceiver());
	}

	@Override
	public String name() {
		return "gimg";
	}

	@Override
	public String description() {
		return "Gives the first image on google images from the search text. Format !gimg 'searchtext', example !gimg map of canada.";
	}

}
