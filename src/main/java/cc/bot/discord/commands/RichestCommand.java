//Ethan Alexander Shulman 2016

//!richest command, lists the top 10 richest people with chronicash

package cc.bot.discord.commands;

import java.util.ArrayList;

import cc.bot.discord.ChronicBot;
import cc.bot.discord.Command;
import cc.bot.discord.DataObject;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import de.btobastian.javacord.entities.User;
import de.btobastian.javacord.entities.message.Message;

public class RichestCommand extends Command {
	
	private int cooldownTime = 0;//cooldown time on richest command because it is intensive iterating through all users and finding richest

	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null || !m.getChannelReceiver().getId().equals(ChronicBot.HOME_CHANNEL_ID)) return;//only allow bot in botzone
		
		int nowTime = (int) (System.currentTimeMillis()/1000);
		if (nowTime-cooldownTime < 5) return;
		cooldownTime = nowTime;
		
		int[] ra = new int[10];
		int[] ro = new int[10];
		int filled = 0,
			min = 0;
		
		//iterate through all users finding and sorting richest
		ArrayList<UserData> users = Main.chronicBot.userData;
		for (int i = 0; i < users.size(); i++) {
			DataObject cashObj = users.get(i).data.get("cash");
			if (cashObj == null) continue;//no cash, skip
			
			int cash = Integer.parseInt(cashObj.data);
			//System.out.println( Main.chronicBot.getUser(users.get(i).id).getName()+" - $"+cash);
			if (filled == 10 && cash < min) continue;
			
			int k = -1;
			if (filled > 0) {
				for (k = filled-1; k > -1; k--) {
					if (cash < ra[k]) {
						if (filled != 10) {
							ra[k+1] = ra[k];
							ro[k+1] = ro[k];
						}
					} else {
						break;
					}
				}
			}
			if (filled == 10) {
				if (k > -1) {
					for (int z = 0; z < k+1; z++) {
						if (z >= filled-1) break;
						ra[z] = ra[z+1];
						ro[z] = ro[z+1];
					}
					ra[k] = cash;
					ro[k] = i;
				}
			} else {
				k++;
				ra[k] = cash;
				ro[k] = i;
				filled++;
			}
		}
		
		//build message string
		String msgs = "__**Richest 10**__:\n";
		for (int i = 0; i < filled; i++) {
			User user = Main.chronicBot.getUser(users.get(ro[i]).id);
			String name = "ERROR";
			if (user != null) {
				name = user.getName();
			}
			msgs += "**"+(filled-i)+"** - "+name+" - $"+ra[i]+"\n";
		}
		
		Main.chronicBot.sendMessage(msgs, null, m.getChannelReceiver());
	}

	@Override
	public String name() {
		return "richest";
	}

	@Override
	public String description() {
		return "Lists the top 10 richest people.";
	}

}
