//Ethan Alexander Shulman 2016

//!toall command

package cc.bot.discord.commands;

import java.util.Collection;

import cc.bot.discord.Command;
import cc.bot.discord.DataObject;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import de.btobastian.javacord.entities.User;
import de.btobastian.javacord.entities.message.Message;

public class ToAllCommand extends Command {

	@Override
	public void run(Message m) {
		//check if user has permissions to do toall
		UserData udat = Main.chronicBot.getUserData(m.getAuthor().getId(), false);
		if (udat == null) return;//no user data
		DataObject pdat = udat.data.get("toall");
		if (pdat == null || pdat.data.equals("0")) return;//no allowed;
	
		//send message to all members on ChronicCast server
		String msg = m.getContent().substring(name().length()+2);
		
		Collection<User> cusers = Main.chronicBot.getServer().getMembers();
		User[] users = new User[cusers.size()];
		cusers.toArray(users);
		
		for (int i = 0; i < users.length; i++) {
			Main.chronicBot.sendMessage(msg, users[i], null);
		}
	}

	@Override
	public String name() {
		return "toall";
	}

	@Override
	public String description() {
		return null;
	}

}
