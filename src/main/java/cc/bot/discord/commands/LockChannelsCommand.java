//Ethan Alexander Shulman 2016


//!lockchannels channel1id channel2id etc..  admin command for locking a bunch of channels

package cc.bot.discord.commands;

import java.util.Collection;

import cc.bot.discord.Command;
import cc.bot.discord.Main;
import de.btobastian.javacord.entities.impl.ImplChannel;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.entities.permissions.PermissionType;
import de.btobastian.javacord.entities.permissions.Role;
import de.btobastian.javacord.entities.permissions.impl.ImplPermissions;
import de.btobastian.javacord.entities.permissions.impl.ImplRole;

public class LockChannelsCommand extends Command {
	
	private boolean locked = false;

	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null) return;//only allow bot in botzone
		
		//only allow keith and ethan to do !lockchannels
		if (!m.getAuthor().getId().equals("155661712715022336") &&
				!m.getAuthor().getId().equals("201328843246665728")) return;
		
		String msg = m.getContent();
		
		//get channel id's to lock from message, seperated by spaces
		String[] channelIds;
		try {
			channelIds = msg.substring(("!"+name()+" ").length()).split(" ");
		} catch (Exception e) {return;}
		
		//get everyone role by name
		Collection<Role> croles = Main.chronicBot.getServer().getRoles();
		Role[] roles = new Role[croles.size()];
		croles.toArray(roles);
		ImplRole everyoneRole = null;
		for (int i = 0; i < roles.length; i++) {
			if (roles[i].getName().equals("@everyone")) {
				everyoneRole = (ImplRole)roles[i];
				break;
			}
		}
		if (everyoneRole == null) return;//couldnt find everyone role
		

		
		//lock channel ids
		for (int i = 0; i < channelIds.length; i++) {
			ImplChannel channel = (ImplChannel) Main.chronicBot.discordAPI.getChannelById(channelIds[i]);
			if (channel == null) continue;//skip invalid channel
			
			//permissions built from old
			ImplPermissions cperms = (ImplPermissions) channel.getOverwrittenPermissions(everyoneRole);
			int allowed = cperms.getAllowed(),
				denied = cperms.getDenied();
			if (locked) {
				allowed = PermissionType.SEND_MESSAGES.set(allowed, true);
				denied = PermissionType.SEND_MESSAGES.set(denied, false);
			} else {
				allowed = PermissionType.SEND_MESSAGES.set(allowed, false);
				denied = PermissionType.SEND_MESSAGES.set(denied, true);
			}

			everyoneRole.setOverwrittenPermissions(channel, new ImplPermissions(allowed,denied));
			
			Main.chronicBot.sendMessage("Locked channel "+channel.getName(), null, m.getChannelReceiver());
		}
		
		locked = !locked;
	}

	@Override
	public String name() {
		return "lockchannels";
	}

	@Override
	public String description() {
		return null;
	}

}
