//Ethan Alexander Shulman 2016

package cc.bot.discord.commands;

import cc.bot.discord.ChronicBot;
import cc.bot.discord.Command;
import cc.bot.discord.DataObject;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import de.btobastian.javacord.entities.message.Message;

public class GiveCashCommand extends Command {

	@Override
	public void run(Message m) {		
		if (m.getChannelReceiver() == null || !m.getChannelReceiver().getId().equals(ChronicBot.HOME_CHANNEL_ID)) return;//only allow bot in botzone
		
		String uid = m.getAuthor().getId();
		
		String smsg = m.getContent();
		String dstUserId = null;
		try {
			dstUserId = smsg.substring(smsg.indexOf("<@")+2,smsg.lastIndexOf('>'));
		} catch (Exception ex) {return;}		
		
		if (dstUserId == uid) return;//cant give yourself cash
		
		UserData rdat = Main.chronicBot.getUserData(dstUserId,false);
		if (rdat == null) return;
		
		int amount = 0;
		try {
			amount = Integer.parseInt(smsg.substring(smsg.lastIndexOf(' ')+1), 10);
		} catch (Exception e) {return;}
		if (amount < 1) return;
		
		//get user data
		UserData udat = Main.chronicBot.getUserData(uid,true);
		
		//get cash amount
		DataObject cashObj = udat.data.get("cash");
		if (cashObj == null) {
			Main.chronicBot.sendMessage("Not enough cash to give $"+amount+".", null, m.getChannelReceiver());
			return;
		}
		synchronized (cashObj.lock) {
			int cash = Integer.parseInt(cashObj.data);
			if (cash < amount) {
				Main.chronicBot.sendMessage("Not enough cash to give $"+amount+".", null, m.getChannelReceiver());
				return;
			}
			
			DataObject rcashObj = rdat.data.get("cash");
			if (rcashObj == null) {
				return;
			}
			synchronized (rcashObj.lock) {
				int rcash = Integer.parseInt(rcashObj.data)+amount;
				cash -= amount;
				rcashObj.data = ""+rcash;
				cashObj.data = ""+cash;
			}
		}
		Main.chronicBot.sendMessage(m.getAuthor().getName()+" gave away $"+amount+".", null, m.getChannelReceiver());
	}

	@Override
	public String name() {
		return "givecash";
	}

	@Override
	public String description() {
		return "Give chroniccash to another member. Format !givecash @user amount, example !givecash @ChronicBOT 250.";
	}

}
