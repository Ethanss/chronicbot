//Ethan Alexander Shulman 2016


//!define command, searches google definitions for a word

package cc.bot.discord.commands;

import cc.bot.discord.BotUtils;
import cc.bot.discord.Command;
import cc.bot.discord.Main;
import de.btobastian.javacord.entities.message.Message;

public class DefineCommand extends Command {

	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null) return;//only allow bot in botzone
		
		//get word to search for
		String word = m.getContent();
		if (word.length() < name().length()+2) return;//not enough text to search
		word = word.substring(name().length()+2);
		int spaceIndex = word.indexOf(' ');
		//dont allow spaces
		if (spaceIndex != -1) word = word.substring(0,spaceIndex);
		
		//get definition from some website
		String html = BotUtils.HttpGet("http://google-dictionary.so8848.com/meaning", "word="+word);
		if (html == null) return;//failed to access dictionary
		
		//iterate through types of meanings building definition message
		String omsg = "__**"+word+"**__\n";
		
		int findex = -1;
		final String typeSearchStr = "<div id="+'"'+"forEmbed"+'"'+"><b>";
		while ((findex = html.indexOf(typeSearchStr)) != -1) {
			html = html.substring(findex+typeSearchStr.length());
			omsg += "\n__"+html.substring(0,html.indexOf('<'))+"__\n\n";
			
			int count = 1;
			final String listSearchStr = "<li style="+'"'+"list-style:decimal"+'"'+">";
			while ((findex = html.indexOf(listSearchStr)) != -1) {
				int kindex = html.indexOf(typeSearchStr);
				if (kindex != -1 && kindex < findex) break;
				html = html.substring(findex+listSearchStr.length());
				omsg += count+". "+html.substring(0, html.indexOf("<div")).replaceAll("<i>", "*").replaceAll("</i>", "*").replaceAll("<b>","**").replaceAll("</b>","**")+"\n\n";
				count++;
			}
		}
		
		/*
		 * googles shut down their dictionary server :c
		 * 
		//get definition json from google
		String json = BotUtils.HttpGet("http://www.google.com/dictionary/json?callback=a&sl=en&tl=en&q="+word, "");
		if (json == null) return;//something went wrong
		System.out.println(json);
		
		//parse first meaning from json
		final String meaningSearchStr = '"'+"type"+'"'+":"+'"'+"meaning"+'"';
		int findex = json.indexOf(meaningSearchStr);
		if (findex == -1) return;//something went wrong
		
		json = json.substring(findex+meaningSearchStr.length());
		
		final String textSearchStr = '"'+"text"+'"'+":";
		findex = json.indexOf(textSearchStr);
		if (findex == -1) return;//something went wrong
		
		findex = findex+textSearchStr.length()+1;
		String meaning = json.substring(findex,json.indexOf('"',findex));
		*/		
		
		//send out meaning
		Main.chronicBot.sendMessage(omsg, null, m.getChannelReceiver());
	}

	@Override
	public String name() {
		return "define";
	}

	@Override
	public String description() {
		return "Gives the definition of a word(sourced from Google). Format !define 'word', example !define monkey.";
	}

}
