//Ethan Alexander Shulman 2016

//!aboutchronicbot command

package cc.bot.discord.commands;

import cc.bot.discord.Command;
import cc.bot.discord.Main;
import de.btobastian.javacord.entities.message.Message;

public class AboutChronicBotCommand extends Command {

	@Override
	public void run(Message m) {
		Main.chronicBot.sendMessage("ChronicBOT is a custom discord bot made in Java(uses Javacord) for the discord server ChronicCast. Developed by Ethan Alexander Shulman http://xaloez.com/. The code is open source and you can find the repo here https://bitbucket.org/Ethanss/chronicbot/overview.", null, m.getChannelReceiver());
	}

	@Override
	public String name() {
		return "aboutchronicbot";
	}

	@Override
	public String description() {
		return "Information about ChronicBOT.";
	}

}
