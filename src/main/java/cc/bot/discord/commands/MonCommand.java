//Ethan Alexander Shulman 2016


package cc.bot.discord.commands;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.swing.Timer;

import cc.bot.discord.ChronicBot;
import cc.bot.discord.Command;
import cc.bot.discord.DataObject;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import de.btobastian.javacord.entities.message.Message;


public class MonCommand extends Command implements ActionListener {

	private final int SPAWN_DELAY = 180000;//3 minute spawn delay
	
	private String helpMessage = null,
					helpMessage2 = null;
	
	private boolean wildBattle = false;
	private String wildBattleUID = null;
	private long wildMonTime;
	private int wildMonLength;
	private Mon wildMon = null;
	private Object wildMonLock = new Object();
	
	
	private Item wildItem = null;
	private Object wildItemLock = new Object();
	
	private int shopItem = -1;
	
	private String shopItemsText,
				   specialShopItemsText = "";
	private short[] shopItems;
	
	public MonCommand() {
		MonDef.loadMon();
				
		//start spawning every minute
		Timer t = new Timer(SPAWN_DELAY, this);
		t.setInitialDelay(5000+(int) (Math.random()*SPAWN_DELAY));
		t.start();
	}
	
	@Override
	public void init() {		
		//generate shop item text string
		shopItemsText = "__Battlemon Shop__:\n\n";
		
		ArrayList<Integer> items = new ArrayList<Integer>();
		for (int i = 0; i < MonDef.ITEMS.length; i++) {
			if (MonDef.ITEMS[i].shopChance == 0) {
				ItemDef idef = MonDef.ITEMS[i];
				shopItemsText += "#"+(i+1)+" **"+idef.name+"** - $"+idef.shopCost+"\n";
				items.add(i);
			}
		}
		shopItems = new short[items.size()];
		for (int i = 0; i < shopItems.length; i++) {
			shopItems[i] = (short)((int)items.get(i));
		}
		
		//load last spawned mon data
		DataObject lastMonDat = Main.chronicBot.mappedChannelData.get("spawnedmon");
		if (lastMonDat != null) {
			if (lastMonDat.data.length() > 0) {
				String[] svars = lastMonDat.data.split(",");
				if (svars[0].length() > 0) {
					wildBattle = true;
					wildBattleUID = svars[0];
				}
				wildMon = new Mon(svars[1]);
				wildMonTime = System.currentTimeMillis();
				wildMonLength = 180000;
			}
		}
	}
	
	@Override
	public void deinit() {
		String wildStr = wildMon==null?"":(wildBattleUID!=null?wildBattleUID:"")+","+wildMon.asString();
		Main.chronicBot.mappedChannelData.put("spawnedmon", new DataObject(wildStr));
	}
		
	
	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null || !m.getChannelReceiver().getId().equals(ChronicBot.HOME_CHANNEL_ID)) return;//only allow bot in botzone
		
		String msg = m.getContent();
		if (msg.length() < 5) return;//not long enough
		
		String subCmd,args = null;
		int sindex;
		if (msg.charAt(4) == ' ') sindex = 5;
		else sindex = 4;
		
		int spaceIndex = msg.indexOf(' ', sindex);
		if (spaceIndex == -1) {
			subCmd = msg.substring(sindex);
		} else {
			subCmd = msg.substring(sindex,spaceIndex);
			args = msg.substring(spaceIndex+1);
		}
		
		subCmd = subCmd.toLowerCase();
		
		//process sub command
		if (subCmd.equals("battle")) {
			
			
			if (args == null) {
				
				//battle wild mon
				int fmonid;
				synchronized (wildMonLock) {
					UserData udat = Main.chronicBot.getUserData(m.getAuthor().getId(), true);
					DataObject playerMonData = udat.data.get("battlemon");
					
					if (playerMonData == null) return;//not registered
					
					synchronized (playerMonData.lock) {
						Player player = new Player(playerMonData.data);
						if (player.whitedOut()) {
							//no battlemon in party to battle with
							return;
						}
						if (player.battleOpponent != null) {
							//currently in battle, check
							if (player.battleOpponent.charAt(0) == 'w') {
								//wild battle, check if still valid
								if (m.getAuthor().getId().equals(wildBattleUID)) {
									//list battle info
									Main.chronicBot.sendMessage("Currently in battle against wild **"+wildMon+"**("+wildMon.hp+"/"+wildMon.hp()+"HP). You have **"+MonDef.MON[player.mon[player.activeMon].id].name+"**("+player.mon[player.activeMon].hp+"/"+player.mon[player.activeMon].hp()+"HP) out.", null, m.getChannelReceiver());
									return;
								} else {
									//no longer in battle
									player.battleOpponent = null;
								}
							} else {
								Main.chronicBot.sendMessage("Currently in battle against another player.", null, m.getChannelReceiver());
								return;
							}
						}
						if (wildMon == null || System.currentTimeMillis()-wildMonTime > wildMonLength || wildBattle) {
							//wild mon no longer available
							Main.chronicBot.sendMessage("Wild Battlemon is no longer there.", null, m.getChannelReceiver());
							return;
						}
						
						wildBattle = true;
						wildBattleUID = m.getAuthor().getId();
						player.battleOpponent = "w";
						//send out first not dead mon
						for (int i = 0; i < player.nMon; i++) {
							if (player.mon[i].hp > 0) {
								player.activeMon = (byte) i;
								break;
							}
						}
						fmonid = player.mon[player.activeMon].id;
						playerMonData.data = player.asString();
					}
				}
				Main.chronicBot.sendMessage("Battle initiated with **"+MonDef.MON[wildMon.id].name.toUpperCase()+"**, sent out **"+MonDef.MON[fmonid].name.toUpperCase()+"** to fight. Use !mon move to use your Battlemons moves and !mon catch to catch the Battlemon.", null, m.getChannelReceiver());
			
			} else {
								
				//battle other player
				UserData udat = Main.chronicBot.getUserData(m.getAuthor().getId(), true);
				DataObject playerMonData = udat.data.get("battlemon");
				
				if (playerMonData == null) return;//not registered
				
				//int fmonid;
				synchronized (playerMonData.lock) {
					Player player = new Player(playerMonData.data);
					if (player.whitedOut()) return;//no battlemon to battle with
					if (player.battleOpponent != null && player.battleOpponent.charAt(0) != '?') return;//already initiated
					

					//start request
					int rspaceIndex = args.indexOf(' ');
						String oid;// = args.substring(2,args.length()-1);
						int betAmount = 0;
						if (rspaceIndex != -1) {
							try {
								betAmount = Integer.parseInt(args.substring(rspaceIndex+1));
							} catch (NumberFormatException ex) {return;}
							if (betAmount < 1) return;//invalid bet amount
							oid = args.substring(2,rspaceIndex);
						} else {
							oid = args.substring(2,args.length()-1);
						}
	
						
						
						UserData oudat = Main.chronicBot.getUserData(oid, false);
						if (oudat == null) {
							Main.chronicBot.sendMessage("No Battlemon player with that name.", null, m.getChannelReceiver());
							return;
						}
						
						DataObject oplayerMonData = oudat.data.get("battlemon");
						if (oplayerMonData == null) {
							Main.chronicBot.sendMessage("No Battlemon player with that name.", null, m.getChannelReceiver());
							return;
						}
						
						synchronized (oplayerMonData.lock) {
							Player oplayer = new Player(oplayerMonData.data);
							
							if (oplayer.battleOpponent == null) {
								//request
								player.battleOpponent = "?"+oid+(betAmount>0?":"+betAmount:"");
								playerMonData.data = player.asString();
								Main.chronicBot.sendMessage("Battle requested. To accept a battle request a battle with desired opponent.", null, m.getChannelReceiver());
								return;
							
							} else {
								String betStr = (betAmount>0?":"+betAmount:"");
								String oppStr = m.getAuthor().getId()+betStr;
								if (oplayer.battleOpponent.equals("?"+oppStr)) {
									//see if accepting request

									//accept
									//put both players in battle
									player.battleOpponent = "@"+oid+betStr;
									oplayer.battleOpponent = "@"+oppStr;
										
									player.nextMove = -1;
									oplayer.nextMove = -1;
										
									for (int i = 0; i < player.nMon; i++) {
										if (player.mon[i].hp > 0) {
											player.activeMon = (byte) i;
											break;
										}
									}
										
										for (int i = 0; i < oplayer.nMon; i++) {
											if (oplayer.mon[i].hp > 0) {
												oplayer.activeMon = (byte) i;
												break;
											}
										}
										
										int fmon1 = player.mon[player.activeMon].id,
											fmon2 = oplayer.mon[oplayer.activeMon].id;
										
										Main.chronicBot.sendMessage("Battle initiated, <@"+m.getAuthor().getId()+"> sent out **"+MonDef.MON[fmon1].name.toUpperCase()+"** to fight, <@"+oid+"> sent out **"+MonDef.MON[fmon2].name.toUpperCase()+"**. Use !mon move to use your Battlemons moves.", null, m.getChannelReceiver());
									
								} else {
									//request
									player.battleOpponent = "?"+oid+(betAmount>0?":"+betAmount:"");
									playerMonData.data = player.asString();
									Main.chronicBot.sendMessage("Battle requested. To accept a battle request a battle with desired opponent.", null, m.getChannelReceiver());
									return;
								}
							}
							
							oplayerMonData.data = oplayer.asString();
						}

						playerMonData.data = player.asString();
				}
			}
			
		} else if (subCmd.equals("shop")) {

			if (args == null) {
				//list shop items
				Main.chronicBot.sendMessage(shopItemsText+specialShopItemsText, null, m.getChannelReceiver());
			} else {
				//process shop args
				String[] avars = args.split(" ");

				if (avars.length < 2) return;//not enough args
				avars[0] = avars[0].toLowerCase();
				if (avars[0].equals("buy")) {
					
					
					//buy item from store
					UserData udat = Main.chronicBot.getUserData(m.getAuthor().getId(), true);
					DataObject playerMonData = udat.data.get("battlemon");
					DataObject cashDat = udat.data.get("cash");
					
					//not registered in either battlemon or chroniccash
					if (playerMonData == null) {
						Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
						return;
					}
					if (cashDat == null) {
						Main.chronicBot.sendMessage("You need to register for chroniccash using !chroniccash before you can do this.", null, m.getChannelReceiver());
						return;
					}
					
					int count = 1;
					//count of item to buy
					if (avars.length > 2) {
						try {
							count = Integer.parseInt(avars[2]);
						} catch (NumberFormatException ex) {return;}
					}
					
					//item to buy, shop item #
					int iid;
					try {
						iid = Integer.parseInt(avars[1]);
					} catch (NumberFormatException e) {return;}
					iid--;
					//check if valid id
					if (iid < 0 || iid >= shopItems.length) return;//not valid id
					
					String buyMsg = "";
					synchronized (playerMonData.lock) {
						Player player = new Player(playerMonData.data);
						
						//check if player can fit items
						int piid = player.findItem(shopItems[iid]);
						if (piid != -1) {
							if (player.inventory.get(piid).count > Integer.MAX_VALUE-count) return;//cant fit items
						}
						
						ItemDef idef = MonDef.ITEMS[shopItems[iid]];
						int sellPrice = idef.shopCost;
						if (sellPrice > Integer.MAX_VALUE/count) return;//resulting multiplication causes overflow
						
						//check if enough cash, if so go through with sale
						int buyAmount = sellPrice*count;
						boolean bought = false;
						synchronized (cashDat.lock) {
							int cash = Integer.parseInt(cashDat.data);
							if (cash >= buyAmount) {
								bought = true;
								cashDat.data = ""+(cash-buyAmount);
							}
						}
						
						//add items to players backpack
						if (bought) {
							if (piid == -1) {
								player.inventory.add(new InventoryItem(shopItems[iid],0,count));
							} else {
								player.inventory.get(piid).count += count;
							}
							playerMonData.data = player.asString();
							buyMsg = "Bought "+count+" "+idef.name+" for $"+buyAmount+".";
						} else {
							buyMsg = "Couldn't buy item, not enough chroniccash.";
						}
					}
					
					Main.chronicBot.sendMessage(buyMsg, null, m.getChannelReceiver());

					
				} else if (avars[0].equals("sell")) {	
					
					
					//sell one of your items
					UserData udat = Main.chronicBot.getUserData(m.getAuthor().getId(), true);
					DataObject playerMonData = udat.data.get("battlemon");
					DataObject cashDat = udat.data.get("cash");

					//not registered in either battlemon or chroniccash
					if (playerMonData == null) {
						Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
						return;
					}
					if (cashDat == null) {
						Main.chronicBot.sendMessage("You need to register for chroniccash using !chroniccash before you can do this.", null, m.getChannelReceiver());
						return;
					}
					
					
					int count = 1;
					//count of item to sell
					if (avars.length > 2) {
						try {
							count = Integer.parseInt(avars[2]);
						} catch (NumberFormatException ex) {return;}
					}
					
					String soldMsg = "";
					synchronized (playerMonData.lock) {
						Player player = new Player(playerMonData.data);
						
						//find item to sell
						int iid = player.findItem(avars[1]);
						if (iid == -1) return;//no item in backpack with that name
						
						InventoryItem iitem = player.inventory.get(iid);
						if (iitem.count < count) {
							//not enough of that item to sell that many
							Main.chronicBot.sendMessage("You don't have that many of that item to sell.", null, m.getChannelReceiver());
							return;
						}
						ItemDef idef = MonDef.ITEMS[iitem.id];
						int sellPrice = Math.round(idef.shopCost/4.0f);
						if (sellPrice > Integer.MAX_VALUE/count) return;//resulting multiplication causes overflow
						
						//add cash from sale
						int sellAmount = sellPrice*count;
						synchronized (cashDat.lock) {
							cashDat.data = ""+(Integer.parseInt(cashDat.data)+sellAmount);
						}
						
						//remove item
						iitem.count -= count;
						if (iitem.count == 0) player.inventory.remove(iid);
						
						playerMonData.data = player.asString();
						soldMsg = "Sold "+count+" "+idef.name+" for $"+sellAmount+".";
					}
					
					Main.chronicBot.sendMessage(soldMsg, null, m.getChannelReceiver());
					
					
				} else if (avars[0].equals("price")) {
					
					
					//how much you get if you sell one of your items to the shop
					int idi = MonDef.findItemDef(avars[1]);
					if (idi == -1) return;//no item found with that name
					
					ItemDef idef = MonDef.ITEMS[idi];
					Main.chronicBot.sendMessage("**"+idef.name+"** sells to the shop for $"+((int)Math.round(idef.shopCost/4.0f))+".", null, m.getChannelReceiver());
				
				
				}
			}
			
		} else if (subCmd.equals("party")) {
		
			//list mon in party/register
			UserData udat = Main.chronicBot.getUserData(m.getAuthor().getId(), true);
			DataObject playerMonData = udat.data.get("battlemon");
			
			if (playerMonData == null) {
				
				//register new player
				udat.data.put("battlemon", new DataObject(Player.spawn().asString()));
				Main.chronicBot.sendMessage("Registered for Battlemon!", null, m.getChannelReceiver());
			
			} else {
				
				//list mon in party
				Player player = new Player(playerMonData.data);
				String partyMsg = "<@"+m.getAuthor().getId()+"> __party__:\n\n";
				for (int i = 0; i < player.nMon; i++) {
					Mon mon = player.mon[i];
					MonDef mdef = MonDef.MON[mon.id];
					partyMsg += "**"+(mon.nickname==null?mdef.name.toUpperCase():mon.nickname)+"**"+
					" - LV"+mon.level+
					" - "+mon.hp+"/"+Mon.calculateHP(mdef.baseStats[MonDef.STAT_HP], mon.iv[MonDef.STAT_HP], mon.ev[MonDef.STAT_HP], mon.level)+"HP"+
					"\n\n";
				}
				
				Main.chronicBot.sendMessage(partyMsg, null, m.getChannelReceiver());
		
			}
			
		} else if (subCmd.equals("items")) {
		
			//list items in backpack
			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			//not registered in either battlemon or chroniccash
			if (playerMonData == null) {
				Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
				return;
			}
			
			Player player = new Player(playerMonData.data);
			String itemsStr = "<@"+m.getAuthor().getId()+">'s __backpack__:\n";
			for (int i = 0; i < player.inventory.size(); i++) {
				InventoryItem item = player.inventory.get(i);
				ItemDef idef = MonDef.ITEMS[item.id];
				itemsStr += "**"+idef.name+"** x"+item.count+" - "+idef.description+"\n";
			}
			
			Main.chronicBot.sendMessage(itemsStr, null, m.getChannelReceiver());
		
		} else if (subCmd.equals("help")) {
			
			//list all sub commands and general info about battlemon command
			if (helpMessage == null) {
				//build help message
				helpMessage = "__Battlemon__ is a turn-based monster battle RPG. To begin playing type !mon party and !chroniccash, for full instructions check out *insert video/some tutorial*.\n\n"
						+ "**List of Commands**:\n\n"
						+ "**help** - This command, lists helpful information about Battlemon.\n\n"
						+ "**grab** - Picks up an item off of the ground.\n\n"
						+ "**catch** - Throws a ball and attempts to catch a wild Battlemon, format: 'catch ball' example: '!mon catch masterball'.\n\n"
						+ "**items** - Lists all of the items in your backpack.\n\n"
						+ "**party** - Lists all Battlemon currently with you also registers you for Battlemon.\n\n"
						+ "**shop** - Accesses the Battlemon shop, view all items available, buy and sell. Format: 'shop buy/sell/price itemname/# count', example: '!mon buy 1 10' to buy 10 of shop item #1. Example: '!mon price Masterball' to get how much you can sell a Masterball to the store for.\n\n"
						+ "**release** - Releases a Battlemon from your party. Format: 'release pokemonname' example: '!mon release raichu'.\n\n"
						+ "**battle** - Initiates a battle with a wild Battlemon or another player, optionally betting cash. Or lists info of current battle. Format: 'battle opponentname amount', example: '!mon battle' to battle a wild Battlemon. Example: '!mon battle @ChronicBOT 500' to battle against ChronicBOT with a stake of 500.\n\n"
						+ "**move** - Uses a Battlemon move in a battle. Format: 'move movename' example: '!mov move tackle'.\n\n"
						+ "**switch** - Switches out the active Battlemon in battle. Format: 'switch battlemonname' example: '!mon switch abra' to send out Abra.\n\n"
						+ "**order** - Switches the order of Battlemon in your party. Format: 'order battlemonname1 battlemonname2' example: '!mon order abra kadabra'.\n\n";
				helpMessage2 = "**trade** - Trade items/battlemon/chroniccash to other players.\n\n"
						+ "**storage** - Access Battlemon storage, format: 'storage page/deposit/withdraw page#/battlemonname/storagenumber. Example: '!mon storage withdraw 1' to withdraw the first Battlemon in storage.\n\n"
						+ "**inspect** - Inspects the stats and moves of a Battlemon in your party. Format: 'inspect battlemonname' example: '!mon inspect pidgey'.\n\n"
						+ "**heal** - Heal's all Battlemon in your party.\n\n"
						+ "**cancel** - Cancel's a battle against another player.";
			}
			Main.chronicBot.sendMessage(helpMessage, m.getAuthor(), null);
			Main.chronicBot.sendMessage(helpMessage2, m.getAuthor(), null);
		} else if (subCmd.equals("grab")) {
			
			//pick up item off the ground
			if (wildItem == null) return;//no item on ground
			
			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			//not registered in either battlemon or chroniccash
			if (playerMonData == null) {
				Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
				return;
			}
			
			
			boolean pickedUp = false,inventoryFull = false;
			String iname = null;
			synchronized (playerMonData.lock) {
				Player player = new Player(playerMonData.data);
				
				if (player.inventory.size() != player.itemLimit) {
					Item i2grab = null;
					synchronized (wildItemLock) {
						if (wildItem != null) {
							i2grab = wildItem;
							wildItem = null;
							pickedUp = true;
						}
					}
					if (pickedUp) {
						int piid = player.findItem(i2grab.id, i2grab.special);
						if (piid == -1) {
							player.inventory.add(new InventoryItem(i2grab.id, i2grab.special, 1));
						} else {
							player.inventory.get(piid).count++;
						}
						iname = MonDef.ITEMS[i2grab.id].name;
					}
				} else {
					inventoryFull = true;
				}
				
				//save player data
				playerMonData.data = player.asString();
			}
			
			//send response message
			if (pickedUp) {
				Main.chronicBot.sendMessage("<@"+m.getAuthor().getId()+"> picked up "+iname.substring(0,1).toUpperCase()+iname.substring(1)+" off the ground.", null, m.getChannelReceiver());
			} else if (inventoryFull) {
				Main.chronicBot.sendMessage("<@"+m.getAuthor().getId()+"> couldn't pick up item because backpack is full!", null, m.getChannelReceiver());
			}
			
		} else if (subCmd.equals("catch") || subCmd.equals("throw")) {
			
			//attempts to catch wild mon
			if (wildMon == null || (!wildBattle && System.currentTimeMillis()-wildMonTime > wildMonLength)) {
				//wild mon no longer available
				Main.chronicBot.sendMessage("Wild Battlemon is no longer there.", null, m.getChannelReceiver());
				return;
			}
			
			int ball = 0;
			if (args != null) {
				if (args.startsWith("g")) {
					ball = 1;
				} else if (args.startsWith("u")) {
					ball = 2;
				}
			}
			//String ballName = MonDef.ITEMS[ball].name;

			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			//not registered in either battlemon or chroniccash
			if (playerMonData == null) {
				Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
				return;
			}
			
			
			String caughtMsg = "Threw ball and...\n";
			synchronized (playerMonData.lock) {
				synchronized (wildMonLock) {
					if (wildMon == null) return;//no wild mon
					Player player = new Player(playerMonData.data);
					
					if (wildBattle && !(player.battleOpponent != null && m.getAuthor().getId().equals(wildBattleUID))) return;//not battling wildmon, cant catch
					if (player.nMon == 6 && player.monStorage.size() == 451) {
						//no room for mon
						Main.chronicBot.sendMessage("Can't catch mon, storage is full.", null, m.getChannelReceiver());
						return;
					}
					
					//check if player has that ball
					int iid = player.findItem(ball);
					if (iid == -1) return;//player doesnt have that ball
					
					InventoryItem item = player.inventory.get(iid);
					//remove 1 ball from inventory
					item.count--;
					if (item.count == 0) player.inventory.remove(iid);
				
					//calculate catch rate of mon with ball
					MonDef mdef = MonDef.MON[wildMon.id];
					float catchRate = (1.01f-(((float)Math.pow((wildMon.level/100.0f)*0.75f+(mdef.totalBaseStats/700.0f)*0.25f, 1.0f/9.2f)/(1.0f+ball*0.05f))*0.5f + (float)Math.pow((float)wildMon.hp/(float)wildMon.hp(), 2.0f)*0.5f));
					if (Math.random() < catchRate) {
						//caught pokemon
						caughtMsg += "Caught **"+mdef.name.toUpperCase()+"**!";
						//add to party or send to storage
						if (player.nMon == 6) {
							//send to storage
							player.monStorage.add(wildMon);
							caughtMsg += " Sent to storage.";
						} else {
							player.mon[player.nMon] = wildMon;
							player.nMon++;
						}
						if (wildBattle && (player.battleOpponent != null && m.getAuthor().getId().equals(wildBattleUID))) {
							player.battleOpponent = null;
						}
						wildMon = null;
					} else {
						//didnt catch pokemon
						caughtMsg += "Failed to catch **"+mdef.name.toUpperCase()+"**.";
					
						if (wildBattle && (player.battleOpponent != null && m.getAuthor().getId().equals(wildBattleUID))) {
							//do wild mon move in battle
							//generate random wild pokemon move
							Mon amon = player.mon[player.activeMon];
							int rmove = (int) (Math.random()*4);
							while (wildMon.moves[rmove] == -1) rmove = (int) (Math.random()*4);
							rmove = wildMon.moves[rmove];

								String mmsg = MonDef.MOVES[rmove].apply(wildMon, amon);
								if (amon.hp <= 0) {
									//player mon died
									amon.hp = 0;
									//check if last battlemon and done battle
									if (player.whitedOut()) {
										Main.chronicBot.sendMessage(caughtMsg+"\n"+mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** fainted. That was your last Battlemon! You lost the battle.", null, m.getChannelReceiver());

										player.battleOpponent = null;
										wildMon = null;
										wildBattle = false;
										
									} else {
										for (int i = 0; i < player.nMon; i++) {
											if (player.mon[i].hp > 0) {
												player.activeMon = (byte) i;
												break;
											}
										}
										Main.chronicBot.sendMessage(caughtMsg+"\n"+mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** fainted. Sent out **"+MonDef.MON[player.mon[player.activeMon].id].name.toUpperCase()+"**, use !mon switch to switch out another Battlemon.", null, m.getChannelReceiver());
									}
									playerMonData.data = player.asString();
									return;
								}
								
								Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** now has "+amon.hp+"/"+amon.hp()+".", null, m.getChannelReceiver());
							
						} else {
							Main.chronicBot.sendMessage("**"+MonDef.MON[wildMon.id].name.toUpperCase()+"** runs away.", null, Main.chronicBot.getHomeChannel());
							wildMon = null;
						}
					
					}
					
					playerMonData.data = player.asString();
				}
			}
			
			Main.chronicBot.sendMessage(caughtMsg, null, m.getChannelReceiver());
		} else if (subCmd.equals("release")) {
			
			if (args == null || args.length() < 2) return;
			
			//release battlemon from party
			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			if (playerMonData == null) return;//not registered
			
			synchronized (playerMonData.lock) {
				Player player = new Player(playerMonData.data);
				
				int mid = player.findMon(args);
				if (mid == -1) return;//no mon with that name
				
				//release
				for (int i = mid; i < player.nMon-1; i++) {
					player.mon[i] = player.mon[i+1];
				}
				player.nMon--;
				player.mon[player.nMon] = null;
				
				//save
				playerMonData.data = player.asString();
			}
			
			Main.chronicBot.sendMessage("**"+args.toUpperCase()+"** was released.", null, m.getChannelReceiver());
			
		} else if (subCmd.equals("move") || subCmd.equals("attack")) {
						
			//uses a battlemon move in battle
			if (args == null || args.length() < 2) return;//not valid args
			
			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			if (playerMonData == null) return;//not registered
			
			synchronized (playerMonData.lock) {
				Player player = new Player(playerMonData.data);
				
				if (player.battleOpponent == null) return;//no battle opponent
				
				//find move id from mon's move names
				Mon amon = player.mon[player.activeMon];

				int mid = amon.findMove(args);
				if (mid == -1) return;//cant find that move

				if (player.battleOpponent.charAt(0) == 'w') {
					mid = amon.moves[mid];
					
					//if no wildmon in battle, end battle mon ran away
					synchronized (wildMonLock) {
						if (wildMon == null || !wildBattle || !wildBattleUID.equals(m.getAuthor().getId())) {
							//wild mon no longer available
							Main.chronicBot.sendMessage("Wild Battlemon is no longer there.", null, m.getChannelReceiver());
							return;
						}
						
						//wild battle
						//do moves
						int speed1 = amon.stat(MonDef.STAT_SPEED),
							speed2 = wildMon.stat(MonDef.STAT_SPEED);
						
						//generate random wild pokemon move
						int rmove = (int) (Math.random()*4);
						while (wildMon.moves[rmove] == -1) rmove = (int) (Math.random()*4);
						rmove = wildMon.moves[rmove];
						
						//apply moves effects in order based on speed
						if (speed1 >= speed2) {
							//apply player move
							String mmsg = MonDef.MOVES[mid].apply(amon,  wildMon);
							if (wildMon.hp <= 0) {
								//wild mon died
								Main.chronicBot.sendMessage(mmsg+"\nWild **"+MonDef.MON[wildMon.id].name.toUpperCase()+"** fainted. You won the battle.", null, m.getChannelReceiver());

								player.battleOpponent = null;
								wildMon = null;
								wildBattle = false;
								playerMonData.data = player.asString();
								return;
							}
							
							//apply wild mon move
							mmsg += "\n"+MonDef.MOVES[rmove].apply(wildMon, amon);
							if (amon.hp <= 0) {
								//player mon died
								amon.hp = 0;
								//check if last battlemon and done battle
								if (player.whitedOut()) {
									Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** fainted. That was your last Battlemon! You lost the battle.", null, m.getChannelReceiver());

									player.battleOpponent = null;
									wildMon = null;
									wildBattle = false;
									
									playerMonData.data = player.asString();

								} else {
									for (int i = 0; i < player.nMon; i++) {
										if (player.mon[i].hp > 0) {
											player.activeMon = (byte) i;
											break;
										}
									}
									Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** fainted. Sent out **"+MonDef.MON[player.mon[player.activeMon].id].name.toUpperCase()+"**, use !mon switch to switch out another Battlemon.", null, m.getChannelReceiver());
								}
								playerMonData.data = player.asString();
								return;
							}
							
							Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[wildMon.id].name.toUpperCase()+"** now has "+wildMon.hp+"/"+wildMon.hp()+" and **"+MonDef.MON[amon.id].name.toUpperCase()+"** now has "+amon.hp+"/"+amon.hp()+".", null, m.getChannelReceiver());
						} else {
							String mmsg = MonDef.MOVES[rmove].apply(wildMon, amon);
							if (amon.hp <= 0) {
								//player mon died
								amon.hp = 0;
								//check if last battlemon and done battle
								if (player.whitedOut()) {
									Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** fainted. That was your last Battlemon! You lost the battle.", null, m.getChannelReceiver());

									player.battleOpponent = null;
									wildMon = null;
									wildBattle = false;
									
								} else {
									for (int i = 0; i < player.nMon; i++) {
										if (player.mon[i].hp > 0) {
											player.activeMon = (byte) i;
											break;
										}
									}
									Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** fainted. Sent out **"+MonDef.MON[player.mon[player.activeMon].id].name.toUpperCase()+"**, use !mon switch to switch out another Battlemon.", null, m.getChannelReceiver());
								}
								playerMonData.data = player.asString();
								return;
							}
							
							mmsg += "\n"+MonDef.MOVES[mid].apply(amon,  wildMon);
							if (wildMon.hp <= 0) {
								//wild mon died
								Main.chronicBot.sendMessage(mmsg+"\nWild **"+MonDef.MON[wildMon.id].name.toUpperCase()+"** fainted. You won the battle.", null, m.getChannelReceiver());
								wildMon = null;
								wildBattle = false;
								player.battleOpponent = null;
								playerMonData.data = player.asString();
								return;
							}
							Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** now has "+amon.hp+"/"+amon.hp()+" and **"+MonDef.MON[wildMon.id].name.toUpperCase()+"** now has "+wildMon.hp+"/"+wildMon.hp()+".", null, m.getChannelReceiver());
						}
					}
				} else if (player.battleOpponent.charAt(0) == '@') {
					
					//battle against other player
					player.nextMove = (byte) mid;
					
					//check if other player is ready
					int betIndex = player.battleOpponent.indexOf(':');
					String opid,betAmount = null;
					if (betIndex == -1) {
						opid = player.battleOpponent.substring(1);
					} else {
						opid = player.battleOpponent.substring(1,betIndex);
						betAmount = player.battleOpponent.substring(betIndex+1);
					}
					DataObject oplayerMonData = Main.chronicBot.getUserData(opid, true).data.get("battlemon");					
					synchronized (oplayerMonData.lock) {
						Player oplayer = new Player(oplayerMonData.data);
						if (oplayer.nextMove != -1) {//ready
							
							Mon omon = oplayer.mon[oplayer.activeMon];
							
							mid = amon.moves[mid];
							int rmove = omon.moves[oplayer.nextMove];
							
							oplayer.nextMove = -1;
							player.nextMove = -1;
						
							//ready do battle moves
							int speed1 = amon.stat(MonDef.STAT_SPEED),
									speed2 = omon.stat(MonDef.STAT_SPEED);
								
								//apply moves effects in order based on speed
								if (speed1 >= speed2) {
									//apply player move
									String mmsg = MonDef.MOVES[mid].apply(amon,  omon);
									if (omon.hp <= 0) {
										//player mon died
										omon.hp = 0;
										//check if last battlemon and done battle
										if (oplayer.whitedOut()) {
											Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[omon.id].name.toUpperCase()+"** fainted. That was your last Battlemon, battle is over! <@"+m.getAuthor().getId()+"> won"+(betAmount==null?"!":" $"+betAmount+"!"), null, m.getChannelReceiver());
											
											oplayer.battleOpponent = null;
											player.battleOpponent = null;
										} else {
											for (int i = 0; i < oplayer.nMon; i++) {
												if (oplayer.mon[i].hp > 0) {
													oplayer.activeMon = (byte) i;
													break;
												}
											}
											Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[omon.id].name.toUpperCase()+"** fainted. Sent out **"+MonDef.MON[oplayer.mon[oplayer.activeMon].id].name.toUpperCase()+"**, use !mon switch to switch out another Battlemon.", null, m.getChannelReceiver());
										}
										playerMonData.data = player.asString();
										oplayerMonData.data = oplayer.asString();
										return;
									}
									
									//apply wild mon move
									mmsg += "\n"+MonDef.MOVES[rmove].apply(omon, amon);
									if (amon.hp <= 0) {
										//player mon died
										amon.hp = 0;
										//check if last battlemon and done battle
										if (player.whitedOut()) {
											Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** fainted. That was your last Battlemon! You lost the battle.", null, m.getChannelReceiver());
	
											oplayer.battleOpponent = null;
											player.battleOpponent = null;
										} else {
											for (int i = 0; i < player.nMon; i++) {
												if (player.mon[i].hp > 0) {
													player.activeMon = (byte) i;
													break;
												}
											}
											Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** fainted. Sent out **"+MonDef.MON[player.mon[player.activeMon].id].name.toUpperCase()+"**, use !mon switch to switch out another Battlemon.", null, m.getChannelReceiver());
										}
										playerMonData.data = player.asString();
										oplayerMonData.data = oplayer.asString();
										return;
									}
									
									Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** now has "+amon.hp+"/"+amon.hp()+" and **"+MonDef.MON[omon.id].name.toUpperCase()+"** now has "+omon.hp+"/"+omon.hp()+".", null, m.getChannelReceiver());
								} else {
									String mmsg = MonDef.MOVES[rmove].apply(omon, amon);
									if (amon.hp <= 0) {
										//player mon died
										amon.hp = 0;
										//check if last battlemon and done battle
										if (player.whitedOut()) {
											Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** fainted. That was your last Battlemon! You lost the battle.", null, m.getChannelReceiver());
	
											oplayer.battleOpponent = null;
											player.battleOpponent = null;
										} else {
											for (int i = 0; i < player.nMon; i++) {
												if (player.mon[i].hp > 0) {
													player.activeMon = (byte) i;
													break;
												}
											}
											Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** fainted. Sent out **"+MonDef.MON[player.mon[player.activeMon].id].name.toUpperCase()+"**, use !mon switch to switch out another Battlemon.", null, m.getChannelReceiver());
										}
										playerMonData.data = player.asString();
										oplayerMonData.data = oplayer.asString();
										return;
									}
									
									mmsg += "\n"+MonDef.MOVES[mid].apply(amon,  omon);
									if (omon.hp <= 0) {
										//player mon died
										omon.hp = 0;
										//check if last battlemon and done battle
										if (oplayer.whitedOut()) {
											Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[omon.id].name.toUpperCase()+"** fainted. That was your last Battlemon! You lost the battle.", null, m.getChannelReceiver());
	
											oplayer.battleOpponent = null;
											player.battleOpponent = null;
										} else {
											for (int i = 0; i < oplayer.nMon; i++) {
												if (oplayer.mon[i].hp > 0) {
													oplayer.activeMon = (byte) i;
													break;
												}
											}
											Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[omon.id].name.toUpperCase()+"** fainted. Sent out **"+MonDef.MON[oplayer.mon[oplayer.activeMon].id].name.toUpperCase()+"**, use !mon switch to switch out another Battlemon.", null, m.getChannelReceiver());
										}
										playerMonData.data = player.asString();
										oplayerMonData.data = oplayer.asString();
										return;
									}
									Main.chronicBot.sendMessage(mmsg+"\n**"+MonDef.MON[amon.id].name.toUpperCase()+"** now has "+amon.hp+"/"+amon.hp()+" and **"+MonDef.MON[omon.id].name.toUpperCase()+"** now has "+omon.hp+"/"+omon.hp()+".", null, m.getChannelReceiver());
								}
													
							oplayerMonData.data = oplayer.asString();
						} else {
							Main.chronicBot.sendMessage("Move accepted, waiting for opponents move.", null, m.getChannelReceiver());
						}
					}
				}
				
				playerMonData.data = player.asString();
			}
		} else if (subCmd.equals("storage")) {
			
			//access battlemon storage
			String[] arg;
			if (args == null) {
				arg = new String[] {""};
			} else {
				arg = args.split(" ");
			}
			
			int argcmd = 0,viewn = 1;
			if (arg[0].equalsIgnoreCase("deposit")) {
				argcmd = 1;
				if (arg.length < 2) return;//not enough args
			} else if (arg[0].equalsIgnoreCase("withdraw")) {
				argcmd = 2;
			}
			
			if (argcmd == 0 || argcmd == 2) {
				if (arg.length > 1) {
					try {
						viewn = Integer.parseInt(arg[1]);
					} catch (NumberFormatException e) {return;}
					if (viewn < 1) return;//not valid
				}
			}
			
			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			//not registered in either battlemon or chroniccash
			if (playerMonData == null) {
				Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
				return;
			}
			
			synchronized (playerMonData.lock) {
				Player player = new Player(playerMonData.data);
				
				if (player.battleOpponent != null && player.battleOpponent.charAt(0) != '?') {
					Main.chronicBot.sendMessage("Cannot access storage in battle.", null, m.getChannelReceiver());
					return;
				}
				
				if (argcmd == 0) {
					//view storage page
					int ssize = (int) Math.ceil(player.monStorage.size()/10.0f);
					if (viewn > ssize) return;//check if storage out of bounds
					
					//display page 
					String storageMsg = "Storage page "+viewn+"/"+ssize+":\n";
					int bindex = (viewn-1)*10,
							mi = player.monStorage.size()-bindex;
					for (int i = 0; i < 10; i++) {
						if (i >= mi) break;//finished all
						Mon mstore = player.monStorage.get(i+bindex);
						storageMsg += "#__"+(bindex+i+1)+"__  LV"+mstore.level+" **"+MonDef.MON[mstore.id].name.toUpperCase()+"**\n";
					}
					//send storage display
					Main.chronicBot.sendMessage(storageMsg, null, m.getChannelReceiver());
				} else if (argcmd == 1) {
					//deposit from party
					int mid = player.findMon(arg[1]);
					if (mid == -1) return;//no battlemon with that name
					
					//deposit mon to storage
					if (player.monStorage.size() == 451) return;//no room in storage
					player.monStorage.add(player.mon[mid]);
					for (int i = mid; i < player.nMon-1; i++) {
						player.mon[i] = player.mon[i+1];
					}
					player.nMon--;
					player.mon[player.nMon] = null;
					
					Main.chronicBot.sendMessage("Deposited **"+arg[1].toUpperCase()+"** in storage.", null, m.getChannelReceiver());
					playerMonData.data = player.asString();
				} else if (argcmd == 2) {
					//withdraw from storage
					viewn--;
					if (player.monStorage.size() <= viewn) return;//cant withdraw that #
					if (player.nMon == 6) return;//no room in party
					
					player.mon[player.nMon] = player.monStorage.get(viewn);
					player.nMon++;
					player.monStorage.remove(viewn);
					
					Main.chronicBot.sendMessage("Withdrew #__"+arg[1]+"__ from storage.", null, m.getChannelReceiver());
					playerMonData.data = player.asString();
				}
			}
			
		} else if (subCmd.equals("order")) {
			
			if (args == null) return;
			String[] mnames = args.split(" ");
			if (mnames.length < 2) return;//not enough args
			
			//swaps order of battlemon in party
			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			//not registered in either battlemon or chroniccash
			if (playerMonData == null) {
				Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
				return;
			}
			
			synchronized (playerMonData.lock) {
				Player player = new Player(playerMonData.data);
				
				int mid1 = player.findMon(mnames[0]);
				if (mid1 == -1) return;//no battlemon with that name
				
				int mid2 = player.findMon(mnames[1]);
				if (mid2 == -1) return;//no battlemon with that name
				
				//swap order
				Mon mbuf = player.mon[mid1];
				player.mon[mid1] = player.mon[mid2];
				player.mon[mid2] = mbuf;
				
				playerMonData.data = player.asString();
			}
			
			Main.chronicBot.sendMessage("Swapped "+mnames[0]+" and "+mnames[1]+".", null, m.getChannelReceiver());
			
		} else if (subCmd.equals("inspect")) {
			
			if (args == null) return;//need argument
			
			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			//not registered in either battlemon or chroniccash
			if (playerMonData == null) {
				Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
				return;
			}
			
			Player player = new Player(playerMonData.data);
			
			int mid = player.findMon(args);
			if (mid == -1) return;//no mon with that name
			
			Mon mon = player.mon[mid];
			MonDef mdef = MonDef.MON[mon.id];
			
			String typeStr = MonDef.TYPE_NAMES[mdef.type1];
			if (mdef.type2 != mdef.type1) {
				typeStr += "/"+MonDef.TYPE_NAMES[mdef.type2];
			}
			String omsg = "LV"+mon.level+" **"+mdef.name.toUpperCase()+"** - "+typeStr+" type - "+mdef.description+"\n"+mdef.imgURL+"\n\n";
			for (int i = 0; i < MonDef.STAT_NAMES.length; i++) {
				if (i == 0) {
					omsg += "__"+MonDef.STAT_NAMES[i].toUpperCase()+"__: "+mon.hp+"/"+Mon.calculateHP(mdef.baseStats[i], mon.iv[i], mon.ev[i], mon.level)+"\n";
				} else {
					omsg += "__"+MonDef.STAT_NAMES[i].toUpperCase()+"__: "+Mon.calculateStat(mdef.baseStats[i], mon.iv[i], mon.ev[i], mon.level)+"\n";
				}
			}
			omsg += "\n\n**Moves**:\n";
			for (int i = 0; i < 4; i++) {
				if (mon.moves[i] != -1) {
					MoveDef vdef = MonDef.MOVES[mon.moves[i]];
					omsg += "__"+vdef.name.toUpperCase()+"__ - "+(vdef.effect==null?"":vdef.effect+" / ")+MonDef.TYPE_NAMES[vdef.type]+" type / "+(vdef.power>-1?vdef.power:0)+" power / "+vdef.accuracy+" accuracy / "+vdef.pp+" pp\n\n";
				}
			}
			
			Main.chronicBot.sendMessage(omsg, null, m.getChannelReceiver());
			
		} else if (subCmd.equals("heal")) {
			
			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			//not registered in either battlemon or chroniccash
			if (playerMonData == null) {
				Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
				return;
			}
			
			synchronized (playerMonData.lock) {
				Player player = new Player(playerMonData.data);
				if (player.battleOpponent != null || player.nMon == 0) {
					//dont allow in battle and with 0 mon
					Main.chronicBot.sendMessage("You cannot heal while in battle.", null, m.getChannelReceiver());
					return;
				}

				for (int i = 0; i < player.nMon; i++) {
					player.mon[i].hp = (short) player.mon[i].hp();
				}
				
				playerMonData.data = player.asString();
			}
			
			Main.chronicBot.sendMessage("Your Battlemon have been restored to full health.", null, m.getChannelReceiver());
			
		} else if (subCmd.equals("switch")) {
			
			if (args == null) return;//need mon argument
			
			//switch out active mon in battle
			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			//not registered in either battlemon or chroniccash
			if (playerMonData == null) {
				Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
				return;
			}
			
			synchronized (playerMonData.lock) {
				Player player = new Player(playerMonData.data);
				if (player.battleOpponent == null) return;//not in battle
				
				int nmid = player.findMon(args);
				if (nmid == -1 || player.mon[nmid].hp <= 0) {
					//couldnt find mon with that name or mon is fainted
					Main.chronicBot.sendMessage("That Battlemon is not able to fight.", null, m.getChannelReceiver());
					return;
				}
				player.activeMon = (byte) nmid;
								
				playerMonData.data = player.asString();
			}
			
			Main.chronicBot.sendMessage("Sent out **"+args.toUpperCase()+"**.", null, m.getChannelReceiver());
			
		} else if (subCmd.equals("cancel")) {
			
			//cancel battle
			DataObject playerMonData = Main.chronicBot.getUserData(m.getAuthor().getId(), true).data.get("battlemon");
			//not registered in either battlemon or chroniccash
			if (playerMonData == null) {
				Main.chronicBot.sendMessage("You need to register for Battlemon using !mon party before you can do this.", null, m.getChannelReceiver());
				return;
			}
			
			synchronized (playerMonData.lock) {
				Player player = new Player(playerMonData.data);
				if (player.battleOpponent == null || (player.battleOpponent.charAt(0) != '@' && player.battleOpponent.charAt(0) != '?')) {
					Main.chronicBot.sendMessage("Cannot run away!", null, m.getChannelReceiver());
					return;
				}
				
				int eindex = player.battleOpponent.indexOf(':');
				if (eindex == -1) eindex = player.battleOpponent.length();
				else {
					Main.chronicBot.sendMessage("Cannot cancel battle with money bet on it.", null, m.getChannelReceiver());
					return;
				}
				String opId = player.battleOpponent.substring(1,eindex);
				
				UserData opData = Main.chronicBot.getUserData(opId, false);
				if (opData == null) return;//invalid opponent
				DataObject monData = opData.data.get("battlemon");
				if (monData == null) return;//invalid opponent
				
				
				synchronized (monData.lock) {
					Player oplayer = new Player(monData.data);
					
					oplayer.battleOpponent = null;
					player.battleOpponent = null;
					
					monData.data = oplayer.asString();
				}
				
				
				playerMonData.data = player.asString();
			}
			
		}
	}

	@Override
	public String name() {
		return "mon";
	}

	@Override
	public String description() {
		return "Text-based monster battles, do !mon help for more information.";
	}


	//80% chance to spawn mon or item every 3 minutes
	public void actionPerformed(ActionEvent e) {
		if (Math.random() < 0.2) return;//20% chance not to do anything
		
		int spawnType = (int) (Math.random()*2);
		switch (spawnType) {
		case 0:
			//spawn mon
			synchronized (wildMonLock) {
				if (wildMon != null && wildBattle && wildBattleUID != null) {
					DataObject playerMonData = Main.chronicBot.getUserData(wildBattleUID, true).data.get("battlemon");
					if (playerMonData != null) {
						synchronized (playerMonData.lock) {
							Player player = new Player(playerMonData.data);
							player.battleOpponent = null;
							playerMonData.data = player.asString();
						}
						Main.chronicBot.sendMessage("**"+MonDef.MON[wildMon.id].name.toUpperCase()+"** runs away.", null, Main.chronicBot.getHomeChannel());
					}
				}
				wildBattle = false;
				wildBattleUID = null;
				wildMonTime = System.currentTimeMillis();
				wildMonLength = (int) (Math.random()*140+45) * 1000;
		
				//spawn random mon
				Mon mon = Mon.spawn();
				MonDef mdef = MonDef.MON[mon.id];
				Main.chronicBot.sendMessage("A wild __**"+mdef.name.toUpperCase()+"**__(level "+mon.level+") appeared!\n"+mdef.imgURL, null, Main.chronicBot.getHomeChannel());
				
				wildMon = mon;
			}
			break;
			
		case 1:
			//drop item
			if (wildItem != null) return;//item already dropped
			
			//spawn random item
			wildItem = Item.spawn();
			ItemDef idef = MonDef.ITEMS[wildItem.id];
			Main.chronicBot.sendMessage(idef.name+" on the ground.\n"+idef.imgURL, null, Main.chronicBot.getHomeChannel());
			break;
			
		case 2:
			//5% chance of rare shop item
			shopItem = Math.random()<0.05?Item.spawnShopItem():-1;
			if (shopItem > -1) {
				ItemDef id = MonDef.ITEMS[shopItem];
				specialShopItemsText = "#? **"+id.name+" - $"+id.shopCost;
			} else {
				specialShopItemsText = "";
			}
			break;
		}
	}

/*
	public static void genMonStuff() {
		System.out.println("new byte[][] {");
		int nTypes = MonDef.TYPE_NAMES.length;
		for (int i = 0; i < nTypes; i++) {
			byte[] effectiveness = new byte[nTypes*nTypes];
			String es = "{";
			
			String html = BotUtils.HttpGet("http://pokemondb.net/type/"+MonDef.TYPE_NAMES[i], "");
			if (html == null) {
				System.out.println("HTML error.");
				return;
			}
			
			int findex;
			
			//iterate through all table entries and record effectiveness
			for (int k = 0; k < nTypes*nTypes; k++) {
				findex = html.indexOf("<td ");
				if (findex == -1) return;//something went wrong
				findex = html.indexOf("class=", findex);
				if (findex == -1) return;//something went wrong

				html = html.substring(findex+28);
				
				String effectId = html.substring(0,html.indexOf('"'+">"));
				byte effect;
				if (effectId.equals("25")) {
					effect = MonDef.TYPE_EFFECTIVENESS_QUARTER_EFFECTIVE;
				} else if (effectId.equals("50")) {
					effect = MonDef.TYPE_EFFECTIVENESS_HALF_EFFECTIVE;
				} else if (effectId.equals("200")) {
					effect = MonDef.TYPE_EFFECTIVENESS_2X_EFFECTIVE;
				} else if (effectId.equals("400")) {
					effect = MonDef.TYPE_EFFECTIVENESS_4X_EFFECTIVE;
				} else {
					effect = MonDef.TYPE_EFFECTIVENESS_NORMAL;
				}
				effectiveness[k] = effect;
				es += effect+(k<nTypes*nTypes-1?",":"");
			}
			System.out.println(es+"}"+(i<nTypes-1?",":""));
		}
		System.out.println("};");
	}
*/
}



class Player {
	
	ArrayList<InventoryItem> inventory = new ArrayList<InventoryItem>();
	int itemLimit = 20;
	
	byte nMon;
	Mon[] mon = new Mon[6];
	ArrayList<Mon> monStorage = new ArrayList<Mon>();
	
	String battleOpponent = null;
	byte activeMon = -1,
		 nextMove = -1;
	
	Player() {}
	Player(String lstr) {
		//load player from string
		String[] svars = lstr.split(",");

		int index = 1,
			nItems = Integer.parseInt(svars[0]);
		for (int i = 0; i < nItems; i++) {
			inventory.add(new InventoryItem(svars[index]));
			index++;
		}
		
		itemLimit = Integer.parseInt(svars[index]);
		index++;
		
		nMon = Byte.parseByte(svars[index]);
		index++;
		for (int i = 0; i < nMon; i++) {
			mon[i] = new Mon(svars[index]);
			index++;
		}
		
		nItems = Integer.parseInt(svars[index]);
		index++;
		for (int i = 0; i < nItems; i++) {
			monStorage.add(new Mon(svars[index]));
			index++;
		}
		
		if (svars[index].length() == 0) {
			battleOpponent = null;
		} else {
			battleOpponent = svars[index];
		}
		
		index++;
		
		activeMon = Byte.parseByte(svars[index]);
		index++;
		nextMove = Byte.parseByte(svars[index]);
	}
	
	
	//check if all mon in party fainted
	boolean whitedOut() {
		for (int i = 0; i < nMon; i++) {
			if (mon[i].hp > 0) return false;//mon not fainted
		}
		return true;
	}
	
	
	//convert player to string
	String asString() {
		String s = "";
		
		//output items
		int nItems = inventory.size();
		s += nItems;
		if (nItems != 0) s += ",";
		for (int i = 0; i < nItems; i++) {
			s += (i==0?"":",")+inventory.get(i).asString();
		}
		
		//output item limit
		s += ","+itemLimit+",";
		
		//output mons and mon storage
		s += nMon;
		for (int i = 0; i < nMon; i++) {
			s += ","+mon[i].asString();
		}
		
		nItems = monStorage.size();
		s += ","+nItems;
		for (int i = 0; i < nItems; i++) {
			s += ","+monStorage.get(i).asString();
		}
		
		s += ","+(battleOpponent==null?"":battleOpponent)+","+activeMon+","+nextMove;
		
		return s;
	}
	
	//find item in inventory by id, returns slot of item found -1 if none found
	int findItem(int id) {
		for (int i = 0; i < inventory.size(); i++) {
			if (inventory.get(i).id == id) return i;
		}
		return -1;
	}
	
	//find item in invetory by id and special, returns slot of item found -1 if none found
	int findItem(int id, int s) {
		for (int i = 0; i < inventory.size(); i++) {
			InventoryItem ii = inventory.get(i);
			if (ii.id == id && ii.special == s) return i;
		}
		return -1;
	}
	
	//find item in inventory by name(not case sensitive), returns slot id of item found -1 if none found
	int findItem(String name) {
		ItemDef[] ids = MonDef.ITEMS;
		for (int i = 0; i < inventory.size(); i++) {
			if (ids[inventory.get(i).id].name.equalsIgnoreCase(name)) return i;
		}
		return -1;
	}
	
	//find mon in party by name, returns slot or -1 if none found
	int findMon(String name) {
		for (int i = 0; i < nMon; i++) {
			if (MonDef.MON[mon[i].id].name.equalsIgnoreCase(name)) return i;
		}
		return -1;
	}
	
	
	//create new player to spawn
	static Player spawn() {
		Player player = new Player();
		player.nMon = 0;
		return player;
	}
}

class Mon {
	short id;
	short[] moves;
	String nickname = null;
	
	byte[] iv;
	short[] ev;
	byte[] pp;
	
	int exp;
	byte level, effect = -1, volatileEffect = -1, speed, evassiveness;
	short hp;
	
	Mon() {}
	Mon(String lstr) {
		//load mon from string
		String[] svars = lstr.split(":");
		id = Short.parseShort(svars[0]);
		moves = new short[4];
		for (int i = 0; i < 4; i++) {
			moves[i] = Short.parseShort(svars[i+1]);
		}
		if (svars[5].length() == 0) nickname = null;
		else nickname = svars[5];
		iv = new byte[6];
		ev = new short[6];
		for (int i = 0; i < 6; i++) {
			iv[i] = Byte.parseByte(svars[6+i]);
			ev[i] = Short.parseShort(svars[12+i]);
		}
		exp = Integer.parseInt(svars[18]);
		level = Byte.parseByte(svars[19]);
		effect = Byte.parseByte(svars[20]);
		volatileEffect = Byte.parseByte(svars[21]);
		hp = Short.parseShort(svars[22]);
	}
	
	String asString() {
		//mon to string
		String s = id+":"+moves[0]+":"+moves[1]+":"+moves[2]+":"+moves[3]+":"+(nickname==null?"":nickname);
		for (int i = 0; i < 6; i++) {
			s += ":"+iv[i];
		}
		for (int i = 0; i < 6; i++) {
			s += ":"+ev[i];
		}
		s += ":"+exp+":"+level+":"+effect+":"+volatileEffect+":"+hp;
		return s;
	}
	
	
	int findMove(String nm) {
		for (int i = 0; i < 4; i++) {
			if (moves[i] != -1 && MonDef.MOVES[moves[i]].name.equalsIgnoreCase(nm)) return i;
		}
		return -1;
	}
	
	//gets hp stat
	int hp() {
		return calculateHP(MonDef.MON[id].baseStats[0], iv[0], ev[0], level);
	}
	//get stat by id
	int stat(int sid) {
		return calculateStat(MonDef.MON[id].baseStats[sid], iv[sid], ev[sid], level);
	}
	
	
	//spawn mon using spawn rates with random iv's and blank ev's
	static Mon spawn() {
		
		long spawnIndex = (long) ((Math.random()*4096.0f)*(MonDef.TOTAL_MON_SPAWN/4096.0f));
		if (spawnIndex >= MonDef.TOTAL_MON_SPAWN) spawnIndex = MonDef.TOTAL_MON_SPAWN;
				
		//find mon that matches index
		long sum = 0;
		int i;
		for (i = 0; i < MonDef.NUMBER_OF_MON; i++) {
			int base = MonDef.MON[i].spawnChance;
			if (spawnIndex >= sum && spawnIndex < sum+base) {
				//found match
				break;
			}
			
			sum += base;
		}
		
		//init mon
		MonDef mdef = MonDef.MON[i];

		Mon mon = new Mon();
		mon.id = (short) i;
		mon.level = (byte) (Math.pow(Math.random(), 16.0f)*50+Math.pow(Math.random(),1.0f+(mdef.spawnChance/150000.0f)*8.0f)*35+Math.random()*15+1);
		mon.exp = (int) (Math.pow(Math.random(), 4.0)*Mon.getXPNeededForNextLevel(mon.level));
		
		mon.iv = new byte[MonDef.STAT_NAMES.length];
		mon.ev = new short[MonDef.STAT_NAMES.length];
		for (i = 0; i < mon.iv.length; i++) {
			mon.iv[i] = (byte) (Math.random()*32);
			mon.ev[i] = 0;
		}
		
		//spawn at full hp
		mon.hp = (short) Mon.calculateHP(mdef.baseStats[MonDef.STAT_HP], mon.iv[MonDef.STAT_HP], mon.ev[MonDef.STAT_HP], mon.level);
		
		//find appropriate moves for level
		mon.moves = new short[4];
		for (i = 0; i < 4; i++) mon.moves[i] = -1;//no move
				
		int movesLearned = 0;
		for (i = 0; i < mdef.movesLearntByLevel.length; i++) {
			if (mdef.levelMovesLearnt[i] <= mon.level) {
				mon.moves[i] = mdef.movesLearntByLevel[i];
				movesLearned++;
				if (movesLearned == 4) break;
			}
		}
		
		return mon;
	}
	
	
	static int getXPNeededForNextLevel(int level) {
		return (int)Math.pow(level, 3);
	}
	
	static int calculateHP(int b, int i, int e, int l) {
		return (int) Math.floor((2*b + i + e) * l / 100 + l + 10);
	}
	static int calculateStat(int b, int i, int e, int l) {
		return (int) Math.floor((2*b + i + e) * l / 100 + 5);
	}
}
class InventoryItem extends Item {
	int count;
	
	InventoryItem(String s) {
		String[] svars = s.split(":");
		id = Integer.parseInt(svars[0]);
		special = Integer.parseInt(svars[1]);
		count = Integer.parseInt(svars[2]);
	}
	InventoryItem(int i, int s, int c) {
		super(i,s);
		count = c;
	}
	
	//convert item to string
	@Override
	String asString() {
		return id+":"+special+":"+count;
	}
}
class Item {
	int id,special;
	
	Item() {}
	Item(String s) {
		//load item from string
		String[] svars = s.split(":");
		id = Integer.parseInt(svars[0]);
		special = Integer.parseInt(svars[1]);
	}
	Item(int i, int s) {
		id = i;
		special = s;
	}
	
	
	//convert item to string
	String asString() {
		return id+":"+special;
	}
	
	
	//spawn random item using drop spawn rate
	static Item spawn() {
		
		long spawnIndex = (long) ((Math.random()*4096.0f)*(MonDef.ITEM_DROP_TOTAL/4096.0f));
		if (spawnIndex >= MonDef.ITEM_DROP_TOTAL) spawnIndex = MonDef.ITEM_DROP_TOTAL;
				
		//find item that matches index
		long sum = 0;
		int i;
		for (i = 0; i < MonDef.ITEMS.length; i++) {
			int base = MonDef.ITEMS[i].dropChance;
			if (spawnIndex >= sum && spawnIndex < sum+base) {
				//found match
				break;
			}
			
			sum += base;
		}
		
		ItemDef idef = MonDef.ITEMS[i];
		return new Item(i, (int) (Math.random()*idef.special));
	}
	static int spawnShopItem() {
		long spawnIndex = (long) ((Math.random()*4096.0f)*(MonDef.ITEM_SHOP_TOTAL/4096.0f));
		if (spawnIndex >= MonDef.ITEM_SHOP_TOTAL) spawnIndex = MonDef.ITEM_SHOP_TOTAL;
				
		//find item that matches index
		long sum = 0;
		int i;
		for (i = 0; i < MonDef.ITEMS.length; i++) {
			int base = MonDef.ITEMS[i].shopChance;
			if (spawnIndex >= sum && spawnIndex < sum+base) {
				//found match
				break;
			}
			
			sum += base;
		}
		
		if (i >= MonDef.ITEMS.length) i = MonDef.ITEMS.length-1;
		return i;
	}
}
class ItemDef {
	String name,description,imgURL;
	int shopCost,special;//cost in chroniccash
	int dropChance,shopChance;
	
	ItemDef(String n, String d, String url, int spec, int cashCost, int dChance, int sChance) {//if shop cost is 0, not sold in shop
		name = n;
		description = d;
		imgURL = url;
		special = spec;
		shopCost = cashCost;
		dropChance = dChance;
		shopChance = sChance;
	}
}
class MoveDef {
	String name = null,
		   effect = null;
	byte type = MonDef.TYPE_NORMAL;
	int power = 100,
		accuracy = 100,
		pp = 25;
	
	MoveDef(String nm, String effct, byte t, int pow, int acc, int p) {
		name = nm;
		effect = effct;
		type = t;
		power = pow;
		accuracy = acc;
		pp = p;
	}
	
	//apply move to mon r from attacking mon a, returns string explaining move 
	String apply(Mon a, Mon r) {
		int damage = 0;
		float effectiveness = 1.0f;
		boolean crit = false;
		if (power > 0) {
			//damage formula
			float add;
			if (MonDef.TYPE_SPECIAL[type]) {
				add = (float)a.stat(MonDef.STAT_SPATK)/(float)r.stat(MonDef.STAT_SPDEF);
			} else {
				add = (float)a.stat(MonDef.STAT_ATTACK)/(float)r.stat(MonDef.STAT_DEFENSE);
			}
			
			MonDef rmdef = MonDef.MON[r.id];
			int rmtypeIndex = 0;
			if (rmdef.type1 != -1) {
				rmtypeIndex += rmdef.type1;
			}
			if (rmdef.type2 != rmdef.type1) {
				rmtypeIndex += rmdef.type2*MonDef.TYPE_NAMES.length;
			}
			
			crit = Math.random() < 0.05f;
			float mod = crit ? 2.0f : 1.0f;//critical hit mod
			effectiveness = MonDef.TYPE_EFFECTIVENESS_VALUE[MonDef.TYPE_EFFECTIVENESS[type][rmtypeIndex]];
			mod *= effectiveness;//and type effectiveness
			
			damage = (int) Math.ceil(((a.level*2+10)/250.0f) * add * power * mod);
		}
		
		String usedMoveStr = "**"+MonDef.MON[a.id].name.toUpperCase()+"** used "+name;
		if (damage > 0) {
			r.hp -= damage;
			return usedMoveStr+(effectiveness==1.0f?".":(effectiveness>1.0f?" it was super effective.":" it was not very effective."))+" It did "+damage+" damage."+(crit?" Critical hit!":"");
		} else {
			return usedMoveStr+" and nothing happened.";
		}
	}
}
class MonDef {
	
	String name,description,imgURL;
	byte type1,type2;
	short[] baseStats;
	short evolution, totalBaseStats;
	byte evolutionLevel;
	short[] movesLearntByLevel, movesLearntByTM;
	byte[] levelMovesLearnt;
	int spawnChance;
	
	MonDef(String nm, String dsc, String imgUrl, byte t1, byte t2, short[] bstats, short ev, byte evLvl) {
		name = nm;
		description = dsc;
		imgURL = imgUrl;
		type1 = t1;
		type2 = t2;
		baseStats = bstats;
		evolution = ev;
		evolutionLevel = evLvl;
	}
	
	MonDef setMoves(short[] movesLearndByLevel, byte[] levelMovesLearnd, short[] movesLearndByTM) {
		movesLearntByLevel = movesLearndByLevel;
		levelMovesLearnt = levelMovesLearnd;
		movesLearntByTM = movesLearndByTM;
		return this;
	}
	
	void calculateExtras() {
		totalBaseStats = (short)(baseStats[0] + baseStats[1] + baseStats[2] + baseStats[3] + baseStats[4]);
		int spawn = totalBaseStats-200;
		if (spawn < 0) spawn = 0;
		int mul = (spawn-100 <= 0) ? 1 : spawn-100;
		mul *= ((spawn-300)/2 <= 0) ? 1 : (spawn-300)/2;
		spawn *= mul;
		if (spawn > 150000) spawn = 150000;
		spawnChance = 150000-spawn;
	}
	
	
	
	//find item definition by name(not case sensitive) returns -1 if none found
	static int findItemDef(String name) {
		for (int i = 0; i < ITEMS.length; i++) {
			if (ITEMS[i].name.equalsIgnoreCase(name)) return i;
		}
		return -1;
	}
	
	
	
	static long ITEM_DROP_TOTAL = 0,
				ITEM_SHOP_TOTAL = 0;
	static final ItemDef[] ITEMS = new ItemDef[] {
		new ItemDef("Easyball","The most basic Battlemon capture device.", "http://vignette3.wikia.nocookie.net/youtubepoop/images/4/4c/Pokeball.png/revision/latest?cb=20150418234807", 0, 500, 10000, 0),
		new ItemDef("Greatball","A device to capture Battlemon.","https://boost-rankedboost.netdna-ssl.com/wp-content/uploads/2016/07/Great-Ball.png", 0, 2300, 5000, 0),
		new ItemDef("Ultraball","A high quality Battlemon capture device.", "https://boost-rankedboost.netdna-ssl.com/wp-content/uploads/2016/07/Ultra-Ball.png", 0, 10000, 1000, 0)
	};
	
	
	static final String[] EFFECT_NAMES = new String[] {
		"burned",
		"frozen",
		"paralyzed",
		"poisoned",
		"sleep"
	};
	static final String[] VOLATILE_EFFECT_NAMES = new String[] {
		"confused",
		"cursed",
		"encore",
		"flinch"
	};
	
	static final int STAT_HP = 0,
					STAT_ATTACK = 1,
					STAT_DEFENSE = 2,
					STAT_SPATK = 3,
					STAT_SPDEF = 4,
					STAT_SPEED = 5;
	static final String[] STAT_NAMES = new String[] {
		"hp",
		"attack",
		"defense",
		"sp. atk",
		"sp. def",
		"speed"
	};
	
	static final byte TYPE_EFFECTIVENESS_NORMAL = 0,
					  TYPE_EFFECTIVENESS_2X_EFFECTIVE = 1,
					  TYPE_EFFECTIVENESS_4X_EFFECTIVE = 2,
					  TYPE_EFFECTIVENESS_HALF_EFFECTIVE = 3,
					  TYPE_EFFECTIVENESS_QUARTER_EFFECTIVE = 4;
	static final float[] TYPE_EFFECTIVENESS_VALUE = new float[] {
		1.0f,
		2.0f,
		4.0f,
		0.5f,
		0.25f
	};
	static final String[] TYPE_NAMES = new String[] {
		"normal",
		"fire",
		"water",
		"electric",
		"grass",
		"ice",
		"fighting",
		"poison",
		"ground",
		"flying",
		"psychic",
		"bug",
		"rock",
		"ghost",
		"dragon",
		"dark",
		"steel",
		"fairy"
	};
	static final boolean[] TYPE_SPECIAL = new boolean[] {
		false,
		true,
		false,
		true,
		true,
		true,
		false,
		false,
		false,
		false,
		true,
		false,
		false,
		true,
		true,
		true,
		true,
		true
	};
	
	static final int TYPE_NORMAL = 0,
					 TYPE_FIRE = 1,
					 TYPE_WATER = 2,
					 TYPE_ELECTRIC = 3,
					 TYPE_GRASS = 4,
					 TYPE_ICE = 5,
					 TYPE_FIGHTING = 6,
					 TYPE_POISON = 7,
					 TYPE_GROUND = 8,
					 TYPE_FLYING = 9,
					 TYPE_PSYCHIC = 10,
					 TYPE_BUG = 11,
					 TYPE_ROCK = 12,
					 TYPE_GHOST = 13,
					 TYPE_DRAGON = 14,
					 TYPE_DARK = 15,
					 TYPE_STEEL = 16,
					 TYPE_FAIRY = 17,
					 TYPE_NONE = 18;
	
	static final byte[][] TYPE_EFFECTIVENESS = new byte[][] {
		{0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,3,3,3,3,3,3,3,3,3,3,3,3,0,0,3,3,4,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0,3,3,3,3,3,3,3,3,3,3,3,3,4,0,3,3,0,3,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,3,0},
		{0,3,3,0,1,1,0,0,0,0,0,1,3,0,3,0,1,0,3,0,4,3,0,0,3,3,3,3,3,0,4,3,4,3,0,3,3,4,0,3,0,0,3,3,3,3,3,0,4,3,4,3,0,3,0,3,3,0,1,1,0,0,0,0,0,1,3,0,3,0,1,0,1,0,0,1,0,2,1,1,1,1,1,2,0,1,0,1,2,1,1,0,0,1,2,0,1,1,1,1,1,2,0,1,0,1,2,1,0,3,3,0,1,1,0,0,0,0,0,1,3,0,3,0,1,0,0,3,3,0,1,1,0,0,0,0,0,1,3,0,3,0,1,0,0,3,3,0,1,1,0,0,0,0,0,1,3,0,3,0,1,0,0,3,3,0,1,1,0,0,0,0,0,1,3,0,3,0,1,0,0,3,3,0,1,1,0,0,0,0,0,1,3,0,3,0,1,0,1,0,0,1,2,2,1,1,1,1,1,0,0,1,0,1,2,1,3,4,4,3,0,0,3,3,3,3,3,0,0,3,4,3,0,3,0,3,3,0,1,1,0,0,0,0,0,1,3,0,3,0,1,0,3,4,4,3,0,0,3,3,3,3,3,0,4,3,0,3,0,3,0,3,3,0,1,1,0,0,0,0,0,1,3,0,3,0,1,0,1,0,0,1,2,2,1,1,1,1,1,2,0,1,0,1,0,1,0,3,3,0,1,1,0,0,0,0,0,1,3,0,3,0,1,0},
		{0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,1,0,0,1,0,1,1,1,2,1,1,1,2,1,0,1,1,1,3,0,0,3,4,3,3,3,0,3,3,3,0,3,4,3,3,3,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,3,0,4,3,0,3,3,3,0,3,3,3,0,3,4,3,3,3,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,1,2,0,1,0,1,1,1,0,1,1,1,2,1,0,1,1,1,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,1,2,0,1,0,1,1,1,2,1,1,1,0,1,0,1,1,1,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,3,0,4,3,4,3,3,3,0,3,3,3,0,3,0,3,3,3,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0,0,1,3,0,3,0,0,0,1,0,0,0,1,0,3,0,0,0},
		{0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,1,1,0,0,0,1,1,1,0,2,1,1,1,1,0,1,1,1,3,3,0,0,4,3,3,3,0,0,3,3,3,3,4,3,3,3,3,3,0,4,0,3,3,3,0,0,3,3,3,3,4,3,3,3,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,2,0,0,1,1,1,0,0,1,1,1,1,0,1,1,1,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,3,3,0,4,4,3,3,3,0,0,3,3,3,3,0,3,3,3,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0,0,0,1,3,3,0,0,0,0,1,0,0,0,0,3,0,0,0},
		{0,3,1,0,3,0,0,3,1,3,0,3,1,0,3,0,3,0,3,0,0,3,4,3,3,4,0,4,3,4,0,3,4,3,4,3,1,0,0,1,0,1,1,0,2,0,1,0,2,1,0,1,0,1,0,3,1,0,3,0,0,3,1,3,0,3,1,0,3,0,3,0,3,4,0,3,0,3,3,4,0,4,3,4,0,3,4,3,4,3,0,3,1,0,3,0,0,3,1,3,0,3,1,0,3,0,3,0,0,3,1,0,3,0,0,3,1,3,0,3,1,0,3,0,3,0,3,4,0,3,4,3,3,0,0,4,3,4,0,3,4,3,4,3,1,0,2,1,0,1,1,0,0,0,1,0,2,1,0,1,0,1,3,4,0,3,4,3,3,4,0,0,3,4,0,3,4,3,4,3,0,3,1,0,3,0,0,3,1,3,0,3,1,0,3,0,3,0,3,4,0,3,4,3,3,4,0,4,3,0,0,3,4,3,4,3,1,0,2,1,0,1,1,0,2,0,1,0,0,1,0,1,0,1,0,3,1,0,3,0,0,3,1,3,0,3,1,0,3,0,3,0,3,4,0,3,4,3,3,4,0,4,3,4,0,3,0,3,4,3,0,3,1,0,3,0,0,3,1,3,0,3,1,0,3,0,3,0,3,4,0,3,4,3,3,4,0,4,3,4,0,3,4,3,0,3,0,3,1,0,3,0,0,3,1,3,0,3,1,0,3,0,3,0},
		{0,3,3,0,1,3,0,0,1,1,0,0,0,0,1,0,3,0,3,0,4,3,0,4,3,3,0,0,3,3,3,3,0,3,4,3,3,4,0,3,0,4,3,3,0,0,3,3,3,3,0,3,4,3,0,3,3,0,1,3,0,0,1,1,0,0,0,0,1,0,3,0,1,0,0,1,0,0,1,1,2,2,1,1,1,1,2,1,0,1,3,4,4,3,0,0,3,3,0,0,3,3,3,3,0,3,4,3,0,3,3,0,1,3,0,0,1,1,0,0,0,0,1,0,3,0,0,3,3,0,1,3,0,0,1,1,0,0,0,0,1,0,3,0,1,0,0,1,2,0,1,1,0,2,1,1,1,1,2,1,0,1,1,0,0,1,2,0,1,1,2,0,1,1,1,1,2,1,0,1,0,3,3,0,1,3,0,0,1,1,0,0,0,0,1,0,3,0,0,3,3,0,1,3,0,0,1,1,0,0,0,0,1,0,3,0,0,3,3,0,1,3,0,0,1,1,0,0,0,0,1,0,3,0,0,3,3,0,1,3,0,0,1,1,0,0,0,0,1,0,3,0,1,0,0,1,2,0,1,1,2,2,1,1,1,1,0,1,0,1,0,3,3,0,1,3,0,0,1,1,0,0,0,0,1,0,3,0,3,4,4,3,0,4,3,3,0,0,3,3,3,3,0,3,0,3,0,3,3,0,1,3,0,0,1,1,0,0,0,0,1,0,3,0},
		{0,1,1,1,1,2,1,0,1,0,0,0,2,0,1,2,2,0,1,0,0,0,0,1,0,3,0,3,3,3,1,0,0,1,1,3,1,0,0,0,0,1,0,3,0,3,3,3,1,0,0,1,1,3,1,0,0,0,0,1,0,3,0,3,3,3,1,0,0,1,1,3,1,0,0,0,0,1,0,3,0,3,3,3,1,0,0,1,1,3,2,1,1,1,1,0,1,0,1,0,0,0,2,0,1,2,2,0,1,0,0,0,0,1,0,3,0,3,3,3,1,0,0,1,1,3,0,3,3,3,3,0,3,0,3,4,4,4,0,0,3,0,0,4,1,0,0,0,0,1,0,3,0,3,3,3,1,0,0,1,1,3,0,3,3,3,3,0,3,4,3,0,4,4,0,0,3,0,0,4,0,3,3,3,3,0,3,4,3,4,0,4,0,0,3,0,0,4,0,3,3,3,3,0,3,4,3,4,4,0,0,0,3,0,0,4,2,1,1,1,1,2,1,0,1,0,0,0,0,0,1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,3,0,3,3,3,1,0,0,1,1,3,2,1,1,1,1,2,1,0,1,0,0,0,2,0,1,0,2,0,2,1,1,1,1,2,1,0,1,0,0,0,2,0,1,2,0,0,0,3,3,3,3,0,3,4,3,4,4,4,0,0,3,0,0,0},
		{0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,1,1,1,1,0,1,1,0,0,1,1,1,0,0,1,1,0,2,0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,3,3,3,3,0,3,3,0,4,3,3,3,4,4,3,3,0,0,3,3,3,3,0,3,3,4,0,3,3,3,4,4,3,3,0,0,0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,3,3,3,3,0,3,3,4,4,3,3,3,0,4,3,3,0,0,3,3,3,3,0,3,3,4,4,3,3,3,4,0,3,3,0,0,0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,0,0,0,0,1,0,0,3,3,0,0,0,3,3,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,2,1,1,0,0,1,1,1,0,0,1,1,0,0},
		{0,1,0,1,3,0,0,1,0,0,0,3,1,0,0,0,1,0,1,0,1,2,0,1,1,2,1,0,1,0,2,1,1,1,2,1,0,1,0,1,3,0,0,1,0,0,0,3,1,0,0,0,1,0,1,2,1,0,0,1,1,2,1,0,1,0,2,1,1,1,2,1,3,0,3,0,0,3,3,0,3,0,3,4,0,3,3,3,0,3,0,1,0,1,3,0,0,1,0,0,0,3,1,0,0,0,1,0,0,1,0,1,3,0,0,1,0,0,0,3,1,0,0,0,1,0,1,2,1,2,0,1,1,0,1,0,1,0,2,1,1,1,2,1,0,1,0,1,3,0,0,1,0,0,0,3,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,3,0,0,1,0,0,0,3,1,0,0,0,1,0,3,0,3,0,4,3,3,0,3,0,3,0,0,3,3,3,0,3,1,2,1,2,0,1,1,2,1,0,1,0,0,1,1,1,2,1,0,1,0,1,3,0,0,1,0,0,0,3,1,0,0,0,1,0,0,1,0,1,3,0,0,1,0,0,0,3,1,0,0,0,1,0,0,1,0,1,3,0,0,1,0,0,0,3,1,0,0,0,1,0,1,2,1,2,0,1,1,2,1,0,1,0,2,1,1,1,0,1,0,1,0,1,3,0,0,1,0,0,0,3,1,0,0,0,1,0},
		{0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,3,3,3,0,0,3,0,3,3,3,3,0,4,3,3,3,4,3,1,1,1,0,0,1,2,1,1,1,1,2,0,1,1,1,0,1,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,1,1,1,0,2,1,0,1,1,1,1,2,0,1,1,1,0,1,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,1,1,1,0,2,1,2,1,1,1,1,0,0,1,1,1,0,1,3,3,3,4,0,3,0,3,3,3,3,0,0,3,3,3,4,3,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0,3,3,3,4,0,3,0,3,3,3,3,0,4,3,3,3,0,3,0,0,0,3,1,0,1,0,0,0,0,1,3,0,0,0,3,0},
		{0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,1,1,1,1,1,1,0,2,1,1,0,1,1,1,1,0,0,1,1,1,1,1,1,1,2,0,1,1,0,1,1,1,1,0,0,1,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,3,3,3,3,3,3,0,0,3,3,0,3,3,3,3,0,4,3,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,0,0,3,3,4,3,3,3,3,0,0,3,0,0,0,0,0,0,1,1,0,0,3,0,0,0,0,0,3,0},
		{0,3,0,0,1,0,3,3,0,3,1,0,0,3,0,1,3,3,3,0,3,3,0,3,4,4,3,4,0,3,3,4,3,0,4,4,0,3,0,0,1,0,3,3,0,3,1,0,0,3,0,1,3,3,0,3,0,0,1,0,3,3,0,3,1,0,0,3,0,1,3,3,1,0,1,1,0,1,0,0,1,0,2,1,1,0,1,2,0,0,0,3,0,0,1,0,3,3,0,3,1,0,0,3,0,1,3,3,3,4,3,3,0,3,0,4,3,4,0,3,3,4,3,0,4,4,3,4,3,3,0,3,4,0,3,4,0,3,3,4,3,0,4,4,0,3,0,0,1,0,3,3,0,3,1,0,0,3,0,1,3,3,3,4,3,3,0,3,4,4,3,0,0,3,3,4,3,0,4,4,1,0,1,1,2,1,0,0,1,0,0,1,1,0,1,2,0,0,0,3,0,0,1,0,3,3,0,3,1,0,0,3,0,1,3,3,0,3,0,0,1,0,3,3,0,3,1,0,0,3,0,1,3,3,3,4,3,3,0,3,4,4,3,4,0,3,3,0,3,0,4,4,0,3,0,0,1,0,3,3,0,3,1,0,0,3,0,1,3,3,1,0,1,1,2,1,0,0,1,0,2,1,1,0,1,0,0,0,3,4,3,3,0,3,4,4,3,4,0,3,3,4,3,0,0,4,3,4,3,3,0,3,4,4,3,4,0,3,3,4,3,0,4,0},
		{0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0,1,0,1,1,1,2,0,1,0,2,1,2,1,1,1,1,0,1,0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0,0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0,0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0,1,2,1,1,1,0,0,1,0,2,1,2,1,1,1,1,0,1,3,0,3,3,3,0,0,3,4,0,3,0,3,3,3,3,4,3,0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0,3,0,3,3,3,0,4,3,0,0,3,0,3,3,3,3,4,3,1,2,1,1,1,2,0,1,0,0,1,2,1,1,1,1,0,1,0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0,1,2,1,1,1,2,0,1,0,2,1,0,1,1,1,1,0,1,0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0,0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0,0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0,0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0,3,0,3,3,3,0,4,3,4,0,3,0,3,3,3,3,0,3,0,1,0,0,0,1,3,0,3,1,0,1,0,0,0,0,3,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,1,1,1,1,1,1,1,1,1,0,1,1,2,1,0,1,1,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,1,1,1,1,1,1,1,1,1,2,1,1,0,1,0,1,1,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,3,3,3,3,3,3,3,3,3,0,3,3,0,3,0,3,3,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,3,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,3,0,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,3,3,3,3,3,3,0,3,3,3,0,3,3,0,3,4,3,4,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,1,1,1,1,1,1,0,1,1,1,0,1,1,2,1,0,1,0,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,1,1,1,1,1,1,0,1,1,1,2,1,1,0,1,0,1,0,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,3,3,3,3,3,3,4,3,3,3,0,3,3,0,3,0,3,4,0,0,0,0,0,0,3,0,0,0,1,0,0,1,0,3,0,3,3,3,3,3,3,3,4,3,3,3,0,3,3,0,3,4,3,0},
		{0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,3,0,4,4,3,0,3,3,3,3,3,3,0,3,3,3,4,0,3,4,0,4,3,0,3,3,3,3,3,3,0,3,3,3,4,0,3,4,4,0,3,0,3,3,3,3,3,3,0,3,3,3,4,0,0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,1,0,0,0,1,0,1,1,1,1,1,1,2,1,1,1,0,2,0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,1,0,0,0,1,2,1,1,1,1,1,1,0,1,1,1,0,2,0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,0,3,3,3,0,1,0,0,0,0,0,0,1,0,0,0,3,1,3,4,4,4,3,0,3,3,3,3,3,3,0,3,3,3,0,0,1,0,0,0,1,2,1,1,1,1,1,1,2,1,1,1,0,0},
		{0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,3,0,3,3,3,3,0,4,3,3,3,3,3,3,0,0,4,3,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,1,0,1,1,1,1,0,0,1,1,1,1,1,1,2,2,0,1,3,4,3,3,3,3,0,0,3,3,3,3,3,3,0,0,4,3,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0,1,0,1,1,1,1,2,0,1,1,1,1,1,1,0,2,0,1,1,0,1,1,1,1,2,0,1,1,1,1,1,1,2,0,0,1,3,4,3,3,3,3,0,4,3,3,3,3,3,3,0,0,0,3,0,3,0,0,0,0,1,3,0,0,0,0,0,0,1,1,3,0}
		};



	static final MoveDef[] MOVES = new MoveDef[] {
		new MoveDef("absorb","User recovers half the HP inflicted on opponent.",(byte)4,20,100,25)
		,new MoveDef("acid","May lower opponent's Special Defense.",(byte)7,40,100,30)
		,new MoveDef("acid-armor","Sharply raises user's Defense.",(byte)7,-1,-1,20)
		,new MoveDef("agility","Sharply raises user's Speed.",(byte)10,-1,-1,30)
		,new MoveDef("amnesia","Sharply raises user's Special Defense.",(byte)10,-1,-1,20)
		,new MoveDef("aurora-beam","May lower opponent's Attack.",(byte)5,65,100,20)
		,new MoveDef("barrage","Hits 2-5 times in one turn.",(byte)0,15,85,20)
		,new MoveDef("barrier","Sharply raises user's Defense.",(byte)10,-1,-1,20)
		,new MoveDef("bide","User takes damage for two turns then strikes back double.",(byte)0,-1,-1,10)
		,new MoveDef("bind","Traps opponent, damaging them for 4-5 turns.",(byte)0,15,85,20)
		,new MoveDef("bite","May cause flinching.",(byte)15,60,100,25)
		,new MoveDef("blizzard","May freeze opponent.",(byte)5,110,70,5)
		,new MoveDef("body-slam","May paralyze opponent.",(byte)0,85,100,15)
		,new MoveDef("bone-club","May cause flinching.",(byte)8,65,85,20)
		,new MoveDef("bonemerang","Hits twice in one turn.",(byte)8,50,90,10)
		,new MoveDef("bubble","May lower opponent's Speed.",(byte)2,40,100,30)
		,new MoveDef("bubble-beam","May lower opponent's Speed.",(byte)2,65,100,20)
		,new MoveDef("clamp","Traps opponent, damaging them for 4-5 turns.",(byte)2,35,85,10)
		,new MoveDef("comet-punch","Hits 2-5 times in one turn.",(byte)0,18,85,15)
		,new MoveDef("confuse-ray","Confuses opponent.",(byte)13,-1,100,10)
		,new MoveDef("confusion","May confuse opponent.",(byte)10,50,100,25)
		,new MoveDef("constrict","May lower opponent's Speed by one stage.",(byte)0,10,100,35)
		,new MoveDef("conversion","Changes user's type to that of its first move.",(byte)0,-1,-1,30)
		,new MoveDef("counter","When hit by a Physical Attack, user strikes back with 2x power.",(byte)6,-1,100,20)
		,new MoveDef("crabhammer","High critical hit ratio.",(byte)2,100,90,10)
		,new MoveDef("cut",null,(byte)0,50,95,30)
		,new MoveDef("defense-curl","Raises user's Defense.",(byte)0,-1,-1,40)
		,new MoveDef("dig","Digs underground on first turn, attacks on second. Can also escape from caves.",(byte)8,80,100,10)
		,new MoveDef("disable","Opponent can't use its last attack for a few turns.",(byte)0,-1,100,20)
		,new MoveDef("dizzy-punch","May confuse opponent.",(byte)0,70,100,10)
		,new MoveDef("double-kick","Hits twice in one turn.",(byte)6,30,100,30)
		,new MoveDef("double-slap","Hits 2-5 times in one turn.",(byte)0,15,85,10)
		,new MoveDef("double-team","Raises user's Evasiveness.",(byte)0,-1,-1,15)
		,new MoveDef("double-edge","User receives recoil damage.",(byte)0,120,100,15)
		,new MoveDef("dragon-rage","Always inflicts 40 HP.",(byte)14,-1,100,10)
		,new MoveDef("dream-eater","User recovers half the HP inflicted on a sleeping opponent.",(byte)10,100,100,15)
		,new MoveDef("drill-peck",null,(byte)9,80,100,20)
		,new MoveDef("earthquake","Power is doubled if opponent is underground from using Dig.",(byte)8,100,100,10)
		,new MoveDef("egg-bomb",null,(byte)0,100,75,10)
		,new MoveDef("ember","May burn opponent.",(byte)1,40,100,25)
		,new MoveDef("explosion","User faints.",(byte)0,250,100,5)
		,new MoveDef("fire-blast","May burn opponent.",(byte)1,110,85,5)
		,new MoveDef("fire-punch","May burn opponent.",(byte)1,75,100,15)
		,new MoveDef("fire-spin","Traps opponent, damaging them for 4-5 turns.",(byte)1,35,85,15)
		,new MoveDef("fissure","One-Hit-KO, if it hits.",(byte)8,-1,-1,5)
		,new MoveDef("flamethrower","May burn opponent.",(byte)1,90,100,15)
		,new MoveDef("flash","Lowers opponent's Accuracy.",(byte)0,-1,100,20)
		,new MoveDef("fly","Flies up on first turn, attacks on second turn.",(byte)9,90,95,15)
		,new MoveDef("focus-energy","Increases critical hit ratio.",(byte)0,-1,-1,30)
		,new MoveDef("fury-attack","Hits 2-5 times in one turn.",(byte)0,15,85,20)
		,new MoveDef("fury-swipes","Hits 2-5 times in one turn.",(byte)0,18,80,15)
		,new MoveDef("glare","Paralyzes opponent.",(byte)0,-1,100,30)
		,new MoveDef("growl","Lowers opponent's Attack.",(byte)0,-1,100,40)
		,new MoveDef("growth","Raises user's Attack and Special Attack.",(byte)0,-1,-1,40)
		,new MoveDef("guillotine","One-Hit-KO, if it hits.",(byte)0,-1,-1,5)
		,new MoveDef("gust","Hits Pokémon using Fly/Bounce with double power.",(byte)9,40,100,35)
		,new MoveDef("harden","Raises user's Defense.",(byte)0,-1,-1,30)
		,new MoveDef("haze","Resets all stat changes.",(byte)5,-1,-1,30)
		,new MoveDef("headbutt","May cause flinching.",(byte)0,70,100,15)
		,new MoveDef("high-jump-kick","If it misses, the user loses half their HP.",(byte)6,130,90,10)
		,new MoveDef("horn-attack",null,(byte)0,65,100,25)
		,new MoveDef("horn-drill","One-Hit-KO, if it hits.",(byte)0,-1,-1,5)
		,new MoveDef("hydro-pump",null,(byte)2,110,80,5)
		,new MoveDef("hyper-beam","User must recharge next turn.",(byte)0,150,90,5)
		,new MoveDef("hyper-fang","May cause flinching.",(byte)0,80,90,15)
		,new MoveDef("hypnosis","Puts opponent to sleep.",(byte)10,-1,60,20)
		,new MoveDef("ice-beam","May freeze opponent.",(byte)5,90,100,10)
		,new MoveDef("ice-punch","May freeze opponent.",(byte)5,75,100,15)
		,new MoveDef("jump-kick","If it misses, the user loses half their HP.",(byte)6,100,95,10)
		,new MoveDef("karate-chop","High critical hit ratio.",(byte)6,50,100,25)
		,new MoveDef("kinesis","Lowers opponent's Accuracy.",(byte)10,-1,80,15)
		,new MoveDef("leech-life","User recovers half the HP inflicted on opponent.",(byte)11,20,100,15)
		,new MoveDef("leech-seed","User steals HP from opponent each turn.",(byte)4,-1,90,10)
		,new MoveDef("leer","Lowers opponent's Defense.",(byte)0,-1,100,30)
		,new MoveDef("lick","May paralyze opponent.",(byte)13,30,100,30)
		,new MoveDef("light-screen","Halves damage from Special attacks for 5 turns.",(byte)10,-1,-1,30)
		,new MoveDef("lovely-kiss","Puts opponent to sleep.",(byte)0,-1,75,10)
		,new MoveDef("low-kick","The heavier the opponent, the stronger the attack.",(byte)6,-1,100,20)
		,new MoveDef("meditate","Raises user's Attack.",(byte)10,-1,-1,40)
		,new MoveDef("mega-drain","User recovers half the HP inflicted on opponent.",(byte)4,40,100,15)
		,new MoveDef("mega-kick",null,(byte)0,120,75,5)
		,new MoveDef("mega-punch",null,(byte)0,80,85,20)
		,new MoveDef("metronome","User performs any move in the game at random.",(byte)0,-1,-1,10)
		,new MoveDef("mimic","Copies the opponent's last move.",(byte)0,-1,-1,10)
		,new MoveDef("minimize","Sharply raises user's Evasiveness.",(byte)0,-1,-1,10)
		,new MoveDef("mirror-move","User performs the opponent's last move.",(byte)9,-1,-1,20)
		,new MoveDef("mist","User's stats cannot be changed for a period of time.",(byte)5,-1,-1,30)
		,new MoveDef("night-shade","Inflicts damage equal to user's level.",(byte)13,-1,100,15)
		,new MoveDef("pay-day","A small amount of money is gained after the battle resolves.",(byte)0,40,100,20)
		,new MoveDef("peck",null,(byte)9,35,100,35)
		,new MoveDef("petal-dance","User attacks for 2-3 turns but then becomes confused.",(byte)4,120,100,10)
		,new MoveDef("pin-missile","Hits 2-5 times in one turn.",(byte)11,25,95,20)
		,new MoveDef("poison-gas","Poisons opponent.",(byte)7,-1,90,40)
		,new MoveDef("poison-powder","Poisons opponent.",(byte)7,-1,75,35)
		,new MoveDef("poison-sting","May poison the opponent.",(byte)7,15,100,35)
		,new MoveDef("pound",null,(byte)0,40,100,35)
		,new MoveDef("psybeam","May confuse opponent.",(byte)10,65,100,20)
		,new MoveDef("psychic","May lower opponent's Special Defense.",(byte)10,90,100,10)
		,new MoveDef("psywave","Inflicts damage 50-150% of user's level.",(byte)10,-1,80,15)
		,new MoveDef("quick-attack","User attacks first.",(byte)0,40,100,30)
		,new MoveDef("rage","Raises user's Attack when hit.",(byte)0,20,100,20)
		,new MoveDef("razor-leaf","High critical hit ratio.",(byte)4,55,95,25)
		,new MoveDef("razor-wind","Charges on first turn, attacks on second. High critical hit ratio.",(byte)0,80,100,10)
		,new MoveDef("recover","User recovers half its max HP.",(byte)0,-1,-1,10)
		,new MoveDef("reflect","Halves damage from Physical attacks for 5 turns.",(byte)10,-1,-1,20)
		,new MoveDef("rest","User sleeps for 2 turns, but user is fully healed.",(byte)10,-1,-1,10)
		,new MoveDef("roar","In battles, the opponent switches. In the wild, the Pokémon runs.",(byte)0,-1,-1,20)
		,new MoveDef("rock-slide","May cause flinching.",(byte)12,75,90,10)
		,new MoveDef("rock-throw",null,(byte)12,50,90,15)
		,new MoveDef("rolling-kick","May cause flinching.",(byte)6,60,85,15)
		,new MoveDef("sand-attack","Lowers opponent's Accuracy.",(byte)8,-1,100,15)
		,new MoveDef("scratch",null,(byte)0,40,100,35)
		,new MoveDef("screech","Sharply lowers opponent's Defense.",(byte)0,-1,85,40)
		,new MoveDef("seismic-toss","Inflicts damage equal to user's level.",(byte)6,-1,100,20)
		,new MoveDef("self-destruct","User faints.",(byte)0,200,100,5)
		,new MoveDef("sharpen","Raises user's Attack.",(byte)0,-1,-1,30)
		,new MoveDef("sing","Puts opponent to sleep.",(byte)0,-1,55,15)
		,new MoveDef("skull-bash","Raises Defense on first turn, attacks on second.",(byte)0,130,100,10)
		,new MoveDef("sky-attack","Charges on first turn, attacks on second. May cause flinching.",(byte)9,140,90,5)
		,new MoveDef("slam",null,(byte)0,80,75,20)
		,new MoveDef("slash","High critical hit ratio.",(byte)0,70,100,20)
		,new MoveDef("sleep-powder","Puts opponent to sleep.",(byte)4,-1,75,15)
		,new MoveDef("sludge","May poison opponent.",(byte)7,65,100,20)
		,new MoveDef("smog","May poison opponent.",(byte)7,30,70,20)
		,new MoveDef("smokescreen","Lowers opponent's Accuracy.",(byte)0,-1,100,20)
		,new MoveDef("soft-boiled","User recovers half its max HP.",(byte)0,-1,-1,10)
		,new MoveDef("solar-beam","Charges on first turn, attacks on second.",(byte)4,120,100,10)
		,new MoveDef("sonic-boom","Always inflicts 20 HP.",(byte)0,-1,90,20)
		,new MoveDef("spike-cannon","Hits 2-5 times in one turn.",(byte)0,20,100,15)
		,new MoveDef("splash","Doesn't do ANYTHING.",(byte)0,-1,-1,40)
		,new MoveDef("spore","Puts opponent to sleep.",(byte)4,-1,100,15)
		,new MoveDef("stomp","May cause flinching.",(byte)0,65,100,20)
		,new MoveDef("strength",null,(byte)0,80,100,15)
		,new MoveDef("string-shot","Sharply lowers opponent's Speed.",(byte)11,-1,95,40)
		,new MoveDef("struggle","Only usable when all PP are gone. Hurts the user.",(byte)0,50,100,-1)
		,new MoveDef("stun-spore","Paralyzes opponent.",(byte)4,-1,75,30)
		,new MoveDef("submission","User receives recoil damage.",(byte)6,80,80,25)
		,new MoveDef("substitute","Uses HP to creates a decoy that takes hits.",(byte)0,-1,-1,10)
		,new MoveDef("super-fang","Always takes off half of the opponent's HP.",(byte)0,-1,90,10)
		,new MoveDef("supersonic","Confuses opponent.",(byte)0,-1,55,20)
		,new MoveDef("surf","Hits all adjacent Pokémon.",(byte)2,90,100,15)
		,new MoveDef("swift","Ignores Accuracy and Evasiveness.",(byte)0,60,-1,20)
		,new MoveDef("swords-dance","Sharply raises user's Attack.",(byte)0,-1,-1,20)
		,new MoveDef("tackle",null,(byte)0,50,100,35)
		,new MoveDef("tail-whip","Lowers opponent's Defense.",(byte)0,-1,100,30)
		,new MoveDef("take-down","User receives recoil damage.",(byte)0,90,85,20)
		,new MoveDef("teleport","Allows user to flee wild battles; also warps player to last PokéCenter.",(byte)10,-1,-1,20)
		,new MoveDef("thrash","User attacks for 2-3 turns but then becomes confused.",(byte)0,120,100,10)
		,new MoveDef("thunder","May paralyze opponent.",(byte)3,110,70,10)
		,new MoveDef("thunder-punch","May paralyze opponent.",(byte)3,75,100,15)
		,new MoveDef("thunder-shock","May paralyze opponent.",(byte)3,40,100,30)
		,new MoveDef("thunder-wave","Paralyzes opponent.",(byte)3,-1,100,20)
		,new MoveDef("thunderbolt","May paralyze opponent.",(byte)3,90,100,15)
		,new MoveDef("toxic","Badly poisons opponent.",(byte)7,-1,90,10)
		,new MoveDef("transform","User takes on the form and attacks of the opponent.",(byte)0,-1,-1,10)
		,new MoveDef("tri-attack","May paralyze, burn or freeze opponent.",(byte)0,80,100,10)
		,new MoveDef("twineedle","Hits twice in one turn. May poison opponent.",(byte)11,25,100,20)
		,new MoveDef("vice-grip",null,(byte)0,55,100,30)
		,new MoveDef("vine-whip",null,(byte)4,45,100,25)
		,new MoveDef("water-gun",null,(byte)2,40,100,25)
		,new MoveDef("waterfall","May cause flinching.",(byte)2,80,100,15)
		,new MoveDef("whirlwind","In battles, the opponent switches. In the wild, the Pokémon runs.",(byte)0,-1,-1,20)
		,new MoveDef("wing-attack",null,(byte)9,60,100,35)
		,new MoveDef("withdraw","Raises user's Defense.",(byte)2,-1,-1,40)
		,new MoveDef("wrap","Traps opponent, damaging them for 4-5 turns.",(byte)0,15,90,20)
		,new MoveDef("aeroblast","High critical hit ratio.",(byte)9,100,95,5)
		,new MoveDef("ancient-power","May raise all user's stats at once.",(byte)12,60,100,5)
		,new MoveDef("attract","If opponent is the opposite gender, it's less likely to attack.",(byte)0,-1,100,15)
		,new MoveDef("baton-pass","User switches out and gives stat changes to the incoming Pokémon.",(byte)0,-1,-1,40)
		,new MoveDef("beat-up","Each Pokémon in your party attacks.",(byte)15,-1,100,30)
		,new MoveDef("belly-drum","User loses 50% of its max HP, but Attack raises to maximum.",(byte)0,-1,-1,10)
		,new MoveDef("bone-rush","Hits 2-5 times in one turn.",(byte)8,25,90,10)
		,new MoveDef("charm","Sharply lowers opponent's Attack.",(byte)17,-1,100,20)
		,new MoveDef("conversion-2","User changes type to become resistant to opponent's last move.",(byte)0,-1,-1,30)
		,new MoveDef("cotton-spore","Sharply lowers opponent's Speed.",(byte)4,-1,100,40)
		,new MoveDef("cross-chop","High critical hit ratio.",(byte)6,100,80,5)
		,new MoveDef("crunch","May lower opponent's Defense.",(byte)15,80,100,15)
		,new MoveDef("curse","Ghosts lose 50% of max HP and curse the opponent; Non-Ghosts raise Attack, Defense and lower Speed.",(byte)13,-1,-1,10)
		,new MoveDef("destiny-bond","If the user faints, the opponent also faints.",(byte)13,-1,-1,5)
		,new MoveDef("detect","Opponent's attack doesn't affect you, but may fail if used often.",(byte)6,-1,-1,5)
		,new MoveDef("dragon-breath","May paralyze opponent.",(byte)14,60,100,20)
		,new MoveDef("dynamic-punch","Confuses opponent.",(byte)6,100,50,5)
		,new MoveDef("encore","Forces opponent to keep using its last move for 3 turns.",(byte)0,-1,100,5)
		,new MoveDef("endure","Always left with at least 1 HP, but may fail if used consecutively.",(byte)0,-1,-1,10)
		,new MoveDef("extreme-speed","User attacks first.",(byte)0,80,100,5)
		,new MoveDef("false-swipe","Always leaves opponent with at least 1 HP.",(byte)0,40,100,40)
		,new MoveDef("feint-attack","Ignores Accuracy and Evasiveness.",(byte)15,60,-1,20)
		,new MoveDef("flail","The lower the user's HP, the higher the power.",(byte)0,-1,100,15)
		,new MoveDef("flame-wheel","May burn opponent.",(byte)1,60,100,25)
		,new MoveDef("foresight","Resets opponent's Evasiveness, Normal-type and Fighting-type attacks can now hit Ghosts, and Ghost-type attacks hit Normal.",(byte)0,-1,-1,40)
		,new MoveDef("frustration","Power decreases with higher Happiness.",(byte)0,-1,100,20)
		,new MoveDef("fury-cutter","Power increases each turn.",(byte)11,40,95,20)
		,new MoveDef("future-sight","Damage occurs 2 turns later.",(byte)10,120,100,10)
		,new MoveDef("giga-drain","User recovers half the HP inflicted on opponent.",(byte)4,75,100,10)
		,new MoveDef("heal-bell","Heals the user's party's status conditions.",(byte)0,-1,-1,5)
		,new MoveDef("hidden-power","Type and power depends on user's IVs.",(byte)0,60,100,15)
		,new MoveDef("icy-wind","Lowers opponent's Speed.",(byte)5,55,95,15)
		,new MoveDef("iron-tail","May lower opponent's Defense.",(byte)16,100,75,15)
		,new MoveDef("lock-on","The next move the user uses is guaranteed to hit.",(byte)0,-1,-1,5)
		,new MoveDef("mach-punch","User attacks first.",(byte)6,40,100,30)
		,new MoveDef("magnitude","Hits with random power.",(byte)8,-1,100,30)
		,new MoveDef("mean-look","Opponent cannot flee or switch.",(byte)0,-1,-1,5)
		,new MoveDef("megahorn",null,(byte)11,120,85,10)
		,new MoveDef("metal-claw","May raise user's Attack.",(byte)16,50,95,35)
		,new MoveDef("milk-drink","User recovers half its max HP.",(byte)0,-1,-1,10)
		,new MoveDef("mind-reader","User's next attack is guaranteed to hit.",(byte)0,-1,-1,5)
		,new MoveDef("mirror-coat","When hit by a Special Attack, user strikes back with 2x power.",(byte)10,-1,100,20)
		,new MoveDef("moonlight","User recovers HP. Amount varies with the weather.",(byte)17,-1,-1,5)
		,new MoveDef("morning-sun","User recovers HP. Amount varies with the weather.",(byte)0,-1,-1,5)
		,new MoveDef("mud-slap","Lowers opponent's Accuracy.",(byte)8,20,100,10)
		,new MoveDef("nightmare","The sleeping opponent loses 25% of its max HP each turn.",(byte)13,-1,100,15)
		,new MoveDef("octazooka","May lower opponent's Accuracy.",(byte)2,65,85,10)
		,new MoveDef("outrage","User attacks for 2-3 turns but then becomes confused.",(byte)14,120,100,10)
		,new MoveDef("pain-split","The user's and opponent's HP becomes the average of both.",(byte)0,-1,-1,20)
		,new MoveDef("perish-song","Any Pokémon in play when this attack is used faints in 3 turns.",(byte)0,-1,-1,5)
		,new MoveDef("powder-snow","May freeze opponent.",(byte)5,40,100,25)
		,new MoveDef("present","Either deals damage or heals.",(byte)0,-1,90,15)
		,new MoveDef("protect","User is not affected by opponent's move.",(byte)0,-1,-1,10)
		,new MoveDef("psych-up","Copies the opponent's stat changes.",(byte)0,-1,-1,10)
		,new MoveDef("pursuit","Double power if the opponent is switching out.",(byte)15,40,100,20)
		,new MoveDef("rain-dance","Makes it rain for 5 turns.",(byte)2,-1,-1,5)
		,new MoveDef("rapid-spin","Removes effects of trap moves.",(byte)0,20,100,40)
		,new MoveDef("return","Power increases with user's Happiness.",(byte)0,-1,100,20)
		,new MoveDef("reversal","The lower the user's HP, the higher the power.",(byte)6,-1,100,15)
		,new MoveDef("rock-smash","May lower opponent's Defense.",(byte)6,40,100,15)
		,new MoveDef("rollout","Doubles in power each turn for 5 turns.",(byte)12,30,90,20)
		,new MoveDef("sacred-fire","May burn opponent.",(byte)1,100,95,5)
		,new MoveDef("safeguard","The user's party is protected from status conditions.",(byte)0,-1,-1,25)
		,new MoveDef("sandstorm","Creates a sandstorm for 5 turns.",(byte)12,-1,-1,10)
		,new MoveDef("scary-face","Sharply lowers opponent's Speed.",(byte)0,-1,100,10)
		,new MoveDef("shadow-ball","May lower opponent's Special Defense.",(byte)13,80,100,15)
		,new MoveDef("sketch","Permanently copies the opponent's last move.",(byte)0,-1,-1,1)
		,new MoveDef("sleep-talk","User performs one of its own moves while sleeping.",(byte)0,-1,-1,10)
		,new MoveDef("sludge-bomb","May poison opponent.",(byte)7,90,100,10)
		,new MoveDef("snore","Can only be used if asleep. May cause flinching.",(byte)0,50,100,15)
		,new MoveDef("spark","May paralyze opponent.",(byte)3,65,100,20)
		,new MoveDef("spider-web","Opponent cannot escape/switch.",(byte)11,-1,-1,10)
		,new MoveDef("spikes","Hurts opponents when they switch into battle.",(byte)8,-1,-1,20)
		,new MoveDef("spite","The opponent's last move loses 2-5 PP.",(byte)13,-1,100,10)
		,new MoveDef("steel-wing","May raise user's Defense.",(byte)16,70,90,25)
		,new MoveDef("sunny-day","Makes it sunny for 5 turns.",(byte)1,-1,-1,5)
		,new MoveDef("swagger","Opponent becomes confused, but its Attack is raised two stages.",(byte)0,-1,90,15)
		,new MoveDef("sweet-kiss","Confuses opponent.",(byte)17,-1,75,10)
		,new MoveDef("sweet-scent","Lowers opponent's Evasiveness.",(byte)0,-1,-1,20)
		,new MoveDef("synthesis","User recovers HP. Amount varies with the weather.",(byte)4,-1,-1,5)
		,new MoveDef("thief","Also steals opponent's held item.",(byte)15,60,100,25)
		,new MoveDef("triple-kick","Hits thrice in one turn at increasing power.",(byte)6,10,90,10)
		,new MoveDef("twister","May cause flinching. Hits Pokémon using Fly/Bounce with double power.",(byte)14,40,100,20)
		,new MoveDef("vital-throw","User attacks last, but ignores Accuracy and Evasiveness.",(byte)6,70,-1,10)
		,new MoveDef("whirlpool","Traps opponent, damaging them for 4-5 turns.",(byte)2,35,85,15)
		,new MoveDef("zap-cannon","Paralyzes opponent.",(byte)3,120,50,5)
		,new MoveDef("aerial-ace","Ignores Accuracy and Evasiveness.",(byte)9,60,-1,20)
		,new MoveDef("air-cutter","High critical hit ratio.",(byte)9,60,95,25)
		,new MoveDef("arm-thrust","Hits 2-5 times in one turn.",(byte)6,15,100,20)
		,new MoveDef("aromatherapy","Cures all status problems in your party.",(byte)4,-1,-1,5)
		,new MoveDef("assist","In a Double Battle, user randomly attacks with a partner's move.",(byte)0,-1,-1,20)
		,new MoveDef("astonish","May cause flinching.",(byte)13,30,100,15)
		,new MoveDef("blast-burn","User must recharge next turn.",(byte)1,150,90,5)
		,new MoveDef("blaze-kick","High critical hit ratio. May burn opponent.",(byte)1,85,90,10)
		,new MoveDef("block","Opponent cannot flee or switch.",(byte)0,-1,-1,5)
		,new MoveDef("bounce","Springs up on first turn, attacks on second. May paralyze opponent.",(byte)9,85,85,5)
		,new MoveDef("brick-break","Breaks through Reflect and Light Screen barriers.",(byte)6,75,100,15)
		,new MoveDef("bulk-up","Raises user's Attack and Defense.",(byte)6,-1,-1,20)
		,new MoveDef("bullet-seed","Hits 2-5 times in one turn.",(byte)4,25,100,30)
		,new MoveDef("calm-mind","Raises user's Special Attack and Special Defense.",(byte)10,-1,-1,20)
		,new MoveDef("camouflage","Changes user's type according to the location.",(byte)0,-1,-1,20)
		,new MoveDef("charge","Raises user's Special Defense and next Electric move's power increases.",(byte)3,-1,-1,20)
		,new MoveDef("cosmic-power","Raises user's Defense and Special Defense.",(byte)10,-1,-1,20)
		,new MoveDef("covet","Opponent's item is stolen by the user.",(byte)0,60,100,25)
		,new MoveDef("crush-claw","May lower opponent's Defense.",(byte)0,75,95,10)
		,new MoveDef("dive","Dives underwater on first turn, attacks on second turn.",(byte)2,80,100,10)
		,new MoveDef("doom-desire","Damage occurs 2 turns later.",(byte)16,140,100,5)
		,new MoveDef("dragon-claw",null,(byte)14,80,100,15)
		,new MoveDef("dragon-dance","Raises user's Attack and Speed.",(byte)14,-1,-1,20)
		,new MoveDef("endeavor","Reduces opponent's HP to same as user's.",(byte)0,-1,100,5)
		,new MoveDef("eruption","Stronger when the user's HP is higher.",(byte)1,150,100,5)
		,new MoveDef("extrasensory","May cause flinching.",(byte)10,80,100,20)
		,new MoveDef("facade","Power doubles if user is burned, poisoned, or paralyzed.",(byte)0,70,100,20)
		,new MoveDef("fake-out","User attacks first, foe flinches. Only usable on first turn.",(byte)0,40,100,10)
		,new MoveDef("fake-tears","Sharply lowers opponent's Special Defense.",(byte)15,-1,100,20)
		,new MoveDef("feather-dance","Sharply lowers opponent's Attack.",(byte)9,-1,100,15)
		,new MoveDef("flatter","Confuses opponent, but raises its Special Attack by two stages.",(byte)15,-1,100,15)
		,new MoveDef("focus-punch","If the user is hit before attacking, it flinches instead.",(byte)6,150,100,20)
		,new MoveDef("follow-me","In Double Battle, the user takes all the attacks.",(byte)0,-1,-1,20)
		,new MoveDef("frenzy-plant","User must recharge next turn.",(byte)4,150,90,5)
		,new MoveDef("grass-whistle","Puts opponent to sleep.",(byte)4,-1,55,15)
		,new MoveDef("grudge","If the users faints after using this move, the PP for the opponent's last move is depleted.",(byte)13,-1,-1,5)
		,new MoveDef("hail","Non-Ice types are damaged for 5 turns.",(byte)5,-1,-1,10)
		,new MoveDef("heat-wave","May burn opponent.",(byte)1,95,90,10)
		,new MoveDef("helping-hand","In Double Battles, boosts the power of the partner's move.",(byte)0,-1,-1,20)
		,new MoveDef("howl","Raises user's Attack.",(byte)0,-1,-1,40)
		,new MoveDef("hydro-cannon","User must recharge next turn.",(byte)2,150,90,5)
		,new MoveDef("hyper-voice",null,(byte)0,90,100,10)
		,new MoveDef("ice-ball","Doubles in power each turn for 5 turns.",(byte)5,30,90,20)
		,new MoveDef("icicle-spear","Hits 2-5 times in one turn.",(byte)5,25,100,30)
		,new MoveDef("imprison","Opponent is unable to use moves that the user also knows.",(byte)10,-1,-1,10)
		,new MoveDef("ingrain","User restores HP each turn. User cannot escape/switch.",(byte)4,-1,-1,20)
		,new MoveDef("iron-defense","Sharply raises user's Defense.",(byte)16,-1,-1,15)
		,new MoveDef("knock-off","Removes opponent's held item for the rest of the battle.",(byte)15,65,100,25)
		,new MoveDef("leaf-blade","High critical hit ratio.",(byte)4,90,100,15)
		,new MoveDef("luster-purge","May lower opponent's Special Defense.",(byte)10,70,100,5)
		,new MoveDef("magic-coat","Reflects moves that cause status conditions back to the attacker.",(byte)10,-1,-1,15)
		,new MoveDef("magical-leaf","Ignores Accuracy and Evasiveness.",(byte)4,60,-1,20)
		,new MoveDef("memento","User faints, sharply lowers opponent's Attack and Special Attack.",(byte)15,-1,100,10)
		,new MoveDef("metal-sound","Sharply lowers opponent's Special Defense.",(byte)16,-1,85,40)
		,new MoveDef("meteor-mash","May raise user's Attack.",(byte)16,90,90,10)
		,new MoveDef("mist-ball","May lower opponent's Special Attack.",(byte)10,70,100,5)
		,new MoveDef("mud-shot","Lowers opponent's Speed.",(byte)8,55,95,15)
		,new MoveDef("mud-sport","Weakens the power of Electric-type moves.",(byte)8,-1,-1,15)
		,new MoveDef("muddy-water","May lower opponent's Accuracy.",(byte)2,90,85,10)
		,new MoveDef("nature-power","Uses a certain move based on the current terrain.",(byte)0,-1,-1,20)
		,new MoveDef("needle-arm","May cause flinching.",(byte)4,60,100,15)
		,new MoveDef("odor-sleuth","Resets opponent's Evasiveness, Normal-type and Fighting-type attacks can now hit Ghosts, and Ghost-type attacks hit Normal.",(byte)0,-1,-1,40)
		,new MoveDef("overheat","Sharply lowers user's Special Attack.",(byte)1,130,90,5)
		,new MoveDef("poison-fang","May badly poison opponent.",(byte)7,50,100,15)
		,new MoveDef("poison-tail","High critical hit ratio. May poison opponent.",(byte)7,50,100,25)
		,new MoveDef("psycho-boost","Sharply lowers user's Special Attack.",(byte)10,140,90,5)
		,new MoveDef("recycle","User's used hold item is restored.",(byte)0,-1,-1,10)
		,new MoveDef("refresh","Cures paralysis, poison, and burns.",(byte)0,-1,-1,20)
		,new MoveDef("revenge","Power increases if user was hit first.",(byte)6,60,100,10)
		,new MoveDef("rock-blast","Hits 2-5 times in one turn.",(byte)12,25,90,10)
		,new MoveDef("rock-tomb","Lowers opponent's Speed.",(byte)12,60,95,15)
		,new MoveDef("role-play","User copies the opponent's Ability.",(byte)10,-1,-1,15)
		,new MoveDef("sand-tomb","Traps opponent, damaging them for 4-5 turns.",(byte)8,35,85,15)
		,new MoveDef("secret-power","Effects of the attack vary with the location.",(byte)0,70,100,20)
		,new MoveDef("shadow-punch","Ignores Accuracy and Evasiveness.",(byte)13,60,-1,20)
		,new MoveDef("sheer-cold","One-Hit-KO, if it hits.",(byte)5,-1,-1,5)
		,new MoveDef("shock-wave","Ignores Accuracy and Evasiveness.",(byte)3,60,-1,20)
		,new MoveDef("signal-beam","May confuse opponent.",(byte)11,75,100,15)
		,new MoveDef("silver-wind","May raise all stats of user at once.",(byte)11,60,100,5)
		,new MoveDef("skill-swap","The user swaps Abilities with the opponent.",(byte)10,-1,-1,10)
		,new MoveDef("sky-uppercut","Hits the opponent, even during Fly.",(byte)6,85,90,15)
		,new MoveDef("slack-off","User recovers half its max HP.",(byte)0,-1,-1,10)
		,new MoveDef("smelling-salts","Power doubles if opponent is paralyzed, but cures it.",(byte)0,70,100,10)
		,new MoveDef("snatch","Steals the effects of the opponent's next move.",(byte)15,-1,-1,10)
		,new MoveDef("spit-up","Power depends on how many times the user performed Stockpile.",(byte)0,-1,100,10)
		,new MoveDef("stockpile","Stores energy for use with Spit Up and Swallow.",(byte)0,-1,-1,20)
		,new MoveDef("superpower","Lowers user's Attack and Defense.",(byte)6,120,100,5)
		,new MoveDef("swallow","The more times the user has performed Stockpile, the more HP is recovered.",(byte)0,-1,-1,10)
		,new MoveDef("tail-glow","Drastically raises user's Special Attack.",(byte)11,-1,-1,20)
		,new MoveDef("taunt","Opponent can only use moves that attack.",(byte)15,-1,100,20)
		,new MoveDef("teeter-dance","Confuses all Pokémon.",(byte)0,-1,100,20)
		,new MoveDef("tickle","Lowers opponent's Attack and Defense.",(byte)0,-1,100,20)
		,new MoveDef("torment","Opponent cannot use the same move in a row.",(byte)15,-1,100,15)
		,new MoveDef("trick","Swaps held items with the opponent.",(byte)10,-1,100,10)
		,new MoveDef("uproar","User attacks for 3 turns and prevents sleep.",(byte)0,90,100,10)
		,new MoveDef("volt-tackle","User receives recoil damage. May paralyze opponent.",(byte)3,120,100,15)
		,new MoveDef("water-pulse","May confuse opponent.",(byte)2,60,100,20)
		,new MoveDef("water-sport","Weakens the power of Fire-type moves.",(byte)2,-1,-1,15)
		,new MoveDef("water-spout","The higher the user's HP, the higher the damage caused.",(byte)2,150,100,5)
		,new MoveDef("weather-ball","Move's power and type changes with the weather.",(byte)0,50,100,10)
		,new MoveDef("will-o-wisp","Burns opponent.",(byte)1,-1,85,15)
		,new MoveDef("wish","The user recovers HP in the following turn.",(byte)0,-1,-1,10)
		,new MoveDef("yawn","Puts opponent to sleep in the next turn.",(byte)0,-1,-1,10)
		};
	
	//too be initialized from file at run-time
	static final MonDef[] MON = new MonDef[1000];
	static int NUMBER_OF_MON = 0;
	static long TOTAL_MON_SPAWN = 0;
	
	//load mon at run-time from file
	static void loadMon() {
		FileInputStream fin;
		try {
			fin = new FileInputStream(new File("battlemon"));
		} catch (FileNotFoundException e) {return;}
		
		byte[] inputBuffer = new byte[4096];
		ByteBuffer lenBuf = ByteBuffer.allocate(4);
		while (true) {
			int rlen;
			try {
				rlen = fin.read(lenBuf.array());
			} catch (IOException e) {break;}
			if (rlen < 4) break;//done file
			
			rlen = lenBuf.getInt(0);
			try {
				fin.read(inputBuffer, 0, rlen);
			} catch (IOException e) {break;}
			
			String str = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(inputBuffer, 0, rlen)).toString();
			String[] segs = str.split('"'+"", -1);
			short[] bstats = new short[STAT_NAMES.length];
			String[] bstatstr = segs[5].split(",");
			for (int i = 0; i < bstats.length; i++) {
				bstats[i] = Short.parseShort(bstatstr[i]);
			}
			MonDef monDef = new MonDef(segs[0],
									   segs[1],
									   segs[2],
									  Byte.parseByte(segs[3]),
									  Byte.parseByte(segs[4]),
									  bstats,
									  Short.parseShort(segs[6]),
									  Byte.parseByte(segs[7]));
			
			short[] movesLearnedByLevel,
					movesLearnedByTM;
			byte[] levelMovesLearned;
			
			String[] movesLblStr = segs[8].split(","),
					 levelMovesStr = segs[9].split(",");
			movesLearnedByLevel = new short[movesLblStr.length];
			levelMovesLearned = new byte[movesLblStr.length];
			for (int i = 0; i < movesLblStr.length; i++) {
				if (movesLblStr[i].length() == 0 || levelMovesStr[i].length() == 0) break;
				movesLearnedByLevel[i] = Short.parseShort(movesLblStr[i]);
				levelMovesLearned[i] = Byte.parseByte(levelMovesStr[i]);
			}
			
			String[] movesLbtStr = segs[10].split(",");
			movesLearnedByTM = new short[movesLbtStr.length];
			for (int i = 0; i < movesLbtStr.length; i++) {
				if (movesLbtStr[i].length() == 0) break;
				movesLearnedByTM[i] = Short.parseShort(movesLbtStr[i]);
			}
			
			monDef.setMoves(movesLearnedByLevel, levelMovesLearned, movesLearnedByTM);
			monDef.calculateExtras();
			TOTAL_MON_SPAWN += monDef.spawnChance;
			
			MON[NUMBER_OF_MON] = monDef;
			
			NUMBER_OF_MON++;
		}
		
		try {
			fin.close();
		} catch (IOException e) {}	
		
		
		//calculate item spawn total
		for (int i = 0; i < ITEMS.length; i++) {
			ITEM_DROP_TOTAL += ITEMS[i].dropChance;
			ITEM_SHOP_TOTAL += ITEMS[i].shopChance;
		}
	}
}




/*
 * 
 
 code used to parse mon data from html
 
 
 
 
		//type vs type/type effectiveness matrix
		System.out.println("new byte[][] {");
		int nTypes = MonDef.TYPE_NAMES.length;
		for (int i = 0; i < nTypes; i++) {
			byte[] effectiveness = new byte[nTypes*nTypes];
			String es = "{";
			
			String html = BotUtils.HttpGet("http://pokemondb.net/type/"+MonDef.TYPE_NAMES[i], "");
			if (html == null) {
				System.out.println("HTML error.");
				return;
			}
			
			int findex;
			
			//iterate through all table entries and record effectiveness
			for (int k = 0; k < nTypes*nTypes; k++) {
				findex = html.indexOf("<td ");
				if (findex == -1) return;//something went wrong
				findex = html.indexOf("class=", findex);
				if (findex == -1) return;//something went wrong

				html = html.substring(findex+28);
				
				String effectId = html.substring(0,html.indexOf('"'+">"));
				byte effect;
				if (effectId.equals("25")) {
					effect = MonDef.TYPE_EFFECTIVENESS_QUARTER_EFFECTIVE;
				} else if (effectId.equals("50")) {
					effect = MonDef.TYPE_EFFECTIVENESS_HALF_EFFECTIVE;
				} else if (effectId.equals("200")) {
					effect = MonDef.TYPE_EFFECTIVENESS_2X_EFFECTIVE;
				} else if (effectId.equals("400")) {
					effect = MonDef.TYPE_EFFECTIVENESS_4X_EFFECTIVE;
				} else {
					effect = MonDef.TYPE_EFFECTIVENESS_NORMAL;
				}
				effectiveness[k] = effect;
				es += effect+(k<nTypes*nTypes-1?",":"");
			}
			System.out.println(es+"}"+(i<nTypes-1?",":""));
		}
		System.out.println("};");
 
 
 
 



 		//mon moves from generation 1 to 3 
		HashMap<String, Integer> typeMap = new HashMap<String, Integer>();
		for (int i = 0; i < MonDef.TYPE_NAMES.length; i++) {
			typeMap.put(MonDef.TYPE_NAMES[i], i);
		}
		
		System.out.println("new MoveDef[] {");
		boolean first = true;
		for (int g = 1; g < 4; g++) {//iterate through generations
			//get moves list html
			String html = BotUtils.HttpGet("http://pokemondb.net/move/generation/"+g,"");
			if (html == null) {//something went wrong
			System.out.println("died on gen");
			return;
			}
				
			//loop through moves in generation
			int findex = -1;
			while ((findex = html.indexOf("<td class="+'"'+"cell-icon-string"+'"'+">")) != -1) {
				html = html.substring(findex);
				
				//find move name
				int mindex = html.indexOf("href="+'"'+"/move/");
				if (mindex == -1) {
					System.out.println("died on move");
					return;
				}
				
				html = html.substring(mindex+12);
				
				String moveName = html.substring(0,html.indexOf('"'));
				
				//find type by name
				int tindex = html.indexOf("href="+'"'+"/type/");
				if (tindex == -1) {
					System.out.println("died on type");
					return;
				}
				
				html = html.substring(tindex+12);
				
				String typeName = html.substring(0,html.indexOf('"'));
				
				//find power, accuracy and pp numbers
				String[] nums = new String[3];
				for (int k = 0; k < 3; k++) {
					final String numSearchStr = "<td class="+'"'+"num"+'"';
					int kindex = html.indexOf(numSearchStr),
					    nindex = html.indexOf("href="+'"'+"/move/");
					if (kindex == -1 || (nindex != -1 && nindex < kindex)) {
						System.out.println("Died on stat");
						return;
					}
					html = html.substring(html.indexOf('>', kindex)+1);
					
					nums[k] = html.substring(0,html.indexOf('<'));
					try {
						nums[k] = Integer.parseInt(nums[k])+"";
					} catch (NumberFormatException e) {
						nums[k] = "-1";
					}
				}
				
				//find effect text
				int eindex = html.indexOf("<td class="+'"'+"long-text");
				if (eindex == -1) {
					System.out.println("died on effect");
					return;
				}
				html = html.substring(eindex+22);
				String effect = html.substring(0,html.indexOf("<"));
				effect = effect.equals("")?null:effect;
				
				System.out.println((first?"":",")+"new MoveDef("+'"'+moveName+'"'+","+(effect!=null?('"'+effect+'"'):null)+",(byte)"+typeMap.get(typeName)+","+nums[0]+","+nums[1]+","+nums[2]+")");
				first = false;
			}
		}
		System.out.println("};");

 
 
 



		//mon from generations 1 to 3
		String[] generationNames = new String[] {
				"http://pokemondb.net/pokedex/game/gold-silver-crystal",
				"http://pokemondb.net/pokedex/game/ruby-sapphire-emerald"
			};
			
			ArrayList<String> mon = new ArrayList<String>();
			
			//go through generations getting URL's of mon
			for (int g = 0; g < generationNames.length; g++) {
				String html = BotUtils.HttpGet(generationNames[g], "");
				if (html == null) return;//something went wrong
				
				int findex = -1;
				while ((findex = html.indexOf("class="+'"'+"ent-name"+'"'+" href="+'"'+"/pokedex/")) != -1) {
					html = html.substring(findex+32);
					mon.add(html.substring(0,html.indexOf('"')));
				}
			}
			
			HashMap<String, Integer> typeMap = new HashMap<String, Integer>();
			for (int i = 0; i < MonDef.TYPE_NAMES.length; i++) {
				typeMap.put(MonDef.TYPE_NAMES[i], i);
			}
			
			HashMap<String, Integer> moveMap = new HashMap<String, Integer>();
			for (int i = 0; i < MonDef.MOVES.length; i++) {
				moveMap.put(MonDef.MOVES[i].name, i);
			}
			
			//output pokemon definitions to file(too much data to inline)
			FileOutputStream fout;
			try {
				fout = new FileOutputStream(new File("battlemon"));
			} catch (FileNotFoundException e1) {return;}
			for (int i = 0; i < mon.size(); i++) {
				//pokemon name
				String name = mon.get(i);
				String html = BotUtils.HttpGet("http://pokemondb.net/pokedex/"+name, "");
				if (html == null) return;//something went wrong
				
				
				//get mon type
				String type1 = null,
						type2 = null;
				
				int findex = -1;
				findex = html.indexOf("href="+'"'+"/type/");
				if (findex == -1) return;//something went wrong
				html = html.substring(findex+12);
				
				findex = html.indexOf("href="+'"'+"/type/");
				if (findex == -1) return;//something went wrong
				
				html = html.substring(findex+12);
				type1 = html.substring(0, html.indexOf('"'));
				
				findex = html.indexOf("href="+'"'+"/type/");
				if (findex != -1) {
					//second type
					html = html.substring(findex+12);
					type2 = html.substring(0, html.indexOf('"'));
				}
							
				//get mon base stats
				final String[] statNames = new String[] {
					"HP", "Attack", "Defense", "Sp. Atk", "Sp. Def", "Speed"	
				};
				String bstatsString = "";
				int[] baseStats = new int[statNames.length];
				for (int k = 0; k < statNames.length; k++) {
					findex = html.indexOf("<th>"+statNames[k]);
					if (findex == -1) return;
					html = html.substring(findex);
					findex = html.indexOf('"'+">");
					if (findex == -1) return;
					
					html = html.substring(findex+2);
					bstatsString += (k==0?"":",")+Integer.parseInt(html.substring(0,html.indexOf('<')));
				}
				
				//get mon text description from emerald version
				findex = html.indexOf("<th>Emerald</th>");
				if (findex == -1) {
					findex = html.indexOf("<br>Emerald");
					if (findex == -1) return;
				}
				html = html.substring(findex);
				String dsc = html.substring(html.indexOf("<td>")+4,html.indexOf("</td>"));
				
				//get mon next evolution
				int evolveLevel = -1,
						evolve = -1;
				
				findex = html.indexOf("alt="+'"'+name.substring(0,1).toUpperCase()+name.substring(1));
				if (findex != -1) {
					html = html.substring(findex+16);
					
					findex = html.indexOf("<i class="+'"'+"icon-arrow"+'"'+">");
		
					while (true) {
						if (findex != -1) {
							html = html.substring(findex);
							try {
								evolveLevel = Integer.parseInt(html.substring(html.indexOf("Level ")+6, html.indexOf(')')));
							} catch (Exception e) {
								//non level-based evolution route, not compatible
								break;
							}
							
							findex = html.indexOf("href="+'"'+"/pokedex/");
							html = html.substring(findex+15);
							evolve = mon.indexOf(html.substring(0, html.indexOf('"')));
						}
						break;
					}
				}
				
				//get all moves mon can learn
				String movesLearnedByLevel = "",
					   levelMovesLearned = "",
					   movesLearnedByTM = "";
				boolean firstMLL = true,
						firstMLT = true;
				
				html = BotUtils.HttpGet("http://pokemondb.net/pokedex/"+name+"/moves/3", "");
				if (html == null) return;
				
				
				//moves learnt by level up
				findex = html.indexOf("Moves learnt by level up");
				if (findex != -1) {
					html = html.substring(findex);
					
					while ((findex = html.indexOf("<td ")) != -1) {
						findex = html.indexOf('>', findex);
						html = html.substring(findex+1);
						
						levelMovesLearned += (firstMLL?"":",")+html.substring(0,html.indexOf('<'));
						
						final String searchMoveStr = "href="+'"'+"/move/";
						findex = html.indexOf(searchMoveStr);
						html = html.substring(findex+searchMoveStr.length());
						
						movesLearnedByLevel += (firstMLL?"":",")+moveMap.get(html.substring(0,html.indexOf('"')).toLowerCase().replaceAll(" ", "-"));
						
						firstMLL = false;
						
						findex = html.indexOf("<tr>");
						if (findex != -1) {
							if (findex > html.indexOf("Egg moves")) {
								break;
							} else {
								html = html.substring(findex);
							}
						}
					}
				}
				
				//moves learnt by HM
				findex = html.indexOf("Moves learnt by HM");
				if (findex != -1) {
					html = html.substring(findex);
					
					int nindex = html.indexOf("Moves learnt by TM");
					while ((findex = html.indexOf("<td ")) != -1 && (nindex != -1 && nindex > findex)) {
						findex = html.indexOf('>', findex);
						html = html.substring(findex+1);
											
						final String searchMoveStr = "href="+'"'+"/move/";
						findex = html.indexOf(searchMoveStr);
						html = html.substring(findex+searchMoveStr.length());
						
						movesLearnedByTM += (firstMLT?"":",")+moveMap.get(html.substring(0,html.indexOf('"')).toLowerCase().replaceAll(" ", "-"));
						firstMLT = false;
						
						findex = html.indexOf("<tr>");
						if (findex != -1) {
							if (findex > html.indexOf("Moves learnt by TM")) {
								break;
							} else {
								html = html.substring(findex);
							}
						}
					}
				}
				
				//moves learnt by TM
				findex = html.indexOf("Moves learnt by TM");
				if (findex != -1) {
					html = html.substring(findex);
					
					int nindex = html.indexOf("Moves learnt by level up");
					while ((findex = html.indexOf("<td ")) != -1 && (nindex != -1 && nindex > findex)) {
						findex = html.indexOf('>', findex);
						html = html.substring(findex+1);
											
						final String searchMoveStr = "href="+'"'+"/move/";
						findex = html.indexOf(searchMoveStr);
						html = html.substring(findex+searchMoveStr.length());
						
						movesLearnedByTM += (firstMLT?"":",")+moveMap.get(html.substring(0,html.indexOf('"')).toLowerCase().replaceAll(" ", "-"));
						firstMLT = false;
						
						findex = html.indexOf("<tr>");
						if (findex != -1) {
							nindex = html.indexOf("Moves learnt by level up");
							if (nindex == -1 || findex > nindex) {
								break;
							} else {
								html = html.substring(findex);
							}
						}
					}
				}
							
				byte[] strBytes = (name+'"'+dsc+'"'+BotUtils.FirstGoogleImagesResult(name+" pokemon")+'"'+typeMap.get(type1)+'"'+typeMap.get(type2)+'"'+bstatsString+'"'+evolve+'"'+evolveLevel+'"'+movesLearnedByLevel+'"'+levelMovesLearned+'"'+movesLearnedByTM).getBytes(StandardCharsets.UTF_8);
				try {
					fout.write(ByteBuffer.wrap(new byte[4]).putInt(strBytes.length).array());
					fout.write(strBytes);
				} catch (IOException e) {}
				//System.out.println((i==0?"":",")+"new MonDef("+'"'+name+'"'+","+'"'+dsc+'"'+","+'"'+BotUtils.FirstGoogleImagesResult(name+" pokemon")+'"'+",(byte)"+typeMap.get(type1)+",(byte)"+typeMap.get(type2)+","+bstatsString+",(short)"+evolve+",(byte)"+evolveLevel+").setMoves("+movesLearnedByLevel+"},"+levelMovesLearned+"},"+movesLearnedByTM+"})");
			}
			try {
				fout.close();
			} catch (IOException e) {}
		
 
 
 
 * 
 */
