//Ethan Alexander Shulman 2016


//!pickupcash command picks up cash randomly dropped on the ground, 10% chance of being a poop dollar


package cc.bot.discord.commands;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import cc.bot.discord.ChronicBot;
import cc.bot.discord.Command;
import cc.bot.discord.DataObject;
import cc.bot.discord.Main;
import cc.bot.discord.UserData;
import de.btobastian.javacord.entities.message.Message;


public class PickUpCashCommand extends Command implements ActionListener {
	
	private final String[] poopDollarImages = new String[] {"http://i.imgur.com/bl2hFa9.gif",
															"https://i.ytimg.com/vi/EeEHvfClelg/maxresdefault.jpg",
															"http://image.spreadshirtmedia.com/image-server/v1/designs/11185909,width=280,height=280?mediaType=png",
															"http://68.media.tumblr.com/dcb606ed544a543564e2f087df957a6a/tumblr_mfq1vdrZnc1ry0dcxo1_500.jpg",
															"http://68.media.tumblr.com/72ca6a372990ee0d2b8c42c667c5160d/tumblr_njy5bo2omp1r6278vo1_400.gif"};

	//private final String[] dropChannels = new String[] {"120626796004638720", "216441714859048962", "216240056778620929", "217881375754289155", "144629547982258176"};
	
	
	private Object droppedCashLock = new Object();
	private int droppedCash = -1;
	private boolean poopDollar = false;
	private String dropChannel = null;
		
	
	public PickUpCashCommand() {
		//call action listener every 3 minutes to randomly drop cash
		new Timer(180000,this).start();
	}
	
	@Override
	public void init() {
		DataObject dropped = Main.chronicBot.mappedChannelData.get("pickupcash");
		if (dropped == null) return;
		
		if (dropped.data.length() > 0) {
			String[] svars = dropped.data.split(":");
			droppedCash = Integer.parseInt(svars[0]);
			poopDollar = svars[1].equals("1");
			dropChannel = svars[2];
		}
	}
	
	@Override
	public void deinit() {
		if (droppedCash > 0) {
			Main.chronicBot.mappedChannelData.put("pickupcash", new DataObject(droppedCash+":"+(poopDollar?1:0)+":"+dropChannel));
		} else {
			Main.chronicBot.mappedChannelData.put("pickupcash", new DataObject(""));
		}
	}
	
	
	public void actionPerformed(ActionEvent e) {
		boolean dropped = false;
		
		synchronized (droppedCashLock) {
			if (droppedCash > -1) return;//cash already on ground
			
			//25% chance to drop cash
			if (Math.random() < 0.25) {
				poopDollar = Math.random()<0.1;//10% chance to be poop dollar
				droppedCash = (int) (Math.random()*1000)+100;
				dropChannel = ChronicBot.HOME_CHANNEL_ID;//dropChannels[(int) (Math.random()*dropChannels.length)];
				dropped = true;
			}
		}
		
		//display message about dollar on random drop channel
		if (dropped) {
			Main.chronicBot.sendMessage("$"+droppedCash+" on the ground.", null, 
					Main.chronicBot.getServer().getChannelById(dropChannel));
		}
	}
	
	
	
	@Override
	public void run(Message m) {
		if (m.getChannelReceiver() == null) return;//only allow bot in botzone
		
		if (droppedCash == -1) return;//no cash dropped
		
		//get user data
		String uid = m.getAuthor().getId();
		UserData udat = Main.chronicBot.getUserData(uid,true);
		
		//get cash amount
		DataObject cashObj = udat.data.get("cash");
		if (cashObj == null) {
			return;
		}
		
		boolean grabbed = false,
				wasPoopDollar = false;
		int grabAmount = -1;
		synchronized (droppedCashLock) {
			if (droppedCash == -1) return;//no cash dropped
			if (!m.getChannelReceiver().getId().equals(dropChannel)) return;//not in right channel
			
			//pick up cash
			synchronized (cashObj.lock) {
				cashObj.data = ""+(Integer.parseInt(cashObj.data)+droppedCash);
			}
			
			grabAmount = droppedCash;
			droppedCash = -1;
			grabbed = true;
			wasPoopDollar = poopDollar;		
		}
		
		//grabbed cash, send confirm message
		if (grabbed) {
			String message = "<@"+m.getAuthor().getId()+"> picked up $"+grabAmount+". ";
			if (wasPoopDollar) {
				message += "\n"+poopDollarImages[(int) (Math.random()*poopDollarImages.length)];
			}
			Main.chronicBot.sendMessage(message, null, m.getChannelReceiver());
		}
	}

	@Override
	public String name() {
		return "pickupcash";
	}

	@Override
	public String description() {
		return "Pick up chroniccash randomly dropped on the ground.";
	}
	
	

}
