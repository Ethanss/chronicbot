//Ethan Alexander Shulman 2016


//Holds data for a message being sent out


package cc.bot.discord;

import de.btobastian.javacord.entities.Channel;
import de.btobastian.javacord.entities.User;



public class PreparedMessage {
	
	public String content;
	public User toUser;
	public Channel toChannel;
	
	public PreparedMessage(String c, User t, Channel p) {
		content = c;
		toUser = t;
		toChannel = p;
	}
	
}
