//Ethan Alexander Shulman 2016

//Home of the program entry point

package cc.bot.discord;





public class Main {

	public static ChronicBot chronicBot;
	
	//Program entry point
	public static void main(String args[]) {

		//generate needed battlemon stuff
		//MonCommand.genMonStuff();
		//if (true) return;

		//start
		new ChronicBot();
		chronicBot.processMessageSender();
	}
	
}