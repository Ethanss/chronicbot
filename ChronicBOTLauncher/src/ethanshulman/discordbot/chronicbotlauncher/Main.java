//Ethan Alexander Shulman 2016




package ethanshulman.discordbot.chronicbotlauncher;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Main {
	
	public static Launcher launcher;
	
	public static void main(String args[]) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {}
		
		launcher = new Launcher();
	}

}
