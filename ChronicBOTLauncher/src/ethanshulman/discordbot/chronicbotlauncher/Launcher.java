//Ethan Alexander Shulman 2016

package ethanshulman.discordbot.chronicbotlauncher;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import javax.swing.JFrame;

public class Launcher extends JFrame implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7052237344185645097L;
	
	
	
	private GUI gui = new GUI();
	
	private boolean botRunning = false,
					botThreadRunning = false,
					restarting = false;
	
	
	public Launcher() {
		super("ChronicBOT Launcher");
		setResizable(false);
		setSize(300,220);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().add(gui);
		setVisible(true);
		
		gui.lblStatus.setText("<html>Status: <font color='red'>Offline</font>.</html>");
	}
	
	
	public boolean toggleBot() {
		botRunning = !botRunning;//toggle bot running
				
		//start or stop bot
		if (botRunning) {			
			gui.lblStatus.setText("<html>Status: <font color='blue'>Initializing</font>.</html>");

			while (botThreadRunning) {//wait for old bot thread to shut down
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {}
			}
			
			botThreadRunning = true;
			new Thread(this).start();
		} else {
			gui.lblStatus.setText("<html>Status: <font color='black'>Shutting down</font>.</html>");
			
			try {
				processConsoleWrite.write("shutdownchronicbot");
				processConsoleWrite.write("\n");
				processConsoleWrite.flush();
				processConsoleWrite.close();
			} catch (IOException e) {}
		}
		
		return botRunning;//return new state of bot
	}

	
	private BufferedWriter processConsoleWrite;

	@Override
	public void run() {
		
		Process process = null;
		try {
			process = (new ProcessBuilder("java","-jar","ChronicBOT_discordbot.jar").redirectErrorStream(true)).start();
		} catch (IOException e) {}
		
		
		processConsoleWrite = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
		Scanner ib = new Scanner(process.getInputStream());
		
		//main process thread reading console output
		while (true) {
			
			//read input
			String istr = ib.nextLine();
			if (istr.contains("Websocket closed with reason") || istr.contains("Connection timed out: connect") || istr.contains("Read timed out")) {
				//disconnected, restart bot
				gui.lblStatus.setText("<html>Status: <font color='gray'>Restarting</font>.</html>");
				restarting = true;
				try {
					processConsoleWrite.write("shutdownchronicbot");
					processConsoleWrite.write("\n");
					processConsoleWrite.flush();
					processConsoleWrite.close();
				} catch (IOException e) {}
			} else if (istr.startsWith("ChronicBOT initialized.")) {
				gui.lblStatus.setText("<html>Status: <font color='green'>Online</font>.</html>");
			} else {
				if (istr.startsWith("Shutting down ChronicBOT")) {
					//bot is shut down
					gui.lblStatus.setText("<html>Status: <font color='red'>Offline</font>.</html>");
					break;
				}
			}
			
		}
		
		ib.close();
		
		if (restarting) {
			new Thread(this).start();
			restarting = false;
		} else {
			botThreadRunning = false;
			botRunning = false;
		}
	}
	
	
}
