package ethanshulman.discordbot.chronicbotlauncher;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI extends JPanel {
	
	public JLabel lblStatus;
	
	public GUI() {
		setLayout(null);
		
		final JButton btnNewButton = new JButton("Start");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean botOn = Main.launcher.toggleBot();
				if (botOn) {
					btnNewButton.setText("Stop");
				} else {
					btnNewButton.setText("Start");
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 26));
		btnNewButton.setBounds(10, 11, 280, 115);
		add(btnNewButton);
		
		lblStatus = new JLabel("Status:");
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblStatus.setBounds(10, 137, 280, 52);
		add(lblStatus);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5366744082601769117L;
}
