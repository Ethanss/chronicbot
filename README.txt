
#ChronicBOT, a custom discord bot made in Java(uses Javacord).



##Getting started

-[Download the binaries](http://xaloez.com/projects/projects/other projects/chronicbot/ChronicBOT0.zip)

-Extract binaries from ZIP.

-Run 'Launch BotTokenObfuscator.bat'(if Linux/Mac open terminal and run 'java -jar BotTokenObfuscator.jar'),
enter Discord bot token, copy the obfuscated version.

-Open 'bot.ini' in any text editor and paste the obfuscated bot token after 'token=', leaving no whitespaces.

-In 'bot.ini' enter the id of your Discord server and the desired home channel for the bot after 'serverid=' and 'channelid='.

-Open ChronicBOT_Launcher.jar, use the Start button to start the bot.